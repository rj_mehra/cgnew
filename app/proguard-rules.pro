# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\rajme\AppData\Local\Android\Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keep public class com.billdesk.sdk.*
-keep public class com.billdesk.config.*
-keep public class com.billdesk.utils.URLUtilActivity
-keep public interface com.billdesk.sdk.LibraryPaymentStatusProtocol{
    public void paymentStatus(java.lang.String,android.app.Activity);
    }
-keep class com.billdesk.sdk.PaymentWebView$JavaScriptInterface{
    public void gotMsg(java.lang.String);
    }
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
    }
-keepattributes JavascriptInterface
-keep public class com.billdesk.sdk.PaymentWebView$JavaScriptInterface
-keep public class * implements com.billdesk.sdk.PaymentWebView$JavaScriptInterface
-keepclassmembers class com.billdesk.sdk.PaymentWebView$JavaScriptInterface {
    <methods>;
    }
    -keep class com.amazonaws.** { *; }
    -keepnames class com.amazonaws.** { *; }
    -dontwarn com.amazonaws.**
    -dontwarn com.fasterxml.**
    # Platform calls Class.forName on types which do not exist on Android to determine platform.
    -dontnote retrofit2.Platform
    # Platform used when running on Java 8 VMs. Will not be used at runtime.
    -dontwarn retrofit2.Platform$Java8
    # Retain generic type information for use by reflection by converters and adapters.
    -keepattributes Signature
    # Retain declared checked exceptions for use by a Proxy instance.
    -keepattributes Exceptions