package in.cableguy.cableguy.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.helper.Transaction;

/**
 * @author Raj Mehra
 * Created by rajme on 1/21/2017.
 */
public class SubsLastTransactionAdapter  extends RecyclerView.Adapter<SubsLastTransactionAdapter.ViewHolder> {
    private Activity activity;
    /**
     * The Tag.
     */
    static String TAG=SubsLastTransactionAdapter.class.getSimpleName();
    /**
     * The Transactions.
     */
    List<Transaction> transactions=new ArrayList<>();

    /**
     * Instantiates a new Subs last transaction adapter.
     *
     * @param transactions the transactions
     * @param activity     the activity
     */
    public SubsLastTransactionAdapter(List<Transaction> transactions,Activity activity) {
        this.activity = activity;
        this.transactions=transactions;
    }

    /**
     * Gets count.
     *
     * @return the count
     */
    public int getCount() {
        return transactions.size();
    }

    /**
     * Gets item.
     *
     * @param position the position
     * @return the item
     */
    public Object getItem(int position) {
        return transactions.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }


    @Override
    public SubsLastTransactionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View pairedDeviceView = inflater.inflate(R.layout.subs_trans_item, parent, false);

        // Return a new holder instance
        SubsLastTransactionAdapter.ViewHolder viewHolder = new SubsLastTransactionAdapter.ViewHolder(pairedDeviceView);
        return viewHolder;
    }

    // Involves populating data into the item through holder

    /**
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(SubsLastTransactionAdapter.ViewHolder viewHolder, final int position) {
        // Get the data model based on position
        final Transaction transaction = transactions.get(position);
        // Set item views based on your views and data model
        TextView receiptNoTV=viewHolder.receiptNoTV,
                subsNameTV=viewHolder.subsNameTV,
                subsNoTV=viewHolder.subsNoTV,
                amountTV=viewHolder.amountTV,
                timestampTV=viewHolder.timestampTV,
                payModeTV=viewHolder.payModeTV;
        receiptNoTV.setText(transaction.getReceiptNo());
        subsNameTV.setText(transaction.getSubscriberName());
        subsNoTV.setText(transaction.getSubscriberNumber());
        amountTV.setText(transaction.getAmount());
        timestampTV.setText(transaction.getDateTime());
        payModeTV.setText(transaction.getPaymentMode());

    }

    /**
     * The type View holder.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

// Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView receiptNoTV, /**
         * The Subs name tv.
         */
        subsNameTV, /**
         * The Subs no tv.
         */
        subsNoTV, /**
         * The Amount tv.
         */
        amountTV, /**
         * The Timestamp tv.
         */
        timestampTV, /**
         * The Pay mode tv.
         */
        payModeTV;

        /**
         * Instantiates a new View holder.
         *
         * @param itemView the item view
         */
// We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);
            receiptNoTV=(TextView)itemView.findViewById(R.id.receiptNoTV);
            subsNameTV=(TextView)itemView.findViewById(R.id.subscriberNameTV);
            subsNoTV=(TextView)itemView.findViewById(R.id.subscriberNumberTV);
            amountTV=(TextView)itemView.findViewById(R.id.amountTV);
            timestampTV=(TextView)itemView.findViewById(R.id.timestampTV);
            payModeTV=(TextView)itemView.findViewById(R.id.paymentModeTV);
        }
    }

}
