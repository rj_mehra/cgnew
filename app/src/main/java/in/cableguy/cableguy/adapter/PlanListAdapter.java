package in.cableguy.cableguy.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.cableguy.cableguy.R;

/**
 * @author Raj Mehra
 * Created by rajme on 1/21/2017.
 */
public class PlanListAdapter extends ArrayAdapter{
    private final Activity context;
    private final ArrayList<String> planList;

    /**
     * Instantiates a new Plan list adapter.
     *
     * @param context  the context
     * @param planList the plan list
     */
    public PlanListAdapter(Activity context, ArrayList<String>planList){
        super(context, R.layout.planlist,planList);
        this.context = context;
        this.planList = planList;
    }

    /**
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.planlist, null, true);
        TextView planNameText = (TextView) rowView.findViewById(R.id.planName);
        planNameText.setText(this.planList.get(position));
        return rowView;
    }
}
