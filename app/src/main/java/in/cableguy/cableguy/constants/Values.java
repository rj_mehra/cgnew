package in.cableguy.cableguy.constants;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;

import java.util.ArrayList;

import in.cableguy.cableguy.helper.Building;
import in.cableguy.cableguy.helper.Customer;
import in.cableguy.cableguy.helper.ECAFCustomer;
import in.cableguy.cableguy.helper.Plan;

/**
 *
 * @author Raj Mehra
 * Created by rajme on 1/21/2017.
 */
public class Values {
    /**
     * The constant SOAP_ACTION.
     */
    public static String SOAP_ACTION = "http://52.66.25.106/CableguyService/MobileService.svc";
    /**
     * The constant NAME_SPACE.
     */
    public static String NAME_SPACE = "http://52.66.25.106/CableguyService/MobileService.svc";
    /**
     * The constant API_URL.
     */
    public static String API_URL = "http://52.66.25.106/CableguyService/MobileService.svc";

    /**
     * The constant LOGIN_METHOD.
     */
/*public static String SOAP_ACTION = "http://ec2-52-66-25-106.ap-south-1.compute.amazonaws.com/api/";
    public static String NAME_SPACE = "http://ec2-52-66-25-106.ap-south-1.compute.amazonaws.com/api/";
    public static String API_URL = "http://ec2-52-66-25-106.ap-south-1.compute.amazonaws.com/api/";*/
    public static String LOGIN_METHOD = "Login";
    /**
     * The Locales.
     */
    public static CharSequence[] locales = {"English", "Hindi", "Marathi", "Gujarati"};
    /**
     * The constant localeSelected.
     */
    public static Boolean localeSelected = false;


    /**
     * The constant selectedArea.
     */
    public static String selectedArea = "";
    /**
     * The constant selectedZone.
     */
    public static String selectedZone = "Select Zone";
    /**
     * The constant buildingName.
     */
//public static String selectedCustomer = "Select Customer";
/*    public static String selectedBuilding = "Select Building";*/
    public static String buildingName = "Select Building";
    /**
     * The constant printValue.
     */
    public static String printValue="";


    /**
     * The Selected channel array.
     */
// DIY
    public static ArrayList<String> selectedChannelArray;
    /**
     * The constant customerName.
     */
    public static String customerName="";
    /**
     * The constant option.
     */
    public static String option = "";


    /**
     * The constant READ_PERM_GRANT.
     */
    public static boolean READ_PERM_GRANT=false;

    /**
     * The constant LOC_PERM_GRANT.
     */
    public static boolean LOC_PERM_GRANT=false;

    /**
     * The constant GPS_ENABLED.
     */
    public static boolean GPS_ENABLED=false;

    /**
     * The constant selectedBuilding.
     */
    public static Building selectedBuilding;

    /**
     * The constant selectedCustomer.
     */
    public static Customer selectedCustomer;

    /**
     * The constant selectedPlan.
     */
    public static Plan selectedPlan;


    /**
     * The constant LOGIN_ACTION.
     */
//SOAP ACTION
    public static String LOGIN_ACTION="http://tempuri.org/IMobileService/Login";

    /**
     * The constant TYPE_DETAILS_ACTION.
     */
    public static String TYPE_DETAILS_ACTION="http://tempuri.org/IMobileService/GetTypeDetails";

    /**
     * The constant SUB_SEARCH_ACTION.
     */
    public static String SUB_SEARCH_ACTION="http://tempuri.org/IMobileService/SubsSearch";

    /**
     * The constant INSERT_PAYMENT_ACTION.
     */
    public static String INSERT_PAYMENT_ACTION="http://tempuri.org/IMobileService/InsertPayment";

    /**
     * The constant ONLINE_PAYMENT_ACTION.
     */
    public static String ONLINE_PAYMENT_ACTION="http://tempuri.org/IMobileService/OnlinePayment";

    /**
     * The constant INSERT_ONLINE_PAYMENT_ACTION.
     */
    public static String INSERT_ONLINE_PAYMENT_ACTION="http://tempuri.org/IMobileService/InsertOnlinePayment";

    /**
     * The constant GET_PLAN_LIST.
     */
    public static String GET_PLAN_LIST="http://tempuri.org/IMobileService/GetPlanList";

    /**
     * The constant SUBMIT_PLAN_CHANGE.
     */
    public static String SUBMIT_PLAN_CHANGE="http://tempuri.org/IMobileService/SubmitPlanChange";

    /**
     * The constant SUBS_LAST_TRANSACTION.
     */
    public static String SUBS_LAST_TRANSACTION="http://tempuri.org/IMobileService/SubscriberTransactionLast5";

    /**
     * The constant USER_LAST_TRANSACTION.
     */
    public static String USER_LAST_TRANSACTION="http://tempuri.org/IMobileService/UserTransactionLast3";

    /**
     * The constant CHANGE_PASSWORD.
     */
    public static String CHANGE_PASSWORD="http://tempuri.org/IMobileService/ChangePassword";

    /**
     * The constant GET_COLLECTION_REPORT.
     */
    public static String GET_COLLECTION_REPORT="http://tempuri.org/IMobileService/GetCollectionReport";

    /**
     * The constant SUBSCRIBER_DETAILS_MODIFY.
     */
    public static String SUBSCRIBER_DETAILS_MODIFY="http://tempuri.org/IMobileService/SubscriberDetailsModify";

    /**
     * The constant REPRINT_ACTION.
     */
    public static String REPRINT_ACTION = "http://tempuri.org/IMobileService/UserSubsLastTransactionDet";

    /**
     * The constant GET_VERSION.
     */
    public static String GET_VERSION = "http://tempuri.org/IMobileService/GetVersion";

    /**
     * The constant USER_TOKEN.
     */
    public static String USER_TOKEN;


    /**
     * The constant onCustomerSelect.
     */
    public static String onCustomerSelect="billingtxn";

    /**
     * The constant listType.
     */
    public static String listType;


    /**
     * The constant selectedCustomerName.
     */
    public static String selectedCustomerName="";

    /**
     * The constant buildingId.
     */
    public static String buildingId = "";
    /**
     * The constant zoneId.
     */
    public static String zoneId="";
    /**
     * The constant redirect.
     */
    public static String redirect="";
    /**
     * The constant areaId.
     */
    public static String areaId="";
    /**
     * The constant lcoId.
     */
    public static String lcoId="";

    /**
     * The constant selectedLcoId.
     */
    public static String selectedLcoId;
    /**
     * The constant customerListRedirect.
     */
    public static String customerListRedirect;

    /**
     * The constant currentVersion.
     */
    public static String currentVersion;

    /**
     * The constant selectedlcoName.
     */
    public static String selectedlcoName = "";
    /**
     * The constant selectedlcoAddress.
     */
    public static String selectedlcoAddress = "";
    /**
     * The constant selectedlcoPhone1.
     */
    public static String selectedlcoPhone1;
    /**
     * The constant selectedlcoPhone2.
     */
    public static String selectedlcoPhone2;

    /**
     * The constant selectedCustomerNumber.
     */
    public static String selectedCustomerNumber;

    /**
     * The constant searchType.
     */
    public static String searchType;

    /**
     * The constant cp1.
     */
    public static String cp1 = "";
    /**
     * The constant cardNumber.
     */
    public static String cardNumber = "";
    /**
     * The constant stbNumber.
     */
    public static String stbNumber = "";
    /**
     * The constant customerNumber.
     */
    public static String customerNumber = "";
    /**
     * The constant etstFlag.
     */
    public static String etstFlag ="";
    /**
     * The constant customerIdSelected.
     */
    public static String customerIdSelected="";
    /**
     * The constant username.
     */
    public static String username = "";
    /**
     * The constant loginTime.
     */
    public static String loginTime = "";
    /**
     * The constant AMTEDITFLAG.
     */
    public static String AMTEDITFLAG = "";
    /**
     * The constant PREVIOUSLY_PAID_AMOUNT.
     */
    public static String PREVIOUSLY_PAID_AMOUNT = "";
    /**
     * The constant currentBalance.
     */
    public static String currentBalance = "";
    /**
     * The constant CREDITLIMITFLAG.
     */
    public static String CREDITLIMITFLAG = "";
    /**
     * The constant ISONLINEPAY.
     */
    public static String ISONLINEPAY = "";
    /**
     * The constant lastTransaction.
     */
    public static String lastTransaction = "";
    /**
     * The constant GET_VERSION_ACTION.
     */
    public static String GET_VERSION_ACTION ="http://tempuri.org/IMobileService/GetVersion";
    /**
     * The constant ECAF_SEARCH.
     */
    public static String ECAF_SEARCH="";
    /**
     * The constant PRINTER_TYPE.
     */
    public static String PRINTER_TYPE="";


    /**
     * The constant fileUri.
     */
    public static Uri fileUri;

    /**
     * The constant mBitmap.
     */
    public static Bitmap mBitmap;

    /**
     * The constant mCanvas.
     */
    public static Canvas mCanvas=new Canvas();

    /**
     * The constant coordinates.
     */
    public static String coordinates;
    /**
     * The constant READ_EXT_STORAGE_PERM.
     */
    public static boolean READ_EXT_STORAGE_PERM;
    /**
     * The constant WRITE_EXT_STORAGE_PERM.
     */
    public static boolean WRITE_EXT_STORAGE_PERM;
    /**
     * The constant setCustomerPhotoUrl.
     */
    public static String setCustomerPhotoUrl, /**
     * The Set id proof url.
     */
    setIdProofUrl, /**
     * The Set sign url.
     */
    setSignUrl, /**
     * The Set address proof url.
     */
    setAddressProofUrl;

    /**
     * The constant idProofImageUri.
     */
    public static Uri idProofImageUri, /**
     * The Add proof image uri.
     */
    addProofImageUri, /**
     * The Cust image uri.
     */
    custImageUri, /**
     * The Sign image uri.
     */
    signImageUri;

    /**
     * The constant idProofImageName.
     */
    public static String idProofImageName, /**
     * The Add proof image name.
     */
    addProofImageName, /**
     * The Cust image name.
     */
    custImageName, /**
     * The Sign image name.
     */
    signImageName;

    /**
     * The constant idProofImageFile.
     */
    public static String idProofImageFile, /**
     * The Add proof image file.
     */
    addProofImageFile, /**
     * The Cust image file.
     */
    custImageFile, /**
     * The Sign image file.
     */
    signImageFile;

    /**
     * The constant GeoLocation.
     */
    public static String GeoLocation="";
    /**
     * The constant ECAF_CUSTOMER.
     */
    public static ECAFCustomer ECAF_CUSTOMER= null;
    /**
     * The constant SELF_CODE.
     */
    public static String SELF_CODE = "";
    /**
     * The constant STATE_ID.
     */
    public static String STATE_ID="";
    /**
     * The constant CITY_ID.
     */
    public static String CITY_ID="";
    /**
     * The constant LOC_ID.
     */
    public static String LOC_ID="";
    /**
     * The constant BUILDING_ID.
     */
    public static String BUILDING_ID="";
    /**
     * The constant MODULE.
     */
    public static String MODULE = "";

    //ECAF STATIC VALUES

    /**
     * The constant ECAF_USERNAME.
     */
    public static String ECAF_USERNAME="";
    /**
     * The constant ECAF_SUBS_CODE.
     */
    public static String ECAF_SUBS_CODE="";
    /**
     * The constant SUBMIT_COUNT.
     */
    public static int SUBMIT_COUNT;
    /**
     * The constant ALLOWECAF.
     */
    public static String ALLOWECAF = "";
    /**
     * The constant PLAN_ID.
     */
    public static String PLAN_ID="";

    /**
     * The constant SLC_ENABLED.
     */
    public static boolean SLC_ENABLED= false;

    /**
     * The constant BILLDESK_URL.
     */
    public static String BILLDESK_URL="https://pgi.billdesk.com/pgidsk/PGIMerchantPayment";
    /**
     * The constant RETURN_URL.
     */
    public static String RETURN_URL="http://35.154.198.134/admin/getresponseapp.aspx";
    /**
     * The constant PAY_LOAD.
     */
    public static String PAY_LOAD ="";
    /**
     * The constant CHECKSUM_KEY.
     */
    public static String CHECKSUM_KEY = "Q9vc3wcSrAZA";

    /**
     * The constant VISIBILITY_COUNTER.
     */
    public static int VISIBILITY_COUNTER ;
    /**
     * The constant ORDER_ID.
     */
    public static String ORDER_ID="";
    /**
     * The constant ORDER_AMOUNT.
     */
    public static String ORDER_AMOUNT="";
    /**
     * The constant isAadhar.
     */
    public static Boolean isAadhar;
    /**
     * The constant isNew.
     */
    public static Boolean isNew;
    /**
     * The constant FETCH_TRANSACTION.
     */
    public static int FETCH_TRANSACTION;
    /**
     * The constant TRANS_MODE.
     */
    public static String TRANS_MODE = "Online";
    /**
     * The constant CHEQUE_PAYMENT.
     */
    public static Boolean CHEQUE_PAYMENT = false;
    /**
     * The constant SLC_OPTION.
     */
    public static String SLC_OPTION="";
    /**
     * The constant ALLOW_OFFLINE.
     */
    public static String ALLOW_OFFLINE = "";
    /**
     * The constant PAY_MODE.
     */
    public static String PAY_MODE = "";
    /**
     * The constant MERCHANTID.
     */
    public static String MERCHANTID = "";
}
