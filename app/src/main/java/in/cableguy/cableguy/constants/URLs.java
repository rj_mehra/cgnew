package in.cableguy.cableguy.constants;

/**
 * @author Raj Mehra
 * Created by Raj on 24-Mar-17.
 */
public class URLs {

    /**
     * The constant IMAGE_URL.
     */
    public static String IMAGE_URL="https://s3-us-west-2.amazonaws.com/cguyapp/";
    /**
     * The constant ECAF_SUBSCRIBER_ACTION.
     */
    public static String ECAF_SUBSCRIBER_ACTION ="http://tempuri.org/IMobileService/EcafSubscriber";
    /**
     * The constant ECAF_EXISTING_PLAN.
     */
    public static String ECAF_EXISTING_PLAN = "http://tempuri.org/IMobileService/ExistingSubscriberPlan";
    /**
     * The constant ECAF_UPDATE_SUBSCRIBER.
     */
    public static String ECAF_UPDATE_SUBSCRIBER = "http://tempuri.org/IMobileService/EcafUpdateSubscriber";
    /**
     * The constant INSERT_ONLINE_PAYMENT_ACTION.
     */
    public static String INSERT_ONLINE_PAYMENT_ACTION = "http://tempuri.org/IMobileService/InsertOnlinePayment";
}
