package in.cableguy.cableguy.constants;

/**
 * @author Raj Mehra
 * Created by Raj on 24-Mar-17.
 */
public class Constants {
    /**
     * The constant COGNITO_POOL_ID.
     */
    public static final String COGNITO_POOL_ID = "us-west-2:98a49160-59c8-46e1-b01c-ef6992fecd61";
    /**
     * The constant BUCKET_NAME.
     */
//"us-west-2:f53479f5-a4cb-430e-9eb8-fb928a77bcfb"
    /*
     * Note, you must first create a bucket using the S3 console before running
     * the sample (https://console.aws.amazon.com/s3/). After creating a bucket,
     * put it's name in the field below.
     */
    public static final String BUCKET_NAME = "cguyapp";

    /**
     * The constant API_URL.
     */
/*    public static final String API_URL = "http://35.154.198.134/cableguyservicerest/Service1.svc/";*/
/*    public static final String API_URL = "http://124.153.73.30:5000/restservice/Service1.svc/";*/
    public static final String API_URL = "http://35.154.198.134/restservice/Service1.svc/";
    /**
     * The constant LOGIN_API.
     */
    public static final String LOGIN_API = API_URL+"Login?";
    /**
     * The constant SUBS_SEARCH.
     */
    public static final String SUBS_SEARCH = API_URL+"SubsSearch?";
    /**
     * The constant PAYMENT_DETAILS.
     */
    public static final String PAYMENT_DETAILS = API_URL+"PaymentDetails?";
    /**
     * The constant INSERT_PAYMENT.
     */
    public static final String INSERT_PAYMENT = API_URL+"InsertPayment?";
    /**
     * The constant GET_PLAN_LIST.
     */
    public static final String GET_PLAN_LIST = API_URL+"GetPlanList?";
    /**
     * The constant SUBMIT_PLAN_CHANGE.
     */
    public static final String SUBMIT_PLAN_CHANGE = API_URL+"SubmitPlanChange?";
    /**
     * The constant GET_COLLECTION_REPORT.
     */
    public static final String GET_COLLECTION_REPORT=API_URL+"GetCollectionReport?";
    /**
     * The constant CHANGE_PASSWORD.
     */
    public static final String CHANGE_PASSWORD = API_URL+"ChangePassword?";
    /**
     * The constant USER_TRANSACTION_LAST_3.
     */
    public static final String USER_TRANSACTION_LAST_3=API_URL+"UserTransactionLast3?";
    /**
     * The constant USER_SUBS_TRANSACTION_DET.
     */
    public static final String USER_SUBS_TRANSACTION_DET=API_URL+"UserSubsLastTransactionDet?";
    /**
     * The constant SUBSCRIBER_TRANSACTION_LAST_5.
     */
    public static final String SUBSCRIBER_TRANSACTION_LAST_5=API_URL+"SubscriberTransactionLast5?";
    /**
     * The constant LAST_SUBSCRIBER_TRANSACTION_DET.
     */
    public static final String LAST_SUBSCRIBER_TRANSACTION_DET=API_URL+"LastSubscriberTransactionDet?";
    /**
     * The constant GET_TYPE_DETAILS.
     */
    public static final String GET_TYPE_DETAILS=API_URL+"GetTypeDetails?";
    /**
     * The constant SUBSCRIBER_DETAILS_MODIFY.
     */
    public static final String SUBSCRIBER_DETAILS_MODIFY=API_URL+"SubscriberDetailsModify?";
    /**
     * The constant GET_VERSION.
     */
    public static final String GET_VERSION = API_URL+"GetVersion?";
    /**
     * The constant ONLINE_PAYMENT.
     */
    public static final String ONLINE_PAYMENT = API_URL+"OnlinePayment?";
    /**
     * The constant INSERT_ONLINE_PAYMENT.
     */
    public static final String INSERT_ONLINE_PAYMENT =API_URL+"InsertOnlinePayment?";
    /**
     * The constant INSERT_SUBSCRIBER.
     */
    public static final String INSERT_SUBSCRIBER = API_URL+"InsSubscriber?";
    /**
     * The constant ECAF_SUBSCRIBER.
     */
    public static final String ECAF_SUBSCRIBER = API_URL+"EcafSubscriber?";
    /**
     * The constant ECAF_UPDATE_SUBSCRIBER.
     */
    public static final String ECAF_UPDATE_SUBSCRIBER = API_URL+"EcafUpdateSubscriber?";
    /**
     * The constant EXISTING_SUBSCRIBER_PLAN.
     */
    public static final String EXISTING_SUBSCRIBER_PLAN =API_URL+"ExistingSubscriberPlan?";
    /**
     * The constant OFFLINE_API.
     */
    public static final String OFFLINE_API = API_URL+"OfflinePayment?";
    /**
     * The constant RENEWAL_API.
     */
    public static final String RENEWAL_API = API_URL+"Renew_Subs_Bill?";


    //Offline DB

    /**
     * The constant DATABASE_NAME.
     */
    public static final String DATABASE_NAME ="CGOFFLINE_DB.db";
    /**
     * The constant OFFLINE_TABLE_NAME.
     */
    public static final String OFFLINE_TABLE_NAME = "offline";
    /**
     * The constant OFFLINE_COLUMN_ID.
     */
    public static final String OFFLINE_COLUMN_ID = "_id";
    /**
     * The constant OFFLINE_COLUMN_USERNAME.
     */
    public static final String OFFLINE_COLUMN_USERNAME = "username";
    /**
     * The constant OFFLINE_COLUMN_SUBSNAME.
     */
    public static final String OFFLINE_COLUMN_SUBSNAME = "subsname";
    /**
     * The constant OFFLINE_COLUMN_SUBSCODE.
     */
    public static final String OFFLINE_COLUMN_SUBSCODE = "subscode";
    /**
     * The constant OFFLINE_COLUMN_PAYMODE.
     */
    public static final String OFFLINE_COLUMN_PAYMODE = "paymode";
    /**
     * The constant OFFLINE_COLUMN_AMOUNT.
     */
    public static final String OFFLINE_COLUMN_AMOUNT = "amount";
    /**
     * The constant OFFLINE_COLUMN_ADDICHARGES.
     */
    public static final String OFFLINE_COLUMN_ADDICHARGES = "addicharges";
    /**
     * The constant OFFLINE_COLUMN_MOBILE.
     */
    public static final String OFFLINE_COLUMN_MOBILE = "mobile";
    /**
     * The constant OFFLINE_COLUMN_REMARK.
     */
    public static final String OFFLINE_COLUMN_REMARK = "remark";
    /**
     * The constant OFFLINE_COLUMN_CHEQUEDATE.
     */
    public static final String OFFLINE_COLUMN_CHEQUEDATE = "chequedate";
    /**
     * The constant OFFLINE_COLUMN_CHEQUENO.
     */
    public static final String OFFLINE_COLUMN_CHEQUENO = "chequeno";
    /**
     * The constant OFFLINE_COLUMN_BANKNAME.
     */
    public static final String OFFLINE_COLUMN_BANKNAME = "bankname";


}
