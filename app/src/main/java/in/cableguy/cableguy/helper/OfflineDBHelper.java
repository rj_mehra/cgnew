package in.cableguy.cableguy.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import in.cableguy.cableguy.constants.Constants;

/**
 * Created by rajme on 6/30/2017.
 */
public class OfflineDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION= 3;
    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            db.execSQL("CREATE TABLE " + Constants.OFFLINE_TABLE_NAME + "("+
                    Constants.OFFLINE_COLUMN_ID + " INTEGER PRIMARY KEY," +
                    Constants.OFFLINE_COLUMN_USERNAME + " TEXT, " +
                    Constants.OFFLINE_COLUMN_SUBSNAME + " TEXT, " +
                    Constants.OFFLINE_COLUMN_SUBSCODE + " TEXT, " +
                    Constants.OFFLINE_COLUMN_PAYMODE + " TEXT, " +
                    Constants.OFFLINE_COLUMN_AMOUNT + " TEXT, " +
                    Constants.OFFLINE_COLUMN_ADDICHARGES + " TEXT, " +
                    Constants.OFFLINE_COLUMN_MOBILE + " TEXT, " +
                    Constants.OFFLINE_COLUMN_REMARK + " TEXT, " +
                    Constants.OFFLINE_COLUMN_CHEQUEDATE + " TEXT, " +
                    Constants.OFFLINE_COLUMN_CHEQUENO + " TEXT, " +
                    Constants.OFFLINE_COLUMN_BANKNAME + " TEXT )"
            );
        }catch (Exception ex){
            Log.e("DB Exception ", ex.toString());
        }

    }

    /**
     * Instantiates a new Offline db helper.
     *
     * @param context the context
     */
    public OfflineDBHelper(Context context) {
        super(context, Constants.DATABASE_NAME , null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Constants.OFFLINE_TABLE_NAME);
        onCreate(db);
    }

    /**
     * Insert data boolean.
     *
     * @param userName   the user name
     * @param subsName   the subs name
     * @param subsCode   the subs code
     * @param payMode    the pay mode
     * @param amount     the amount
     * @param addCharges the add charges
     * @param mobile     the mobile
     * @param remark     the remark
     * @param chequeDate the cheque date
     * @param chequeNo   the cheque no
     * @param bankName   the bank name
     * @return the boolean
     */
    public boolean insertData (String userName, String subsName, String subsCode, String payMode, String amount, String addCharges,
                               String mobile, String remark, String chequeDate, String chequeNo, String bankName){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.OFFLINE_COLUMN_USERNAME, userName);
        contentValues.put(Constants.OFFLINE_COLUMN_SUBSNAME, subsName);
        contentValues.put(Constants.OFFLINE_COLUMN_SUBSCODE, subsCode);
        contentValues.put(Constants.OFFLINE_COLUMN_PAYMODE, payMode);
        contentValues.put(Constants.OFFLINE_COLUMN_AMOUNT, amount);
        contentValues.put(Constants.OFFLINE_COLUMN_ADDICHARGES, addCharges);
        contentValues.put(Constants.OFFLINE_COLUMN_MOBILE, mobile);
        contentValues.put(Constants.OFFLINE_COLUMN_REMARK, remark);
        contentValues.put(Constants.OFFLINE_COLUMN_CHEQUEDATE, chequeDate);
        contentValues.put(Constants.OFFLINE_COLUMN_CHEQUENO, chequeNo);
        contentValues.put(Constants.OFFLINE_COLUMN_BANKNAME, bankName);

        db.insert(Constants.OFFLINE_TABLE_NAME, null, contentValues);


        return true;
    }


    /**
     * Get all data cursor.
     *
     * @return the cursor
     */
    public Cursor getAllData(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + Constants.OFFLINE_TABLE_NAME, null);
        return res;
    }

    /**
     * Delete all data boolean.
     *
     * @return the boolean
     */
    public boolean deleteAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Constants.OFFLINE_TABLE_NAME,null,null);
        return true;
    }

    /**
     * Data count long.
     *
     * @return the long
     */
    public long dataCount(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + Constants.OFFLINE_TABLE_NAME, null);
        int count = res.getCount();
        return count;
    }
}
