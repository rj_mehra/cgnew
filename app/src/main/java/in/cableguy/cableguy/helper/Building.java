package in.cableguy.cableguy.helper;

/**
 *
 * @author Raj Mehra
 * Created by rajme on 1/21/2017.
 */
public class Building {
    /**
     * The Building id.
     */
    String buildingId, /**
     * The Building name.
     */
    buildingName;

    /**
     * Instantiates a new Building.
     *
     * @param buildingId   the building id
     * @param buildingName the building name
     */
    public Building(String buildingId, String buildingName) {
        this.buildingId = buildingId;
        this.buildingName = buildingName;
    }

    /**
     * Instantiates a new Building.
     */
    public Building() {
    }

    /**
     * Gets building id.
     *
     * @return the building id
     */
    public String getBuildingId() {
        return buildingId;
    }

    /**
     * Sets building id.
     *
     * @param buildingId the building id
     */
    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    /**
     * Gets building name.
     *
     * @return the building name
     */
    public String getBuildingName() {
        return buildingName;
    }

    /**
     * Sets building name.
     *
     * @param buildingName the building name
     */
    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }
}
