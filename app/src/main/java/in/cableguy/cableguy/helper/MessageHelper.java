package in.cableguy.cableguy.helper;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by rajme on 1/21/2017.
 */
public class MessageHelper {
    /**
     * The Context.
     */
    Context context;

    /**
     * Instantiates a new Message helper.
     *
     * @param context the context
     */
    public MessageHelper(Context context) {
        super();
        this.context = context;
    }

    /**
     * Display short.
     *
     * @param s the s
     */
    public void displayShort(String s){
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
    }

    /**
     * Display long.
     *
     * @param s the s
     */
    public void displayLong(String s){
        Toast.makeText(context,s, Toast.LENGTH_LONG).show();
    }
}
