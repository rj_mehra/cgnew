package in.cableguy.cableguy.helper;

/**
 * Created by rajme on 1/21/2017.
 */
public class Lco {
    /**
     * The Lco name.
     */
    String lcoName, /**
     * The Lco id.
     */
    lcoId, /**
     * The Lco address.
     */
    lcoAddress, /**
     * The Lco contact.
     */
    lcoContact, /**
     * The Lco contact 2.
     */
    lcoContact2;

    /**
     * Gets lco contact 2.
     *
     * @return the lco contact 2
     */
    public String getLcoContact2() {
        return lcoContact2;
    }

    /**
     * Sets lco contact 2.
     *
     * @param lcoContact2 the lco contact 2
     */
    public void setLcoContact2(String lcoContact2) {
        this.lcoContact2 = lcoContact2;
    }

    /**
     * Gets lco name.
     *
     * @return the lco name
     */
    public String getLcoName() {
        return lcoName;
    }

    /**
     * Instantiates a new Lco.
     *
     * @param lcoName the lco name
     * @param lcoId   the lco id
     */
    public Lco(String lcoName,String lcoId) {
        this.lcoName = lcoName;
        this.lcoId= lcoId;
    }

    /**
     * Sets lco name.
     *
     * @param lcoName the lco name
     */
    public void setLcoName(String lcoName) {
        this.lcoName = lcoName;
    }

    /**
     * Gets lco id.
     *
     * @return the lco id
     */
    public String getLcoId() {
        return lcoId;
    }

    /**
     * Sets lco id.
     *
     * @param lcoId the lco id
     */
    public void setLcoId(String lcoId) {
        this.lcoId = lcoId;
    }

    /**
     * Gets lco address.
     *
     * @return the lco address
     */
    public String getLcoAddress() {
        return lcoAddress;
    }

    /**
     * Sets lco address.
     *
     * @param lcoAddress the lco address
     */
    public void setLcoAddress(String lcoAddress) {
        this.lcoAddress = lcoAddress;
    }

    /**
     * Gets lco contact.
     *
     * @return the lco contact
     */
    public String getLcoContact() {
        return lcoContact;
    }

    /**
     * Sets lco contact.
     *
     * @param lcoContact the lco contact
     */
    public void setLcoContact(String lcoContact) {
        this.lcoContact = lcoContact;
    }
}
