package in.cableguy.cableguy.helper;

/**
 * Created by rajme on 1/21/2017.
 */
public class UserDetails {
    private String userName,token;

    private String password;
    private String lcoId,lcoName,lcoAddress,menu, phone1,phone2, username, loginTime;

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets login time.
     *
     * @return the login time
     */
    public String getLoginTime() {
        return loginTime;
    }

    /**
     * Sets login time.
     *
     * @param loginTime the login time
     */
    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    /**
     * Gets phone 1.
     *
     * @return the phone 1
     */
    public String getPhone1() {
        return phone1;
    }

    /**
     * Sets phone 1.
     *
     * @param phone1 the phone 1
     */
    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    /**
     * Gets phone 2.
     *
     * @return the phone 2
     */
    public String getPhone2() {
        return phone2;
    }

    /**
     * Sets phone 2.
     *
     * @param phone2 the phone 2
     */
    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    /**
     * Instantiates a new User details.
     */
    public UserDetails(){

    }

    /**
     * Instantiates a new User details.
     *
     * @param userName the user name
     * @param token    the token
     * @param lcoId    the lco id
     */
    public UserDetails(String userName, String token, String lcoId) {
        this.userName = userName;
        this.token = token;
        this.lcoId = lcoId;
    }

    /**
     * Gets menu.
     *
     * @return the menu
     */
    public String getMenu() {
        return menu;
    }

    /**
     * Sets menu.
     *
     * @param menu the menu
     */
    public void setMenu(String menu) {
        this.menu = menu;
    }

    /**
     * Gets lco name.
     *
     * @return the lco name
     */
    public String getLcoName() {
        return lcoName;
    }


    /**
     * Sets lco name.
     *
     * @param lcoName the lco name
     */
    public void setLcoName(String lcoName) {
        this.lcoName = lcoName;
    }

    /**
     * Gets lco address.
     *
     * @return the lco address
     */
    public String getLcoAddress() {
        return lcoAddress;
    }

    /**
     * Sets lco address.
     *
     * @param lcoAddress the lco address
     */
    public void setLcoAddress(String lcoAddress) {
        this.lcoAddress = lcoAddress;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets lco id.
     *
     * @return the lco id
     */
    public String getLcoId() {
        return lcoId;
    }

    /**
     * Sets lco id.
     *
     * @param lcoId the lco id
     */
    public void setLcoId(String lcoId) {
        this.lcoId = lcoId;
    }

    /**
     * Gets user name.
     *
     * @return the user name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets user name.
     *
     * @param userName the user name
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets token.
     *
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets token.
     *
     * @param token the token
     */
    public void setToken(String token) {
        this.token = token;
    }
}
