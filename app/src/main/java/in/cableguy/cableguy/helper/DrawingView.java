package in.cableguy.cableguy.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import in.cableguy.cableguy.constants.Values;

/**
 * Created by Raj on 24-Mar-17.
 */
public class DrawingView extends View {
    /**
     * The X.
     */
    public float x, /**
     * The Y.
     */
    y;
    /**
     * The Width.
     */
    public int width;
    /**
     * The Height.
     */
    public  int height;
    private Bitmap mBitmap= Values.mBitmap;
    private Canvas mCanvas=Values.mCanvas;
    private Path mPath=new Path();
    private Paint mBitmapPaint;
    /**
     * The Context.
     */
    Context context;
    private Paint circlePaint;
    private Path circlePath;
    private Paint mPaint=new Paint();
    private CustomViewListener customViewListener;

    /**
     * Instantiates a new Drawing view.
     *
     * @param c the c
     */
    public DrawingView(Context c) {
        super(c);
        context=c;

    }

    /**
     * Instantiates a new Drawing view.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        circlePaint = new Paint();
        circlePath = new Path();
        circlePaint.setAntiAlias(true);
        circlePaint.setColor(Color.BLUE);
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setStrokeJoin(Paint.Join.MITER);
        circlePaint.setStrokeWidth(2f);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(5);
    }

    /**
     * Gets custom view listener.
     *
     * @return the custom view listener
     */
    public CustomViewListener getCustomViewListener() {
        return customViewListener;
    }

    /**
     * Sets custom view listener.
     *
     * @param customViewListener the custom view listener
     */
    public void setCustomViewListener(CustomViewListener customViewListener) {
        this.customViewListener = customViewListener;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap( mBitmap, 0, 0, mBitmapPaint);
        canvas.drawPath( mPath,  mPaint);
        canvas.drawPath( circlePath,  circlePaint);
        if(getCustomViewListener()!=null){
            getCustomViewListener().onUpdateValue(Values.coordinates);
            getCustomViewListener().plotCoordinates(x, y);
        }
    }

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private void touch_start(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
            mX = x;
            mY = y;
            circlePath.reset();
            circlePath.addCircle(mX, mY, 30, Path.Direction.CW);
        }
    }

    private void touch_up() {
        mPath.lineTo(mX, mY);
        circlePath.reset();
        // commit the path to our offscreen
        mCanvas.drawPath(mPath,  mPaint);
        // kill this so we don't double draw
        mPath.reset();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        this.x=x;
        this.y=y;
        Values.coordinates=x+" , "+y;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up();
                invalidate();
                break;
        }
        return true;
    }

    /**
     * The interface Custom view listener.
     */
    public interface CustomViewListener {
        /**
         * On update value.
         *
         * @param updatedValue the updated value
         */
        void onUpdateValue(String updatedValue);

        /**
         * Plot coordinates.
         *
         * @param x the x
         * @param y the y
         */
        void plotCoordinates(Float x, Float y);
    }

}
