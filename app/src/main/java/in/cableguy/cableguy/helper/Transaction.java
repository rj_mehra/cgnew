package in.cableguy.cableguy.helper;

/**
 * Created by rajme on 1/21/2017.
 */
public class Transaction {
    private String subscriberNumber,subscriberName,amount,receiptNo,dateTime,paymentMode;

    /**
     * Gets subscriber number.
     *
     * @return the subscriber number
     */
    public String getSubscriberNumber() {
        return subscriberNumber;
    }

    /**
     * Sets subscriber number.
     *
     * @param subscriberNumber the subscriber number
     */
    public void setSubscriberNumber(String subscriberNumber) {
        this.subscriberNumber = subscriberNumber;
    }

    /**
     * Gets subscriber name.
     *
     * @return the subscriber name
     */
    public String getSubscriberName() {
        return subscriberName;
    }

    /**
     * Sets subscriber name.
     *
     * @param subscriberName the subscriber name
     */
    public void setSubscriberName(String subscriberName) {
        this.subscriberName = subscriberName;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Sets amount.
     *
     * @param amount the amount
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * Gets receipt no.
     *
     * @return the receipt no
     */
    public String getReceiptNo() {
        return receiptNo;
    }

    /**
     * Sets receipt no.
     *
     * @param receiptNo the receipt no
     */
    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    /**
     * Gets date time.
     *
     * @return the date time
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * Sets date time.
     *
     * @param dateTime the date time
     */
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * Gets payment mode.
     *
     * @return the payment mode
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * Sets payment mode.
     *
     * @param paymentMode the payment mode
     */
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }
}
