package in.cableguy.cableguy.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by rajme on 1/21/2017.
 */
public class LocalStore {
    private SharedPreferences sharedPreferences;
    private String userDetails="USERDETAILS";
    private String userName="USRNAME";
    private String token="TOKEN";
    private String lcoId="LCOID";
    private String password="PASSWORD";
    private String lcoName="LCONAME";
    private String lcoAddress="LCOADDR";
    private String menuType="MENU";

    /**
     * Instantiates a new Local store.
     *
     * @param context the context
     */
    public LocalStore(Context context){
        sharedPreferences=context.getSharedPreferences(userDetails, 0);
    }

    /**
     * Get user details user details.
     *
     * @return the user details
     */
    public UserDetails getUserDetails(){
        UserDetails userDetails=new UserDetails();
        userDetails.setUserName(sharedPreferences.getString(userName,"NO USRNAME"));
        userDetails.setToken(sharedPreferences.getString(token,"NO TOKEN"));
        userDetails.setLcoId(sharedPreferences.getString(lcoId,"NO LOCID"));
        userDetails.setPassword(sharedPreferences.getString(password,"NO PASSWORD"));
        userDetails.setLcoName(sharedPreferences.getString(lcoName,"NO LCO NAME"));
        userDetails.setLcoAddress(sharedPreferences.getString(lcoAddress,"NO LCO ADDR"));
        userDetails.setMenu(sharedPreferences.getString(menuType,"NO MENU"));
        return userDetails;

    }

    /**
     * Store user details.
     *
     * @param userDetails the user details
     */
    public void storeUserDetails(UserDetails userDetails){
        SharedPreferences.Editor peditor = sharedPreferences.edit();
        peditor.putString(userName, userDetails.getUserName());
        peditor.putString(token, userDetails.getToken());
        peditor.putString(lcoId,userDetails.getLcoId());
        peditor.putString(password,userDetails.getPassword());
        peditor.putString(lcoName,userDetails.getLcoName());
        peditor.putString(lcoAddress,userDetails.getLcoAddress());
        peditor.putString(menuType,userDetails.getMenu());
        peditor.commit();
    }
}
