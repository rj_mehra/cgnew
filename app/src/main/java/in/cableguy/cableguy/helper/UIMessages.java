package in.cableguy.cableguy.helper;

/**
 * Created by rajme on 1/21/2017.
 */
public class UIMessages {
    /**
     * The constant INCOMPLETE_INFO.
     */
    public static String INCOMPLETE_INFO="Kindly, fill in all the values";
    /**
     * The constant INVALID_CRED.
     */
    public static String INVALID_CRED="Invalid login credentials";

    /**
     * The constant SERVER_ERROR.
     */
    public static String SERVER_ERROR="Something went wrong. Kindly, retry.";

    /**
     * The constant PAID_AMT_LESS.
     */
    public static String PAID_AMT_LESS="Paid Amount must be less than or equal to ";
    /**
     * The constant AGENT_CONFLICT.
     */
    public static String AGENT_CONFLICT="Either this phone has been assigned to someone" +
            " or else you have been assigned a different phone.Kindly, contact the administrator";

    /**
     * The constant PERMS_REQUIRED.
     */
    public static String PERMS_REQUIRED ="We need read device and location permissions to go ahead";

    /**
     * The constant PERM_READ_REQD.
     */
    public static String PERM_READ_REQD="We need read device permission to go ahead";

    /**
     * The constant GPS_ENABLE_MESSAGE.
     */
    public static String GPS_ENABLE_MESSAGE="Kindly, switch on GPS and Login";
}
