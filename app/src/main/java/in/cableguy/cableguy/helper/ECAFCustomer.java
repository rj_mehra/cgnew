package in.cableguy.cableguy.helper;

/**
 * Created by rajme on 3/26/2017.
 */
public class ECAFCustomer {
    /**
     * The In flag.
     */
    String in_flag, /**
     * The In subsid.
     */
    in_subsid, /**
     * The In subscd.
     */
    in_subscd, /**
     * The In subsname.
     */
    in_subsname, /**
     * The In operid.
     */
    in_operid,
    /**
     * The In address 1.
     */
    in_address1, /**
     * The In bldgid.
     */
    in_bldgid, /**
     * The In pin.
     */
    in_pin, /**
     * The In phone 1.
     */
    in_phone1,
    /**
     * The In phone 2.
     */
    in_phone2, /**
     * The In email.
     */
    in_email, /**
     * The In stid.
     */
    in_stid, /**
     * The In ctcd.
     */
    in_ctcd,
    /**
     * The In locid.
     */
    in_locid, /**
     * The In substype rc.
     */
    in_substypeRC, /**
     * The In road.
     */
    in_road, /**
     * The In plan.
     */
    in_plan,
    /**
     * The In planrate.
     */
    in_planrate, /**
     * The In connamt.
     */
    in_connamt, /**
     * The In flatno.
     */
    in_flatno, /**
     * The In stb no.
     */
    in_STBNo, /**
     * The In smartcard.
     */
    in_Smartcard,
    /**
     * The In addressproof.
     */
    in_addressproof, /**
     * The In idproof.
     */
    in_idproof, /**
     * The In photoproof.
     */
    in_photoproof, /**
     * The In sign.
     */
    in_sign, /**
     * The In geolocation.
     */
    in_geolocation, /**
     * The Stb charges.
     */
    stbCharges;

    /**
     * Gets stb charges.
     *
     * @return the stb charges
     */
    public String getStbCharges() {
        return stbCharges;
    }

    /**
     * Sets stb charges.
     *
     * @param stbCharges the stb charges
     */
    public void setStbCharges(String stbCharges) {
        this.stbCharges = stbCharges;
    }

    /**
     * Instantiates a new Ecaf customer.
     */
    public ECAFCustomer() {
    }


    /**
     * Gets in flag.
     *
     * @return the in flag
     */
    public String getIn_flag() {
        return in_flag;
    }

    /**
     * Sets in flag.
     *
     * @param in_flag the in flag
     */
    public void setIn_flag(String in_flag) {
        this.in_flag = in_flag;
    }

    /**
     * Gets in subsid.
     *
     * @return the in subsid
     */
    public String getIn_subsid() {
        return in_subsid;
    }

    /**
     * Sets in subsid.
     *
     * @param in_subsid the in subsid
     */
    public void setIn_subsid(String in_subsid) {
        this.in_subsid = in_subsid;
    }

    /**
     * Gets in subscd.
     *
     * @return the in subscd
     */
    public String getIn_subscd() {
        return in_subscd;
    }

    /**
     * Sets in subscd.
     *
     * @param in_subscd the in subscd
     */
    public void setIn_subscd(String in_subscd) {
        this.in_subscd = in_subscd;
    }

    /**
     * Gets in subsname.
     *
     * @return the in subsname
     */
    public String getIn_subsname() {
        return in_subsname;
    }

    /**
     * Sets in subsname.
     *
     * @param in_subsname the in subsname
     */
    public void setIn_subsname(String in_subsname) {
        this.in_subsname = in_subsname;
    }

    /**
     * Gets in operid.
     *
     * @return the in operid
     */
    public String getIn_operid() {
        return in_operid;
    }

    /**
     * Sets in operid.
     *
     * @param in_operid the in operid
     */
    public void setIn_operid(String in_operid) {
        this.in_operid = in_operid;
    }

    /**
     * Gets in address 1.
     *
     * @return the in address 1
     */
    public String getIn_address1() {
        return in_address1;
    }

    /**
     * Sets in address 1.
     *
     * @param in_address1 the in address 1
     */
    public void setIn_address1(String in_address1) {
        this.in_address1 = in_address1;
    }

    /**
     * Gets in bldgid.
     *
     * @return the in bldgid
     */
    public String getIn_bldgid() {
        return in_bldgid;
    }

    /**
     * Sets in bldgid.
     *
     * @param in_bldgid the in bldgid
     */
    public void setIn_bldgid(String in_bldgid) {
        this.in_bldgid = in_bldgid;
    }

    /**
     * Gets in pin.
     *
     * @return the in pin
     */
    public String getIn_pin() {
        return in_pin;
    }

    /**
     * Sets in pin.
     *
     * @param in_pin the in pin
     */
    public void setIn_pin(String in_pin) {
        this.in_pin = in_pin;
    }

    /**
     * Gets in phone 1.
     *
     * @return the in phone 1
     */
    public String getIn_phone1() {
        return in_phone1;
    }

    /**
     * Sets in phone 1.
     *
     * @param in_phone1 the in phone 1
     */
    public void setIn_phone1(String in_phone1) {
        this.in_phone1 = in_phone1;
    }

    /**
     * Gets in phone 2.
     *
     * @return the in phone 2
     */
    public String getIn_phone2() {
        return in_phone2;
    }

    /**
     * Sets in phone 2.
     *
     * @param in_phone2 the in phone 2
     */
    public void setIn_phone2(String in_phone2) {
        this.in_phone2 = in_phone2;
    }

    /**
     * Gets in email.
     *
     * @return the in email
     */
    public String getIn_email() {
        return in_email;
    }

    /**
     * Sets in email.
     *
     * @param in_email the in email
     */
    public void setIn_email(String in_email) {
        this.in_email = in_email;
    }

    /**
     * Gets in stid.
     *
     * @return the in stid
     */
    public String getIn_stid() {
        return in_stid;
    }

    /**
     * Sets in stid.
     *
     * @param in_stid the in stid
     */
    public void setIn_stid(String in_stid) {
        this.in_stid = in_stid;
    }

    /**
     * Gets in ctcd.
     *
     * @return the in ctcd
     */
    public String getIn_ctcd() {
        return in_ctcd;
    }

    /**
     * Sets in ctcd.
     *
     * @param in_ctcd the in ctcd
     */
    public void setIn_ctcd(String in_ctcd) {
        this.in_ctcd = in_ctcd;
    }

    /**
     * Gets in locid.
     *
     * @return the in locid
     */
    public String getIn_locid() {
        return in_locid;
    }

    /**
     * Sets in locid.
     *
     * @param in_locid the in locid
     */
    public void setIn_locid(String in_locid) {
        this.in_locid = in_locid;
    }

    /**
     * Gets in substype rc.
     *
     * @return the in substype rc
     */
    public String getIn_substypeRC() {
        return in_substypeRC;
    }

    /**
     * Sets in substype rc.
     *
     * @param in_substypeRC the in substype rc
     */
    public void setIn_substypeRC(String in_substypeRC) {
        this.in_substypeRC = in_substypeRC;
    }

    /**
     * Gets in road.
     *
     * @return the in road
     */
    public String getIn_road() {
        return in_road;
    }

    /**
     * Sets in road.
     *
     * @param in_road the in road
     */
    public void setIn_road(String in_road) {
        this.in_road = in_road;
    }

    /**
     * Gets in plan.
     *
     * @return the in plan
     */
    public String getIn_plan() {
        return in_plan;
    }

    /**
     * Sets in plan.
     *
     * @param in_plan the in plan
     */
    public void setIn_plan(String in_plan) {
        this.in_plan = in_plan;
    }

    /**
     * Gets in planrate.
     *
     * @return the in planrate
     */
    public String getIn_planrate() {
        return in_planrate;
    }

    /**
     * Sets in planrate.
     *
     * @param in_planrate the in planrate
     */
    public void setIn_planrate(String in_planrate) {
        this.in_planrate = in_planrate;
    }

    /**
     * Gets in connamt.
     *
     * @return the in connamt
     */
    public String getIn_connamt() {
        return in_connamt;
    }

    /**
     * Sets in connamt.
     *
     * @param in_connamt the in connamt
     */
    public void setIn_connamt(String in_connamt) {
        this.in_connamt = in_connamt;
    }

    /**
     * Gets in flatno.
     *
     * @return the in flatno
     */
    public String getIn_flatno() {
        return in_flatno;
    }

    /**
     * Sets in flatno.
     *
     * @param in_flatno the in flatno
     */
    public void setIn_flatno(String in_flatno) {
        this.in_flatno = in_flatno;
    }

    /**
     * Gets in stb no.
     *
     * @return the in stb no
     */
    public String getIn_STBNo() {
        return in_STBNo;
    }

    /**
     * Sets in stb no.
     *
     * @param in_STBNo the in stb no
     */
    public void setIn_STBNo(String in_STBNo) {
        this.in_STBNo = in_STBNo;
    }

    /**
     * Gets in smartcard.
     *
     * @return the in smartcard
     */
    public String getIn_Smartcard() {
        return in_Smartcard;
    }

    /**
     * Sets in smartcard.
     *
     * @param in_Smartcard the in smartcard
     */
    public void setIn_Smartcard(String in_Smartcard) {
        this.in_Smartcard = in_Smartcard;
    }

    /**
     * Gets in addressproof.
     *
     * @return the in addressproof
     */
    public String getIn_addressproof() {
        return in_addressproof;
    }

    /**
     * Sets in addressproof.
     *
     * @param in_addressproof the in addressproof
     */
    public void setIn_addressproof(String in_addressproof) {
        this.in_addressproof = in_addressproof;
    }

    /**
     * Gets in idproof.
     *
     * @return the in idproof
     */
    public String getIn_idproof() {
        return in_idproof;
    }

    /**
     * Sets in idproof.
     *
     * @param in_idproof the in idproof
     */
    public void setIn_idproof(String in_idproof) {
        this.in_idproof = in_idproof;
    }

    /**
     * Gets in photoproof.
     *
     * @return the in photoproof
     */
    public String getIn_photoproof() {
        return in_photoproof;
    }

    /**
     * Sets in photoproof.
     *
     * @param in_photoproof the in photoproof
     */
    public void setIn_photoproof(String in_photoproof) {
        this.in_photoproof = in_photoproof;
    }

    /**
     * Gets in sign.
     *
     * @return the in sign
     */
    public String getIn_sign() {
        return in_sign;
    }

    /**
     * Sets in sign.
     *
     * @param in_sign the in sign
     */
    public void setIn_sign(String in_sign) {
        this.in_sign = in_sign;
    }

    /**
     * Gets in geolocation.
     *
     * @return the in geolocation
     */
    public String getIn_geolocation() {
        return in_geolocation;
    }

    /**
     * Sets in geolocation.
     *
     * @param in_geolocation the in geolocation
     */
    public void setIn_geolocation(String in_geolocation) {
        this.in_geolocation = in_geolocation;
    }
}
