package in.cableguy.cableguy.helper;

import android.app.Application;

import com.prowesspride.api.Setup;

import in.cableguy.cableguy.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by rajme on 1/21/2017.
 */
public class GlobalApp extends Application {
    /**
     * The constant setup.
     */
    public static Setup setup = null;
    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/segoeui.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        //....
    }

}
