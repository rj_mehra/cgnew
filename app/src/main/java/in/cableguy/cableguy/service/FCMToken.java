package in.cableguy.cableguy.service;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * The type Fcm token.
 */
public class FCMToken extends FirebaseInstanceIdService {
    /**
     * Instantiates a new Fcm token.
     */
    public FCMToken() {
    }

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("FCM", "Refreshed token: " + refreshedToken);

        Log.e("Refreshed Token ",refreshedToken);
    }
}
