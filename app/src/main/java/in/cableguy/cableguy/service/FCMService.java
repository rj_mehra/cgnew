package in.cableguy.cableguy.service;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * The type Fcm service.
 */
public class FCMService extends FirebaseMessagingService {
    /**
     * Instantiates a new Fcm service.
     */
    public FCMService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.d("FCM Server", "From: " + remoteMessage.getFrom());
        Log.d("FCM Message", "Notification Message Body: " + remoteMessage.getNotification().getBody());
    }
}
