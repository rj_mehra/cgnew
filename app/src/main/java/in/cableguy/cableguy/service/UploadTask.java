package in.cableguy.cableguy.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.helper.OfflineDBHelper;

/**
 * The type Upload task.
 */
public class UploadTask extends Service {
    /**
     * The Tag.
     */
    public String TAG = "ConnectionReciever";
    /**
     * The Db helper.
     */
    OfflineDBHelper dbHelper;
    /**
     * The Count.
     */
    long count;

    /**
     * Instantiates a new Upload task.
     */
    public UploadTask() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


        private class uploadOfflineTask extends AsyncTask<String, Void, JSONObject> {
            /**
             * The Is.
             */
            InputStream is = null;
            /**
             * The Result.
             */
            String result = "";
            /**
             * The Json.
             */
            String json = "";

            /**
             * The Sb.
             */
            StringBuilder sb = new StringBuilder();


            /**
             * The Username off.
             */
            String usernameOff, /**
             * The Subs name.
             */
            subsName, /**
             * The Subs code.
             */
            subsCode, /**
             * The Paid amount.
             */
            paidAmount, /**
             * The Add charges.
             */
            addCharges, /**
             * The Mobile.
             */
            mobile, /**
             * The Remark.
             */
            remark, /**
             * The Pay mode.
             */
            payMode, /**
             * The Bank name.
             */
            bankName, /**
             * The Cheque date.
             */
            chequeDate, /**
             * The Cheque no.
             */
            chequeNo;

            @Override
            protected JSONObject doInBackground(String... params) {
                URL url;
                usernameOff = params[10];
                subsName = params[0];
                subsCode = params[1];
                paidAmount = params[2];
                addCharges = params[3];
                mobile = params[4];
                remark = params[5];
                payMode = params[6];
                bankName = params[7];
                chequeDate = params[8];
                chequeNo = params[9];
                try {
                    url = new URL(Constants.OFFLINE_API + "Username=" + usernameOff + "&SubsName=" + subsName + "&SubsCode=" + subsCode + "&PayMode=" + payMode
                            + "&Amount=" + paidAmount + "&AddiCharges=" + addCharges + "&Mobile=" + mobile + "&Remark=" + remark
                            + "&ChqDt=" + chequeDate + "&ChqNo=" + chequeNo + "&BankName=" + bankName);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                    conn.setRequestMethod("GET");
                    conn.connect();
                    Log.e("conn", ":" + conn);
                    int statuscode = conn.getResponseCode();
                    Log.e("statuscode", "" + statuscode);
                    Log.e("response", conn.getResponseMessage());
                    if (statuscode == 200) {
                        is = new BufferedInputStream(conn.getInputStream());
                    } else
                        is = new BufferedInputStream(conn.getErrorStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            is, "iso-8859-1"), 8);
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    is.close();

                    json = sb.toString();
                    //Log.d("JValue","value 1 "+json);
                    json = json.replace("\"", "");
                    Log.d("JValue", "value 2" + json);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                JSONObject jobj = new JSONObject();
                try {
                    jobj = new JSONObject(json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return jobj;
            }

            @Override
            protected void onPostExecute(JSONObject json) {
                String status;
                JSONArray dataArray;
                try {
                    status = json.getString("STATUS");
                    dataArray = json.getJSONArray("Data");
                    switch (status) {
                        case "0":
                            try{
                                Toast.makeText(getApplicationContext(), "All pending offline task has been uploaded, Thank you", Toast.LENGTH_SHORT).show();

                            }catch (Exception ex){
                                Log.e(" Service Error ", ex.toString());
                            }
                            break;
                        case "1":
                            try{
                                Toast.makeText(getApplicationContext(), "Some error occurred uploading offline data, please login again or try later", Toast.LENGTH_SHORT).show();

                            }catch (Exception ex){
                                Log.e(" Service Error ", ex.toString());
                            }
                            break;
                        default:
                            try{
                                Toast.makeText(getApplicationContext(), "Some error occurred uploading offline data, please login again or try later", Toast.LENGTH_SHORT).show();

                            }catch (Exception ex){
                                Log.e(" Service Error ", ex.toString());
                            }
                            break;
                    }
                } catch (Exception ex) {
                }
            }
        }

}
