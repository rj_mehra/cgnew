package in.cableguy.cableguy.pg;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import com.billdesk.sdk.LibraryPaymentStatusProtocol;

import in.cableguy.cableguy.ui.StatusActivity;

/**
 * Created by Raj on 04-05-2017.
 */
public class CallBack implements LibraryPaymentStatusProtocol, Parcelable {
    /**
     * The Tag.
     */
    String TAG = "Callback ::: > ";

    /**
     * Instantiates a new Call back.
     */
    public CallBack() {
        Log.v(TAG, "CallBack()....");
    }

    /**
     * Instantiates a new Call back.
     *
     * @param in the in
     */
    public CallBack(Parcel in) {
        Log.v(TAG, "CallBack(Parcel in)....");
    }

    @Override
    public void paymentStatus(String status, Activity context) {
        Log.v(TAG,
                "paymentStatus(String status, Activity context)....::::status:::::"
                        + status);
/*        Toast.makeText(context, "PG Response:: " + status, Toast.LENGTH_LONG)
                .show();*/

        Intent mIntent = new Intent(context, StatusActivity.class);
        mIntent.putExtra("status", status);
        context.startActivity(mIntent);
        context.finish();
    }

    @Override
    public int describeContents() {
        Log.v(TAG, "describeContents()....");
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Log.v(TAG, "writeToParcel(Parcel dest, int flags)....");
        // TODO Auto-generated method stub
    }

    /**
     * The constant CREATOR.
     */
    @SuppressWarnings("rawtypes")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        String TAG = "Callback --- Parcelable.Creator ::: > ";

        @Override
        public CallBack createFromParcel(Parcel in) {
            Log.v(TAG, "CallBackActivity createFromParcel(Parcel in)....");
            return new CallBack(in);
        }

        @Override
        public Object[] newArray(int size) {
            Log.v(TAG, "Object[] newArray(int size)....");
            return new CallBack[size];
        }
    };
}
