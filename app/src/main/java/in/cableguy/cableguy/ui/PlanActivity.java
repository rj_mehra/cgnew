package in.cableguy.cableguy.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.adapter.PlanListAdapter;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.ECAFCustomer;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.MessageHelper;
import in.cableguy.cableguy.helper.Plan;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author Raj Mehra
 * The type Plan activity.
 *
 * This activity lists all the plans
 */
public class PlanActivity extends AppCompatActivity {
    /**
     * The Adapter.
     */
    PlanListAdapter adapter;
    /**
     * The Plan list view.
     */
    ListView planListView;
    /**
     * The Plan list.
     */
    ArrayList<String> planList = new ArrayList<String>();
    /**
     * The Plans.
     */
    ArrayList<Plan> plans = new ArrayList<>();
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Message.
     */
    MessageHelper message;
    /**
     * The Ecaf customer.
     */
    ECAFCustomer ecafCustomer;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        overridePendingTransition(0, 0);
        getSupportActionBar().setTitle("Select Plan");
        ecafCustomer = new ECAFCustomer();
        localStore=new LocalStore(PlanActivity.this);
        message=new MessageHelper(PlanActivity.this);
        adapter = new PlanListAdapter(PlanActivity.this, planList);
        planListView = (ListView)findViewById(R.id.planList);
        planListView.setAdapter(adapter);
        progressDialog = new ProgressDialog(PlanActivity.this);
        planListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Values.selectedPlan=plans.get(position);
                Log.e("Selected Plan",Values.selectedPlan.toString());
                AlertDialog.Builder dialog=new AlertDialog.Builder(PlanActivity.this);
                dialog.setMessage("Do you want to select "+ Values.selectedPlan.getName()+" ?");
                dialog.setTitle("Plan Selection");
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ConnectivityManager cm = (ConnectivityManager) PlanActivity.this.
                                getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = cm.getActiveNetworkInfo();
                        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                            //Connectivity
                            if(Values.MODULE.equalsIgnoreCase("KYCI")){
                                Log.e("Values module insert",Values.MODULE);
                                Values.ECAF_CUSTOMER.setIn_plan(Values.selectedPlan.getCode());
                                Values.ECAF_CUSTOMER.setIn_planrate(Values.selectedPlan.getRate());
                                new InsertEcafCustomer().execute();
                            }
                            else{
                                new SubmitPlanChange().execute();
                            }

                        }
                        else{
                            //No connectivity
                            message.displayShort("No internet connectivity");

                        }
                    }
                });
                dialog.show();

            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(PlanActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        ConnectivityManager cm = (ConnectivityManager) PlanActivity.this.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            //Connectivity
            new FetchPlans().execute();
        }
        else{
            //No connectivity
            message.displayShort("No internet connectivity");

        }

    }

    /**
     * Background task for inserting ECAF customer using the plan data which is applicable for new customers
     */

    private class InsertEcafCustomer extends AsyncTask<String,Void,JSONObject>{
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(PlanActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                String uri = Constants.ECAF_SUBSCRIBER+"username="+localStore.getUserDetails().getUserName()+"&in_flag=I"+"&in_subsid="+Values.ECAF_CUSTOMER.getIn_subsid()+
                        "&in_subscd="+Values.ECAF_CUSTOMER.getIn_subscd()+"&in_subsname="+Values.ECAF_CUSTOMER.getIn_subsname()+"&in_operid="+Values.selectedLcoId+"" +
                        "&in_address1="+Values.ECAF_CUSTOMER.getIn_address1()+"&in_bldgid="+Values.ECAF_CUSTOMER.getIn_bldgid()+"&in_pin=" +Values.ECAF_CUSTOMER.getIn_pin()+
                        "&in_phone1="+Values.ECAF_CUSTOMER.getIn_phone1()+"&in_phone2="+Values.ECAF_CUSTOMER.getIn_phone2()+"" +
                        "&in_email="+Values.ECAF_CUSTOMER.getIn_email()+"&in_stid="+Values.ECAF_CUSTOMER.getIn_stid()+"" +
                        "&in_ctcd=" +Values.ECAF_CUSTOMER.getIn_ctcd()+"&in_locid="+Values.ECAF_CUSTOMER.getIn_locid()+"&in_substypeRC="+Values.ECAF_CUSTOMER.getIn_substypeRC()+
                        "&in_road="+Values.ECAF_CUSTOMER.getIn_road()+"&in_plan="+Values.ECAF_CUSTOMER.getIn_plan()+"&in_planrate=" +Values.ECAF_CUSTOMER.getIn_planrate()+
                        "&in_connamt="+Values.ECAF_CUSTOMER.getIn_connamt()+"&in_flatno="+Values.ECAF_CUSTOMER.getIn_flatno()+"" +
                        "&in_STBNo="+Values.ECAF_CUSTOMER.getIn_STBNo()+"&in_Smartcard=" +Values.ECAF_CUSTOMER.getIn_Smartcard()+
                        "&in_addressproof="+Values.ECAF_CUSTOMER.getIn_addressproof()+"&in_idproof="+Values.ECAF_CUSTOMER.getIn_idproof()+
                        "&in_photoproof="+Values.ECAF_CUSTOMER.getIn_photoproof()+"&in_sign=" +Values.ECAF_CUSTOMER.getIn_sign()+
                        "&in_geolocation="+Values.ECAF_CUSTOMER.getIn_geolocation()+"&Token="+localStore.getUserDetails().getToken()+"&stbcharges="+Values.ECAF_CUSTOMER.getStbCharges();
                url = new URL(uri.replaceAll(" ","%20"));
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        Intent intent = new Intent(PlanActivity.this, SuccessActivity.class);
                        Values.MODULE ="KYCI";
                        startActivity(intent);
                        finish();
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(PlanActivity.this, "Some error occurred, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(PlanActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
            }finally {
                progressDialog.dismiss();
            }
        }
    }


    /**
     * Background task for submitting plan change
     */

    private class SubmitPlanChange extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(PlanActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try {
                String uri = Constants.SUBMIT_PLAN_CHANGE+"UserName="+localStore.getUserDetails().getUserName()+"" +
                        "&Subsid="+Values.selectedCustomer.getId()+
                        "&PlanId="+Values.selectedPlan.getId()+"&PlanAmount="+Values.selectedPlan.getRate()+"" +
                        "&Token="+localStore.getUserDetails().getToken();
                url = new URL(uri.replaceAll(" ","%20"));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    planList.clear();
                }
            });

            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(PlanActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(PlanActivity.this, "Invalid Login Details or Session Timed out", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(PlanActivity.this, "Cannot connect to the server, please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                progressDialog.dismiss();
            }
        }
    }

    /**
     *
     * Background task for fetching plans
     */

    private class FetchPlans extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(PlanActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try {
                url = new URL(Constants.GET_PLAN_LIST + "&Username=" + localStore.getUserDetails().getUserName() + "&Token=" + localStore.getUserDetails().getToken());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            planList.clear();
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        for(int i=0;i<dataArray.length();i++){
                            final JSONObject obj = dataArray.getJSONObject(i);

                            PlanActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        String planId=obj.getString("PlanID");
                                        String planRate=obj.getString("PlanRate");
                                        String planName=obj.getString("PlanName");
                                        String planCode = obj.getString("PlanCode");
                                        Plan plan=new Plan();
                                        plan.setId(planId);
                                        plan.setName(planName);
                                        plan.setRate(planRate);
                                        plan.setCode(planCode);
                                        plans.add(plan);
                                        planList.add(obj.getString("PlanName")+" - ₹"+planRate);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    adapter.notifyDataSetChanged();
                                }
                            });
                        }
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(PlanActivity.this, "Invalid Login Details or Session Timed out", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(PlanActivity.this, "Cannot connect to the server, please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                progressDialog.dismiss();
            }
        }
    }


    /**
     * @param filePath
     */
    private void beginUpload(String filePath) {
        if (filePath == null) {
            Toast.makeText(this, "Could not find the filepath of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(filePath);
        Log.e("NameofImage", file.getName());
        /*TransferObserver observer = transferUtility.upload(Constants.BUCKET_NAME, file.getName(),
                file);*/
        /*
         * Note that usually we set the transfer listener after initializing the
         * transfer. However it isn't required in this sample app. The flow is
         * click upload button -> start an activity for image selection
         * startActivityForResult -> onActivityResult -> beginUpload -> onResume
         * -> set listeners to in progress transfers.
         */
        // observer.setTransferListener(new UploadListener());
    }
}
