package in.cableguy.cableguy.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.MessageHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author Raj Mehra
 * The type Zone list activity.
 *
 * This activity lists the zones for the LCO
 */
public class ZoneListActivity extends AppCompatActivity {
    private ListView zoneListView;
    /**
     * The Adapter.
     */
    ArrayAdapter<String> adapter;
    /**
     * The Message.
     */
    MessageHelper message;
    /**
     * The Search zone.
     */
    EditText searchZone;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Zone list.
     */
    ArrayList<String> zoneList=new ArrayList<>();
    /**
     * The Zone id list.
     */
    ArrayList<String> zoneIdList = new ArrayList<>();
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone_list);
        overridePendingTransition(0, 0);
        getSupportActionBar().setTitle("Select Zone");
        //String zone[] = {"North Mumbai","South Mumbai","West Mumbai","East Mumbai"};
        zoneListView = (ListView)findViewById(R.id.zoneListView);
        searchZone = (EditText)findViewById(R.id.searchZone);

        adapter = new ArrayAdapter<String>(this,R.layout.list_item,R.id.listItemName,zoneList);
        zoneListView.setAdapter(adapter);
        message=new MessageHelper(ZoneListActivity.this);
        localStore=new LocalStore(ZoneListActivity.this);
        zoneListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = (String)zoneListView.getAdapter().getItem(position);
                //Toast.makeText(ZoneListActivity.this, "You selected "+item, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ZoneListActivity.this,CustomerDetailsActivity.class);
                //intent.putExtra("Zone",item);
                Values.selectedZone=item;
                Values.zoneId = zoneIdList.get(position);
                startActivity(intent);
                finish();
            }
        });

        searchZone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ZoneListActivity.this.adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ZoneListActivity.this,CustomerDetailsActivity.class);
        startActivity(intent);
        finish();
    }
    @Override
    protected void onResume() {
        super.onResume();
        ConnectivityManager cm = (ConnectivityManager) ZoneListActivity.this.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            //Connectivity
            new FetchZones().execute();
        }
        else{
            //No connectivity
            message.displayShort("No internet connectivity");

        }
    }

    /**
     * Background task to fetch zones
     */

    private class FetchZones extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ZoneListActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try {
                if (Values.redirect.equalsIgnoreCase("NormalType")) {
                    url = new URL(Constants.GET_TYPE_DETAILS + "Type=Z" + "&Username=" + localStore.getUserDetails().getUserName() + "&Token=" + localStore.getUserDetails().getToken() + "&lcoid=" + Values.selectedLcoId);
                } else {
                    url = new URL(Constants.GET_TYPE_DETAILS + "Type=Z" + "&Username=" + localStore.getUserDetails().getUserName() + "&Token=" + localStore.getUserDetails().getToken() + "&lcoid=" + Values.selectedLcoId + "&zoneid=" + Values.zoneId + "&areaid=" + Values.areaId + "&buildingid=" + Values.buildingId + "&stateid=" + "&ctid=");
                }

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    zoneList.clear();
                }
            });

            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        for(int i=0;i<dataArray.length();i++){

                            final JSONObject obj = dataArray.getJSONObject(i);
                            ZoneListActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        zoneList.add(obj.getString("ZoneName"));
                                        zoneIdList.add(obj.getString("ZoneId"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    adapter.notifyDataSetChanged();
                                }
                            });

                        }
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Values.selectedZone="No Zone";
                                Toast.makeText(ZoneListActivity.this, "Invalid Login Details or Session Timed out", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ZoneListActivity.this, "Cannot connect to the server, please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                progressDialog.dismiss();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(ZoneListActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
