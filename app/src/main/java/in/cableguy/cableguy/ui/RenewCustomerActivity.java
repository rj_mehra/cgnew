package in.cableguy.cableguy.ui;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.ConvertToISOString;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.Plan;
import in.cableguy.cableguy.helper.UserDetails;

/**
 * @author Raj Mehra
 * The type Renew customer activity.
 *
 * Activity created specially for Amol from Sai cable for renewing customers
 */
public class RenewCustomerActivity extends AppCompatActivity {
    /**
     * The Submit button.
     */
    Button submitButton;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The User details.
     */
    UserDetails userDetails;
    /**
     * The Plans.
     */
    ArrayList<Plan> plans = new ArrayList<>();
    /**
     * The Plan list.
     */
    ArrayList<String> planList = new ArrayList<String>();
    /**
     * The Plan id list.
     */
    ArrayList<String> planIDList = new ArrayList<String>();
    /**
     * The Plan spinner.
     */
    Spinner planSpinner;
    /**
     * The Pick date tv.
     */
    TextView pickDateTV, /**
     * The Subs name tv.
     */
    subsNameTV, /**
     * The Subs add tv.
     */
    subsAddTV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renew_customer);
        submitButton = (Button)findViewById(R.id.submitButton);
        localStore = new LocalStore(RenewCustomerActivity.this);
        planSpinner = (Spinner)findViewById(R.id.planSpinner);
        pickDateTV = (TextView)findViewById(R.id.pickDateTV);
        subsNameTV = (TextView)findViewById(R.id.subsNameTV);
        subsNameTV.setText(Values.selectedCustomer.getName());
        new FetchPlans().execute();
        planSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Values.PLAN_ID = planIDList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new RenewCustomer().execute(pickDateTV.getText().toString());
            }
        });

        pickDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickDate(view);
            }
        });
    }


    /**
     * The Selected date.
     */
    String selectedDate;

    /**
     * Pick date.
     *
     * @param view the view
     */
    public void pickDate(View view) {
        final Calendar c1 = Calendar.getInstance();
        DatePickerDialog datedialog = new DatePickerDialog(RenewCustomerActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(
                            final DatePicker view,
                            int year,
                            int monthOfYear,
                            int dayOfMonth) {
                        final String date1 = year + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + String.format("%02d", dayOfMonth);
                        AlertDialog.Builder dialog = new AlertDialog.Builder(RenewCustomerActivity.this);
                        dialog.setTitle("Date Selection");
                        dialog.setMessage("Do you want to select " + date1 + " ?");
                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                            }
                        });
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                selectedDate = date1;
                                final DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                Date d = new Date();
                                try {
                                    d = format.parse(date1);
                                } catch (Exception e) {
                                }
                                selectedDate = ConvertToISOString.getISO8601StringForDate(d);
                                RenewCustomerActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pickDateTV.setText(date1);
                                    }
                                });
                            }
                        });
                        dialog.show();
                    }

                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DATE));
        datedialog.getDatePicker().setMinDate(c1.getTimeInMillis());
        datedialog.show();
    }

    /**
     * Background task for fetching plans
     */
    private class FetchPlans extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(RenewCustomerActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try {
                url = new URL(Constants.GET_PLAN_LIST + "&Username=" + localStore.getUserDetails().getUserName() + "&Token=" + localStore.getUserDetails().getToken());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    planList.clear();
                    planIDList.clear();
                }
            });
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        for(int i=0;i<dataArray.length();i++){
                            final JSONObject obj = dataArray.getJSONObject(i);

                            RenewCustomerActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        String planId=obj.getString("PlanID");
                                        String planRate=obj.getString("PlanRate");
                                        String planName=obj.getString("PlanName");
                                        String planCode = obj.getString("PlanCode");
                                        Plan plan=new Plan();
                                        plan.setId(planId);
                                        plan.setName(planName);
                                        plan.setRate(planRate);
                                        plan.setCode(planCode);
                                        plans.add(plan);
                                        planIDList.add(obj.getString("PlanID"));
                                        planList.add(obj.getString("PlanName")+" - ₹"+planRate);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(RenewCustomerActivity.this, android.R.layout.simple_spinner_item, planList);
                                    planSpinner.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                }
                            });
                        }
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(RenewCustomerActivity.this, "Invalid Login Details or Session Timed out", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(RenewCustomerActivity.this, "Cannot connect to the server, please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                progressDialog.dismiss();
            }
        }
    }

    /**
     * Background task for renewing customers
     */

    private class RenewCustomer extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(RenewCustomerActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            String pickDate = params[0];
            try {
                String uri = Constants.RENEWAL_API+"Subsid="+ Values.selectedCustomer.getId()+
                        "&Plan_Id="+Values.PLAN_ID+"&LCO_Id="+localStore.getUserDetails().getLcoId()+"" +
                        "&Start_date="+pickDate+"&userId="+localStore.getUserDetails().getUserName();
                url = new URL(uri.replaceAll(" ","%20"));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    planList.clear();
                }
            });

            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(RenewCustomerActivity.this, "Successful", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(RenewCustomerActivity.this, "Bill Already Generated...", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(RenewCustomerActivity.this, "Some error occurred!", Toast.LENGTH_SHORT).show();
                            }
                        });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                progressDialog.dismiss();
            }
        }
    }
}
