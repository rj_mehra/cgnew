package in.cableguy.cableguy.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.btprinter.BTPrinter;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 *
 * @author Raj Mehra
 * The type Splash activity.
 *
 * This activity is the first screen which opens with logo for the app
 */
public class SplashActivity extends AppCompatActivity {
    /**
     * The Bt printer.
     */
    BTPrinter btPrinter;
    /**
     * The Current version.
     */
    String currentVersion;
    /**
     * The Package info.
     */
    PackageInfo packageInfo;
    /**
     * The Ver code.
     */
    int verCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        btPrinter = new BTPrinter(this);
        /**
         *
         * initialize bluetooth connection
         *
         * @see BTPrinter
         */
        new Thread(new Runnable() {
            @Override
            public void run() {
                btPrinter.OpenBTConnection();
            }
        });
        Values.TRANS_MODE="Online";
        Thread t = new Thread() {
            public void run() {
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        };
        t.start();
}
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * Delete file from the phone storage
     *
     * Required for deleting photos taken during ecaf
     *
     * @see DocumentActivity
     *
     */

    private static void deleteStorage() {

        // External sdcard location
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/CG" + "/UploadedImages/");
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                new File(dir, children[i]).delete();
            }
        }
    }

/*
    private class GetVersion extends AsyncTask<String,Void,JSONObject> {
        *//**
         * The Is.
         *//*
        InputStream is = null;
        *//**
         * The Result.
         *//*
        String result = "";
        *//**
         * The Json.
         *//*
        String json = "";

        *//**
         * The Sb.
         *//*
        StringBuilder sb = new StringBuilder();
        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                url = new URL(Constants.GET_VERSION+"AppType=CG");
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);
                *//*json=json.replace("\"","");*//*
                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2 :"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
                Intent intent = new Intent(SplashActivity.this, UpdateActivity.class);
                startActivity(intent);
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");

                JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "0":
                        if(Values.currentVersion.equalsIgnoreCase(obj.getString("VersionNo"))){
                            Thread t = new Thread() {
                                public void run() {
                                    try {
                                        sleep(500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    } finally {
                                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }

                                }
                            };
                            t.start();

                        }else{
                            Intent intent = new Intent(SplashActivity.this, UpdateActivity.class);
                            startActivity(intent);
                        }
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(SplashActivity.this, "Invalid Version Number, Please Update!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Intent intent = new Intent(SplashActivity.this, UpdateActivity.class);
                        startActivity(intent);
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(SplashActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        intent = new Intent(SplashActivity.this, UpdateActivity.class);
                        startActivity(intent);
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
                Intent intent = new Intent(SplashActivity.this, UpdateActivity.class);
                startActivity(intent);
            }
        }
    }*/


}
