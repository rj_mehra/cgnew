package in.cableguy.cableguy.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.appinvite.AppInviteInvitationResult;
import com.google.android.gms.appinvite.AppInviteReferral;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crash.FirebaseCrash;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.MessageHelper;
import in.cableguy.cableguy.helper.OfflineDBHelper;
import in.cableguy.cableguy.helper.UIMessages;
import in.cableguy.cableguy.helper.UserDetails;

/**
 * The type Login activity.
 *
 * @author Raj Mehra
 * @version 1.2.20  This is the code for the login screen, all elements on the screen have events associated which is listed below.
 */
public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    /**
     * The Message.
     */
    MessageHelper message = new MessageHelper(LoginActivity.this);
    /**
     * The Pos.
     */
    Integer pos = 0;
    /**
     * The Change lang tv.
     */
    TextView changeLangTV, /**
     * The Forgot password tv.
     */
    forgotPasswordTV, /**
     * The Version number.
     */
    versionNumber, /**
     * The Info message tv.
     */
    infoMessageTV;
    /**
     * The User name ed.
     */
    EditText userNameED, /**
     * The Password ed.
     */
    passwordED;
    /**
     * The Login button.
     */
    Button loginButton, /**
     * The Contact us button.
     */
    contactUsButton;
    /**
     * The Handler.
     */
    Handler handler;
    /**
     * The Telephony manager.
     */
    TelephonyManager telephonyManager;
    /**
     * The Username.
     */
    String username, /**
     * The Password.
     */
    password, /**
     * The Version.
     */
    version, /**
     * The Imei.
     */
    imei;
    final private static int MY_PERMISSIONS_CALL_PHONE = 500;
    final private static int My_PERMIMSSIONS_READ_PHONE = 100;
    final private static int MY_PERMISSIONS_LOCATION = 200;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    /**
     * The Current version.
     */
    String currentVersion;
    /**
     * The Package info.
     */
    PackageInfo packageInfo;
    /**
     * The Ver code.
     */
    int verCode;
    /**
     * The Cg number.
     */
    TextView cgNumber, /**
     * The Cg number 2.
     */
    cgNumber2, /**
     * The Cg number 3.
     */
    cgNumber3;
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAnalytics mFirebaseAnalytics;
    /**
     * The Privacy text.
     */
    TextView privacyText, /**
     * The Terms text.
     */
    termsText;
    /**
     * The Db helper.
     */
    OfflineDBHelper dbHelper;
    /**
     * The Is updated.
     */
    String isUpdated;
    /**
     * The Count.
     */
    long count;
    private InterstitialAd mInterstitialAd;
    private static final String TAG = "MainActivity";

    private AdView mAdView;

    /**
     * @param savedInstanceState
     * This is where the activity starts loading up all ui elements and the actions associated with them
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        overridePendingTransition(0, 0);
        getSupportActionBar().hide();
        termsText = (TextView) findViewById(R.id.termsText);
        cgNumber = (TextView) findViewById(R.id.cgNumber);
        cgNumber2 = (TextView) findViewById(R.id.cgNumber2);
        cgNumber3 = (TextView) findViewById(R.id.cgNumber3);
        privacyText = (TextView) findViewById(R.id.privacyText);
        infoMessageTV = (TextView) findViewById(R.id.infoMessageTV);
        contactUsButton = (Button) findViewById(R.id.contactUs);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("BE10ABAE13B9C4AB66EDC18C7A74829A").build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(LoginActivity.this);
        mInterstitialAd.setAdUnitId("ca-app-pub-4954480297678755/8989153414");
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("BE10ABAE13B9C4AB66EDC18C7A74829A").build());
        contactUsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ContactActivity.class);
                startActivity(intent);
            }
        });
        termsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, TermsActivity.class);
                startActivity(intent);
            }
        });
        cgNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "9702499222"));
                int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(LoginActivity.this,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(LoginActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_CALL_PHONE);
                    } else {
                        startActivity(intent);

                    }
                }
            }
        });

        infoMessageTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"sales@cableguy.in"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Help App not working");
                /*intent.putExtra(Intent.EXTRA_TEXT, "mail body");*/
                startActivity(Intent.createChooser(intent, ""));
            }
        });
        cgNumber2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "9702797021"));
                int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(LoginActivity.this,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(LoginActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_CALL_PHONE);
                    } else {
                        startActivity(intent);

                    }
                }
            }
        });
        cgNumber3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "8689923456"));
                int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(LoginActivity.this,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(LoginActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_CALL_PHONE);
                    } else {
                        startActivity(intent);

                    }
                }
            }
        });
        privacyText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, PrivacyActivity.class);
                startActivity(intent);
            }
        });
        // Obtain the FirebaseAnalytics instance.
        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();
        if (Intent.ACTION_VIEW.equals(action) && data != null) {
            // TODO: Parse the data URL and show right content for the URL.

            Log.e("Firebase App Indexing", "no data");
        }
        FirebaseCrash.log("Activity created");

        // ...

        // Build GoogleApiClient with AppInvite API for receiving deep links
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, LoginActivity.this)
                .addApi(AppInvite.API)
                .build();

        // Check if this app was launched from a deep link. Setting autoLaunchDeepLink to true
        // would automatically launch the deep link if one is found.
        boolean autoLaunchDeepLink = false;
        AppInvite.AppInviteApi.getInvitation(mGoogleApiClient, this, autoLaunchDeepLink)
                .setResultCallback(
                        new ResultCallback<AppInviteInvitationResult>() {
                            @Override
                            public void onResult(@NonNull AppInviteInvitationResult result) {
                                if (result.getStatus().isSuccess()) {
                                    // Extract deep link from Intent
                                    Intent intent = result.getInvitationIntent();
                                    String deepLink = AppInviteReferral.getDeepLink(intent);

                                    // Handle the deep link. For example, open the linked
                                    // content, or apply promotional credit to the user's
                                    // account.

                                    // ...
                                } else {
                                    Log.d("Firebase Error", "getInvitation: no deep link found.");
                                }
                            }
                        });


        loginButton = (Button) findViewById(R.id.loginButton);
        userNameED = (EditText) findViewById(R.id.usernameED);
        passwordED = (EditText) findViewById(R.id.passwordED);
        changeLangTV = (TextView) findViewById(R.id.changeLang);
        versionNumber = (TextView) findViewById(R.id.versionNumber);
        telephonyManager = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);

        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            currentVersion = packageInfo.versionName;
            verCode = packageInfo.versionCode;
            Values.currentVersion = currentVersion + "." + verCode;
            versionNumber.setText(currentVersion + "." + verCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        localStore = new LocalStore(LoginActivity.this);
        changeLangTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Values.localeSelected = false;
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);//ERROR ShowDialog cannot be resolved to a type
                builder.setTitle("Select Language");
                builder.setSingleChoiceItems(Values.locales, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pos = which;
                        message.displayShort("You selected " + Values.locales[which] + " as the language");
                    }
                });
                builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        message.displayShort("You confirmed " + Values.locales[pos] + " as the language");
                        switch (pos) {
                            case 0:
                                setLocale("en");
                                break;
                            case 1:
                                setLocale("hi");
                                break;
                            case 2:
                                setLocale("gu");
                                break;
                            case 3:
                                setLocale("ta");
                                break;
                        }
                        Values.localeSelected = true;
                        dialog.dismiss();
                    }


                });
                builder.setCancelable(false);
                if (!Values.localeSelected)
                    builder.show();
            }
        });
        callForRead();
        FirebaseCrash.logcat(Log.ERROR, "Tada", "NPE caught");
        dbHelper = new OfflineDBHelper(LoginActivity.this);
        try{
            count = dbHelper.dataCount();
            Log.e("Count ",count + "");
        }catch (Exception ex){
            Log.e("Can't Count ", ex.toString());
        }

    }

    /**
     * On pause action for the activity in case the app is closed down causing the application to pause temporarily
     *
     * The code for pause initiates a Google Ad request which is shown on the screen as an overlay
     */
    @Override
    protected void onPause() {
        super.onPause();

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }
    }

    /**
     * This relates to user action on the screen which can be touch on any element and it will pop out Google Ad for once.
     */
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }
    }

    /**
     * This method initiates request for login when the activity is reinitialised from paused state
     */
    @Override
    protected void onResume() {
        super.onResume();
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = userNameED.getText().toString();
                password = passwordED.getText().toString();
                /*Intent i=new Intent(LoginActivity.this,TypeSelectionActivity.class);
                startActivity(i);*/
                /*if(Values.LOC_PERM_GRANT && Values.READ_PERM_GRANT) {*/
                /**
                 * This checks for active internet connection if it is not present then the user is prompted to connect first
                 */
                if (Values.READ_PERM_GRANT) {
                    if (!(username.equals("") || password.equals(""))) {

                        ConnectivityManager cm = (ConnectivityManager) LoginActivity.this.
                                getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo netInfo = cm.getActiveNetworkInfo();
                        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                            //Connectivity
                            progressDialog = new ProgressDialog(LoginActivity.this);
                            progressDialog.setMessage("Authenticating...");
                            progressDialog.setProgressStyle(0);
                            //progressDialog.show();
                            new GetVersion().execute();


                            //LoginMethod(username,password,Values.currentVersion,telephonyManager.getDeviceId());
                            //progressDialog.dismiss();
                        } else {
                            //No connectivity
                            message.displayShort("No internet connectivity");

                        }


                    } else {
                        message.displayShort(UIMessages.INCOMPLETE_INFO);
                    }
                } else {
                    callForRead();
//
//                    message.displayLong(UIMessages.PERMS_REQUIRED);
                }
            }
        });
    }

    /**
     * @param requestCode
     * @param permissions
     * @param grantResults
     *
     * This method contains action for permission result like CALL, READ, Storage access if denied the respective permission needs to be called again
     *
     * if approved then the user will be allowed to login and use the app
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case My_PERMIMSSIONS_READ_PHONE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Values.READ_PERM_GRANT = true;

                    mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
                    //message.displayShort("READ DONE "+Values.READ_PERM_GRANT);
                } else {
                    //message.displayShort("READ DONE "+Values.READ_PERM_GRANT);

                    message.displayLong(UIMessages.PERMS_REQUIRED);
                }
                break;
            /*case MY_PERMISSIONS_LOCATION:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Values.LOC_PERM_GRANT=true;
                    //message.displayShort("LOC DONE "+Values.LOC_PERM_GRANT);

                }
                else{
                    //message.displayShort("LOC DONE "+Values.LOC_PERM_GRANT);
                    message.displayLong(UIMessages.PERMS_REQUIRED);

                }
                break;*/

            default:
                break;
        }
    }

    /**
     * This method is used to call permissions in Android lollipop and greater i.e. Android 5.1(SDK 22.0) and greater.
     */
    public void callForRead() {

        int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
        try {
            if (currentAPIVersion >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(LoginActivity.this,
                        Manifest.permission.READ_PHONE_STATE)
                        != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(LoginActivity.this,
                                Manifest.permission.WAKE_LOCK)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(LoginActivity.this,
                                Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(LoginActivity.this,
                                Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(LoginActivity.this,
                                Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(LoginActivity.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(LoginActivity.this,
                                Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(LoginActivity.this,
                                Manifest.permission.BLUETOOTH)
                                != PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "called");
                    ActivityCompat.requestPermissions(LoginActivity.this,
                            new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.WAKE_LOCK,
                                    Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.BLUETOOTH},
                            My_PERMIMSSIONS_READ_PHONE);

                } else if (ContextCompat.checkSelfPermission(LoginActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(LoginActivity.this,
                                Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(LoginActivity.this,
                                Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(LoginActivity.this,
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_LOCATION);

                } else {

                    Values.LOC_PERM_GRANT = true;
                    Values.READ_PERM_GRANT = true;
                }
            } else {
                Values.LOC_PERM_GRANT = true;
                Values.READ_PERM_GRANT = true;
            }
        } catch (Exception ex) {
            Log.e("Permission Exception", ex + " ");
        }
    }

    /**
     * Sets locale.
     *
     * @param lang This method is used to set the language of the application. Available languages include Hindi, English, Marathi, Telugu
     */
    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent i = getIntent();
        finish();
        startActivity(i);
    }

    /**
     * Forgotpass.
     *
     * @param view This method is to call forgot password activity on "Forgot password" click View is passed into this method to enable the method identify the source of click event
     */
    public void forgotpass(View view) {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    /**
     * Background task for login by calling the login API
     */

    private class Login extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try {
                url = new URL(Constants.LOGIN_API + "Username=" + username + "&Password=" + password + "&Version=" + Values.currentVersion + "&Imeino=" + telephonyManager.getDeviceId());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            String status;
            JSONArray dataArray;
            Intent intent;
            try {
                status = json.getString("STATUS");
                dataArray = json.getJSONArray("Data");
                switch (status) {
                    case "0":
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject obj = dataArray.getJSONObject(i);
                                String token = obj.getString("TOKEN");
                                String lcoId = obj.getString("LCOID");/*
                            String username = obj.getString("USERNAME");*/
                                String username = obj.getString("USERID");
                                Values.ECAF_USERNAME = obj.getString("USERID");
                                String lcoName = obj.getString("LCONAME");
                                String lcoAddress = obj.getString("LCOADDRESS");
                                String menu = obj.getString("MENU");
                                String loginTime = obj.getString("SERVER_DATETIME");
                                String phone1 = obj.getString("LCOPHONE1");
                                String phone2 = obj.getString("LCOPHONE2");
                                Values.currentBalance = obj.getString("UserBalance");
                                Values.CREDITLIMITFLAG = obj.getString("CREDITLIMITFLAG");
                                Values.etstFlag = obj.getString("ETSTFlag");
                                Values.SELF_CODE = obj.getString("SELFCODEALLOW");
                                Values.AMTEDITFLAG = obj.getString("AMTEDITFLAG");
                                Values.ISONLINEPAY = obj.getString("ISONLINEPAY");
                                Values.ALLOWECAF = obj.getString("ALLOWECAF");
                                Values.ALLOW_OFFLINE = obj.getString("ALLOWOFFLINE");
                                Values.MERCHANTID = obj.getString("MERCHANTID");
                                Values.lcoId = lcoId;
                                UserDetails userDetails = new UserDetails(username, token, lcoId);
                                userDetails.setPassword(passwordED.getText().toString());
                                userDetails.setUsername(username);
                                userDetails.setLoginTime(loginTime);
                                userDetails.setLcoName(lcoName);
                                userDetails.setLcoAddress(lcoAddress);
                                userDetails.setMenu(menu);
                                userDetails.setPhone1(phone1);
                                userDetails.setPhone2(phone2);
                                localStore.storeUserDetails(userDetails);
                                Values.loginTime = loginTime;
                                Values.username = username;
                                Values.selectedlcoName = userDetails.getLcoName();
                                Values.selectedlcoPhone1 = userDetails.getPhone1();
                                Values.selectedlcoPhone2 = userDetails.getPhone2();
                                Values.selectedLcoId = userDetails.getLcoId();
                                if (menu.equalsIgnoreCase("L")) {
                                    intent = new Intent(LoginActivity.this, NavDrawerActivity2.class);
                                    Values.redirect = "LCOType";
                                    Values.buildingId = "";
                                    Values.zoneId = "";
                                    Values.areaId = "";
                                    Values.selectedCustomer = null;
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                } else if (menu.equalsIgnoreCase("B")) {
                                    intent = new Intent(LoginActivity.this, NavDrawerActivity.class);
                                    Values.redirect = "BuildingType";
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                } else if (menu.equalsIgnoreCase("N")) {
                                    intent = new Intent(LoginActivity.this, NavDrawerActivity3.class);
                                    Values.redirect = "NormalType";
                                    Values.buildingId = "";
                                    Values.zoneId = "";
                                    Values.areaId = "";
                                    Values.selectedCustomer = null;
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(LoginActivity.this, "User type not defined, Please contact support", Toast.LENGTH_SHORT).show();
                                }
                            }
                            try{
                                Cursor c = dbHelper.getAllData();
                                if(count>0){
                                    c.moveToFirst();

                                    while(!c.isAfterLast()){
                                        new uploadOfflineTask().execute(c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_SUBSNAME)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_SUBSCODE)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_AMOUNT)),
                                                c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_ADDICHARGES)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_MOBILE)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_REMARK)),
                                                c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_PAYMODE)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_BANKNAME)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_CHEQUEDATE)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_CHEQUENO)),c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_USERNAME)));
                                        c.moveToNext();
                                    }

                                    dbHelper.deleteAllData();


                                }

                            }catch (Exception ex){
                                Log.e("error ",ex.toString());
                            }
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, "Invalid Login details, Please Try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, "Server Error Occurred", Toast.LENGTH_SHORT).show();
                            }
                        });

                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                progressDialog.dismiss();
            }

        }
    }

    /**
     * Background task for uploading offline task automatically
     */

    private class uploadOfflineTask extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();


        /**
         * The Username off.
         */
        String usernameOff, /**
         * The Subs name.
         */
        subsName, /**
         * The Subs code.
         */
        subsCode, /**
         * The Paid amount.
         */
        paidAmount, /**
         * The Add charges.
         */
        addCharges, /**
         * The Mobile.
         */
        mobile, /**
         * The Remark.
         */
        remark, /**
         * The Pay mode.
         */
        payMode, /**
         * The Bank name.
         */
        bankName, /**
         * The Cheque date.
         */
        chequeDate, /**
         * The Cheque no.
         */
        chequeNo;

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            usernameOff = params[10];
            subsName = params[0];
            subsCode = params[1];
            paidAmount = params[2];
            addCharges = params[3];
            mobile = params[4];
            remark = params[5];
            payMode = params[6];
            bankName = params[7];
            chequeDate = params[8];
            chequeNo = params[9];
            try {
                String uri =Constants.OFFLINE_API + "Username=" + usernameOff + "&SubsName=" + subsName + "&SubsCode=" + subsCode + "&PayMode=" + payMode
                        + "&Amount=" + paidAmount + "&AddiCharges=" + addCharges + "&Mobile=" + mobile + "&Remark=" + remark
                        + "&ChqDt=" + chequeDate + "&ChqNo=" + chequeNo + "&BankName=" + bankName;

                url = new URL(uri.replaceAll(" ","%20"));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            String status;
            JSONArray dataArray;
            try {
                status = json.getString("STATUS");
                dataArray = json.getJSONArray("Data");
                switch (status) {
                    case "0" :
                        Toast.makeText(LoginActivity.this, "All pending offline task has been uploaded, Thank you", Toast.LENGTH_SHORT).show();
                        break;
                    case "1":
                        Toast.makeText(LoginActivity.this, "Some error occurred uploading offline data, please login again or try later", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(LoginActivity.this, "Some error occurred uploading offline data, please login again or try later", Toast.LENGTH_SHORT).show();
                        break;
                }
            } catch (Exception ex) {
            }
        }
    }


    /**
     * Background task for getting the latest version of the application
     */

    private class GetVersion extends AsyncTask<String,Void,JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();
        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                url = new URL(Constants.GET_VERSION+"AppType=CG&username="+username);
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");

                JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "0":
                        if(Values.currentVersion.equalsIgnoreCase(obj.getString("VersionNo"))){
                            isUpdated="Y";
                        }else{
                            isUpdated ="N";
                        }
                        if(isUpdated=="Y"){
                            new Login().execute();
                        }else{
                            Log.e("Isupdated ", isUpdated+"");
                            Log.e("i Don't care", "lol");
                            Intent intent = new Intent(LoginActivity.this,UpdateActivity.class);
                            startActivity(intent);
                        }
                        break;
                    case "1":
                        Log.e("updated ", isUpdated+"");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;

                }

            }catch (Exception ex){
                Log.e("Version Update",ex.toString());
            }
        }
    }
}