package in.cableguy.cableguy.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.ECAFCustomer;
import in.cableguy.cableguy.helper.LocalStore;

/**
 * @author Raj Mehra
 *
 * The type Ecaf existing customer activity.
 */
public class ECAFExistingCustomerActivity extends AppCompatActivity {
    /**
     * The Next button.
     */
    Button nextButton;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Email id.
     */
    TextView emailId, /**
     * The Street.
     */
    street, /**
     * The Pin code.
     */
    pinCode, /**
     * The Flat no.
     */
    flatNo, /**
     * The Alt phone number.
     */
    altPhoneNumber, /**
     * The Phone number.
     */
    phoneNumber, /**
     * The Stb number.
     */
    stbNumber, /**
     * The Card number.
     */
    cardNumber, /**
     * The Inst charges.
     */
    instCharges, /**
     * The Stb charges.
     */
    stbCharges;
    /**
     * The Cust name.
     */
    EditText custName, /**
     * The Cust address.
     */
    custAddress;
    /**
     * The Cust type.
     */
    RadioGroup custType;
    /**
     * The Area.
     */
    Spinner area, /**
     * The Location.
     */
    location, /**
     * The Building.
     */
    building, /**
     * The City.
     */
    city, /**
     * The State sp.
     */
    stateSP;
    /**
     * The Ecaf customer.
     */
    ECAFCustomer ecafCustomer;
    /**
     * The State list.
     */
    ArrayList<String> stateList, /**
     * The Location list.
     */
    locationList, /**
     * The City list.
     */
    cityList, /**
     * The Building list.
     */
    BuildingList, /**
     * The State ids.
     */
    stateIds, /**
     * The Location id.
     */
    locationId, /**
     * The City ids.
     */
    cityIds, /**
     * The Building id.
     */
    buildingId;
    /**
     * The Bldgid.
     */
    String bldgid, /**
     * The State id.
     */
    stateId, /**
     * The City id.
     */
    cityId, /**
     * The Locid.
     */
    locid, /**
     * The Cust type rg.
     */
    custTypeRG, /**
     * The Req type.
     */
    reqType, /**
     * The Subsid.
     */
    subsid;
    /**
     * The Adapter.
     */
    ArrayAdapter<String> adapter, /**
     * The State adapter.
     */
    stateAdapter, /**
     * The City adapter.
     */
    cityAdapter, /**
     * The Loc adapter.
     */
    locAdapter, /**
     * The Building adapter.
     */
    buildingAdapter;
    /**
     * The Pos.
     */
    int pos;
    /**
     * The Subscriber id.
     */
    TextView subscriberId, /**
     * The Plan name.
     */
    planName, /**
     * The Plan amount.
     */
    planAmount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecafexisting_customer);
        custName = (EditText)findViewById(R.id.custName);
        emailId = (TextView)findViewById(R.id.emailId);
        custAddress = (EditText)findViewById(R.id.custAddress);
        phoneNumber = (TextView)findViewById(R.id.phoneNumber);
        stbNumber = (TextView)findViewById(R.id.stbNumber);
        cardNumber = (TextView)findViewById(R.id.cardNumber);
        altPhoneNumber = (TextView)findViewById(R.id.altPhoneNumber);
        getSupportActionBar().setTitle("Existing Customer");
        planName = (TextView)findViewById(R.id.planName);
        planAmount = (TextView)findViewById(R.id.planAmount);
        progressDialog = new ProgressDialog(ECAFExistingCustomerActivity.this);
        localStore = new LocalStore(ECAFExistingCustomerActivity.this);
        nextButton = (Button)findViewById(R.id.nextButton);
        subscriberId = (TextView)findViewById(R.id.subscriberId);
        new FetchCustomerData().execute();
        Values.setAddressProofUrl="";
        Values.setCustomerPhotoUrl="";
        Values.setIdProofUrl="";
        Values.setSignUrl="";

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ecafCustomer = new ECAFCustomer();
                if(emailId.getText().toString().equalsIgnoreCase("")||phoneNumber.getText().toString().equalsIgnoreCase("")||altPhoneNumber.getText().toString().equalsIgnoreCase("")){
                    Toast.makeText(ECAFExistingCustomerActivity.this, "Please select all fields", Toast.LENGTH_SHORT).show();
                }else{
                    ecafCustomer.setIn_subsname(custName.getText().toString());
                    ecafCustomer.setIn_address1(custAddress.getText().toString());
                    ecafCustomer.setIn_subsid(subscriberId.getText().toString());
                    ecafCustomer.setIn_phone1(phoneNumber.getText().toString());
                    ecafCustomer.setIn_phone2(altPhoneNumber.getText().toString());
                    ecafCustomer.setIn_email(emailId.getText().toString());
                    Values.ECAF_CUSTOMER = ecafCustomer;
                    Intent intent = new Intent(ECAFExistingCustomerActivity.this,DocumentActivity.class);
                    Values.MODULE = "KYCE";
                    Values.redirect="";
                    startActivity(intent);
                }
            }
        });


    }


    private class FetchPlanData extends AsyncTask<String,Void,JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ECAFExistingCustomerActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            String subsidText = params[0];
            try{
                url = new URL(Constants.EXISTING_SUBSCRIBER_PLAN+"username="+localStore.getUserDetails().getUserName()+"&in_subsid="+subsidText+"&in_operid="+Values.selectedLcoId+"&Token="+localStore.getUserDetails().getToken());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                final JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "0":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    planName.setText(obj.getString("PlanName"));
                                    planAmount.setText(obj.getString("PlanRate"));
                                    Values.PLAN_ID = obj.getString("PlanCode");
                                    emailId.setText(obj.getString("EmailId"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ECAFExistingCustomerActivity.this, "Error while fetching plans", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ECAFExistingCustomerActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
            }finally {
                progressDialog.dismiss();
            }
        }
    }

    /**
     * Background task to fetch customer data
     */
    private class FetchCustomerData extends AsyncTask<String,Void,JSONObject>{
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();


        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                String uri = Constants.SUBS_SEARCH+"mode="+Values.searchType+"&Lcoid="+Values.selectedLcoId+
                        "&SubsName="+Values.selectedCustomerName+"&SubsNo="+Values.selectedCustomerNumber+"&MobNo="+
                        Values.cp1+"&CRDNO="+Values.cardNumber+"&STBNO="+Values.stbNumber+"&UserID="+localStore.getUserDetails().getUserName()+
                        "&Token="+localStore.getUserDetails().getToken();
                url = new URL(uri.replaceAll(" ","%20"));
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                final JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "0":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    custName.setText(obj.getString("SUBSNAME"));
                                    emailId.setText("");
                                    custAddress.setText(obj.getString("SUBSADDRESS"));
                                    subscriberId.setText(obj.getString("SUBSID"));
                                    phoneNumber.setText(obj.getString("subsphone1"));
                                    altPhoneNumber.setText(obj.getString("subsphone2"));
                                    stbNumber.setText(obj.getString("STBNO"));
                                    cardNumber.setText(obj.getString("CARDNO"));
                                }
                                catch (JSONException e){
                                    Log.e("Fetch Error ",e.toString());
                                }

                                new FetchPlanData().execute(subscriberId.getText().toString());

                            }
                        });
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ECAFExistingCustomerActivity.this, "Could not fetch customers, Please try again", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ECAFExistingCustomerActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
            }
        }
    }


}
