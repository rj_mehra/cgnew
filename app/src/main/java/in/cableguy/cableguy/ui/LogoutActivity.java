package in.cableguy.cableguy.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import in.cableguy.cableguy.R;

public class LogoutActivity extends AppCompatActivity {

    private InterstitialAd mInterstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        mInterstitialAd = new InterstitialAd(LogoutActivity.this);
        mInterstitialAd.setAdUnitId("ca-app-pub-4954480297678755/8989153414");
        mInterstitialAd.loadAd(new AdRequest.Builder().addTestDevice("BE10ABAE13B9C4AB66EDC18C7A74829A").build());
        mInterstitialAd.show();
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }
    }


    /**
     * This relates to user action on the screen which can be touch on any element and it will pop out Google Ad for once.
     */
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }
    }
}
