package in.cableguy.cableguy.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.Lco;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.MessageHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 *
 * @author Raj Mehra
 *
 * The type Normal type activity.
 *
 * This activity lists the LCOs assigned to the user
 */
public class NormalTypeActivity extends AppCompatActivity {
    /**
     * The Next button.
     */
    Button nextButton;
    /**
     * The Lco list view.
     */
    ListView lcoListView;
    /**
     * The Adapter.
     */
    ArrayAdapter<String> adapter;
    /**
     * The Lco list.
     */
    ArrayList<String> lcoList=new ArrayList<>();
    /**
     * The Lco etst flag.
     */
    ArrayList<String> lcoETSTFlag = new ArrayList<>();
    /**
     * The Lcos.
     */
    ArrayList<Lco> lcos=new ArrayList<>();
    /**
     * The Lco address list.
     */
    ArrayList<String> lcoAddressList = new ArrayList<>();
    /**
     * The Lco phone 1 list.
     */
    ArrayList<String> lcoPhone1List = new ArrayList<>();
    /**
     * The Lco phone 2 list.
     */
    ArrayList<String> lcoPhone2List = new ArrayList<>();
    /**
     * The Lco name map.
     */
    Map<String,Lco> lcoNameMap=new HashMap<>();
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Message.
     */
    MessageHelper message;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_normal_type);
        overridePendingTransition(0, 0);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        nextButton = (Button)findViewById(R.id.nextButton);
        lcoListView = (ListView)findViewById(R.id.lcoListView);
        adapter = new ArrayAdapter<String>(this,R.layout.lcolist,R.id.lcoName,lcoList);
        lcoListView.setAdapter(adapter);
        localStore=new LocalStore(NormalTypeActivity.this);
        message=new MessageHelper(NormalTypeActivity.this);
        /*nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NormalTypeActivity.this,CustomerDetailsActivity2.class);
                startActivity(intent);
            }
        });
        */

        lcoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = (String)lcoListView.getAdapter().getItem(position);/*
                Intent intent = new Intent(NormalTypeActivity.this,CustomerDetailsActivity.class);*/
                String lcoId=lcoNameMap.get(item).getLcoId();
                if(Values.redirect.equalsIgnoreCase("LCOSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("LCOSubsTransaction")){
                    Values.redirect="LCOSubsTransaction";
                    Values.selectedCustomerName="";
                    Values.selectedLcoId=lcoId;
                    Log.e("Values.selectedLcoid ",Values.selectedLcoId);
                    Values.selectedlcoName = lcoNameMap.get(item).getLcoName();
                    Values.selectedlcoAddress = lcoAddressList.get(position);
                    Values.selectedlcoPhone1 = lcoPhone1List.get(position);
                    Values.selectedlcoPhone2 = lcoPhone2List.get(position);
                    Intent intent = new Intent(NormalTypeActivity.this, NormalSearchActivity.class);
                    startActivity(intent);
                }else if(Values.redirect.equalsIgnoreCase("ECAFLCO")){
                    Values.redirect="ECAFLCO";
                    Values.selectedCustomerName="";
                    Values.selectedLcoId=lcoId;
                    Log.e("Values.selectedLcoid ",Values.selectedLcoId);
                    Values.selectedlcoName = lcoNameMap.get(item).getLcoName();
                    Values.selectedlcoAddress = lcoAddressList.get(position);
                    Values.selectedlcoPhone1 = lcoPhone1List.get(position);
                    Values.selectedlcoPhone2 = lcoPhone2List.get(position);
                    Intent intent = new Intent(NormalTypeActivity.this, ECAFCustomerTypeActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(NormalTypeActivity.this,SearchActivity.class);
                    Values.selectedCustomerName="";
                    Values.selectedLcoId=lcoId;
                    Log.e("Values.selectedLcoid 2 ",Values.selectedLcoId);
                    Values.selectedlcoName = lcoNameMap.get(item).getLcoName();
                    Values.selectedlcoAddress = lcoAddressList.get(position);
                    Values.selectedlcoPhone1 = lcoPhone1List.get(position);
                    Values.selectedlcoPhone2 = lcoPhone2List.get(position);
                    Values.redirect = "LCOType";
                    Values.etstFlag = lcoETSTFlag.get(position);
                    Log.e("ETSTFLAG ", Values.etstFlag);
                    startActivity(intent);
                    finish();
                }
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        ConnectivityManager cm = (ConnectivityManager) NormalTypeActivity.this.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            //Connectivity
            new FetchLCOs().execute();
        }
        else{
            //No connectivity
            message.displayShort("No internet connectivity");

        }

    }


    /**
     *
     * Background task for fetching LCOs assigned to the user
     */

    private class FetchLCOs extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(NormalTypeActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            lcoList.clear();
            try {
                url = new URL(Constants.GET_TYPE_DETAILS + "Type=L" + "&Username=" + localStore.getUserDetails().getUserName() + "&Token=" + localStore.getUserDetails().getToken());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        for(int i = 0; i<dataArray.length();i++){
                            final JSONObject obj = dataArray.getJSONObject(i);
                            try {
                                String lcoName=obj.getString("LcoName");
                                String lcoId=obj.getString("LcoId");
                                String lcoAddress = obj.getString("LcoAddress");
                                String lcoPhone1 = obj.getString("LcoContact");
                                String lcoPhone2 = obj.getString("LcoContact");
//                                        Values.etstFlag = jsonObject.getString("ETSTFlag");
                                lcoETSTFlag.add(obj.getString("ETSTFlag"));
                                Lco lco=new Lco(lcoName,lcoId);
                                lcos.add(lco);
                                lcoList.add(lcoName);
                                lcoAddressList.add(lcoAddress);
                                lcoPhone1List.add(lcoPhone1);
                                lcoPhone2List.add(lcoPhone2);
                                lcoNameMap.put(lcoName,lco);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            adapter.notifyDataSetChanged();

                        }
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(NormalTypeActivity.this, "No Lcos mapped", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(NormalTypeActivity.this, "Cannot connect to the server, please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(NormalTypeActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
}
