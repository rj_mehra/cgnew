package in.cableguy.cableguy.ui;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Values;

/**
 * @author Raj Mehra
 * The type Ecaf customer type activity.
 */
public class ECAFCustomerTypeActivity extends AppCompatActivity {
    /**
     * The Search button.
     */
    Button searchButton;
    /**
     * The Search ll.
     */
    LinearLayout searchLL;
    /**
     * The Search options sp.
     */
    Spinner searchOptionsSP;
    /**
     * The Search term ed.
     */
    EditText searchTermED;
    /**
     * The Customer type rg.
     */
    RadioGroup customerTypeRG;
    private String[] optionsArray;
    /**
     * The Pos.
     */
    int pos;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecafcustomer_type);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        optionsArray = new String[]{"Search By","VC","STB No","Phone Number","Customer Code"};
        searchButton = (Button)findViewById(R.id.searchButton);
        searchLL =(LinearLayout)findViewById(R.id.searchLL);
        getSupportActionBar().setTitle("Select Customer Type");
        searchTermED = (EditText)findViewById(R.id.searchTermED);
        searchOptionsSP = (Spinner)findViewById(R.id.searchOptionsSP);
        customerTypeRG = (RadioGroup)findViewById(R.id.customerTypeRG);
        Values.setAddressProofUrl="";
        Values.setCustomerPhotoUrl="";
        Values.setIdProofUrl="";
        Values.setSignUrl="";
        Values.addProofImageFile = "";
        Values.custImageFile ="";
        Values.signImageFile ="";
        Values.idProofImageFile = "";
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinnerlayout, R.id.optionSP, optionsArray);
        searchOptionsSP.setAdapter(adapter);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search = searchTermED.getText().toString();
                if(isGPSEnabled()) {
                    if (!searchTermED.getText().toString().equalsIgnoreCase("")) {
                        Values.isNew = false;
                        Intent intent = new Intent(ECAFCustomerTypeActivity.this, VerificationTypeActivity.class);
                        if (Values.ECAF_SEARCH.equalsIgnoreCase("VC")) {
                            Values.searchType = "CRD";
                            Values.cardNumber = search;
                            Values.stbNumber = "";
                            Values.cp1 = "";
                            Values.selectedCustomerNumber = "";

                            startActivity(intent);
                        } else if (Values.ECAF_SEARCH.equalsIgnoreCase("STB No")) {
                            Values.searchType = "STB";
                            Values.cardNumber = "";
                            Values.stbNumber = search;
                            Values.cp1 = "";
                            Values.selectedCustomerNumber = "";
                            startActivity(intent);
                        } else if (Values.ECAF_SEARCH.equalsIgnoreCase("Phone Number")) {
                            Values.searchType = "CP1";
                            Values.cardNumber = "";
                            Values.stbNumber = "";
                            Values.cp1 = search;
                            Values.selectedCustomerNumber = "";
                            startActivity(intent);
                        } else if (Values.ECAF_SEARCH.equalsIgnoreCase("Customer Code")) {

                            Values.searchType = "CC";
                            Values.cardNumber = "";
                            Values.stbNumber = "";
                            Values.cp1 = "";
                            Values.selectedCustomerNumber = search;
                            startActivity(intent);
                        } else if (Values.ECAF_SEARCH.equalsIgnoreCase("Search By")) {
                            Toast.makeText(ECAFCustomerTypeActivity.this, "Please select an option", Toast.LENGTH_SHORT).show();
                        } else {
                        }
                    } else {
                        Toast.makeText(ECAFCustomerTypeActivity.this, "Search field cannot be left blank", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(ECAFCustomerTypeActivity.this, "Please enable Location Settings in order to continue", Toast.LENGTH_SHORT).show();
                    Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(viewIntent);
                }

            }
        });

        customerTypeRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                pos=customerTypeRG.indexOfChild(findViewById(checkedId));
                switch(pos){
                    case 0:
                        if(isGPSEnabled()) {
                            Values.isNew = true;
                            Intent intent = new Intent(ECAFCustomerTypeActivity.this, VerificationTypeActivity.class);
                            startActivity(intent);
                        }else{
                            Toast.makeText(ECAFCustomerTypeActivity.this, "Please enable Location Settings in order to continue", Toast.LENGTH_SHORT).show();
                            Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(viewIntent);
                        }
                        break;
                    case 1:
                        if(!isGPSEnabled()) {
                            Toast.makeText(ECAFCustomerTypeActivity.this, "Please enable Location Settings in order to continue", Toast.LENGTH_SHORT).show();
                            Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(viewIntent);
                        }
                        break;
                }
            }
        });

        searchOptionsSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = parent.getItemAtPosition(position).toString();
                switch (position){
                    case 0 :
                        Values.ECAF_SEARCH = "Search By";
                        break;
                    case 1 :
                        Values.ECAF_SEARCH = "VC";
                        break;
                    case 2:
                        Values.ECAF_SEARCH = "STB No";
                        break;
                    case 3:
                        Values.ECAF_SEARCH = "Phone Number";
                        break;
                    case 4:
                        Values.ECAF_SEARCH = "Customer Code";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     *
     * Method to check if GPS is enabled on the phone
     * @return true or false
     */
    private boolean isGPSEnabled(){
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if(!provider.contains("gps")){ //if gps is disabled
            /*final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);

*/          return false;
        }
        else{
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        customerTypeRG.clearCheck();
    }
}
