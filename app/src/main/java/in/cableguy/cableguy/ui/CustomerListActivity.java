package in.cableguy.cableguy.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.Customer;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.MessageHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author Raj Mehra
 * The type Customer list activity.
 *
 * This activity deals with listing the customers by calling customer list API which is listed in CONSTANTS FILE
 */
public class CustomerListActivity extends AppCompatActivity {
    private ListView customerListView;
    /**
     * The Adapter.
     */
    ArrayAdapter<String> adapter;
    /**
     * The Customer list.
     */
    ArrayList<String> customerList=new ArrayList<>();
    /**
     * The Customer id list.
     */
    ArrayList<String> customerIdList = new ArrayList<>();
    /**
     * The Customer name id list.
     */
    ArrayList<String> customerNameIdList=new ArrayList<>();
    /**
     * The Customers.
     */
    ArrayList<Customer> customers=new ArrayList<>();
    /**
     * The Customerreal id.
     */
    ArrayList<String> customerrealId = new ArrayList<>();
    /**
     * The Customer name map.
     */
/*Map<String,Customer> customerIdCustMap=new HashMap<>();
    Map<String,String> customerIdNameMap=new HashMap<>();*/
    Map<String,Customer> customerNameMap=new HashMap<>();
    /**
     * The Customer id map.
     */
    Map<String,String> customerIdMap = new HashMap<>();
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    /**
     * The Search customer.
     */
    EditText searchCustomer;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Message.
     */
    MessageHelper message;

    ImageView userIconIV;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(CustomerListActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);
        overridePendingTransition(0, 0);
        //String customer[] = {"Ryan Fernandes","Krishna Gupta","Mathews Fuler","John Doe","Rovina Hensiki","Elaine Schmuk"};
        customerListView = (ListView)findViewById(R.id.customerListView);
        searchCustomer = (EditText)findViewById(R.id.searchCustomer);
        getSupportActionBar().setTitle("Search Customer");
        adapter = new ArrayAdapter<String>(this,R.layout.list_item,R.id.listItemName,customerNameIdList);
        customerListView.setAdapter(adapter);
        localStore=new LocalStore(CustomerListActivity.this);
        message=new MessageHelper(CustomerListActivity.this);
        userIconIV = (ImageView)findViewById(R.id.userIconIV);
        customerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = (String)customerListView.getAdapter().getItem(position);

                Values.selectedCustomer=customerNameMap.get(item);
                if(Values.selectedCustomer!=null){
                    Values.selectedCustomerNumber = Values.selectedCustomer.getNumber();
                    Values.selectedCustomerName=Values.selectedCustomer.getName();
                    Values.customerIdSelected =Values.selectedCustomer.getId();
                }else{
                    Values.selectedCustomerNumber = null;
                    Values.selectedCustomerName = null;
                    Values.customerIdSelected = null;
                    Values.selectedCustomer = null;
                }

                if(Values.redirect.equalsIgnoreCase("BuildingSubsTransaction")){
                    Values.listType="subs";
                    Intent intent = new Intent(CustomerListActivity.this, LastTransactionListActivity.class);
                    startActivity(intent);
                }else if(Values.redirect.equalsIgnoreCase("NormalSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("NormalSubsTransaction")){

                    Values.listType="subs";
                    Intent intent = new Intent(CustomerListActivity.this, LastTransactionListActivity.class);
                    startActivity(intent);
                }else if(Values.redirect.equalsIgnoreCase("LCOSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("LCOSubsTransaction")){

                    Values.listType="subs";
                    Intent intent = new Intent(CustomerListActivity.this, LastTransactionListActivity.class);
                    startActivity(intent);
                }else if(Values.redirect.equalsIgnoreCase("BuildingType")){

                    Intent intent = new Intent(CustomerListActivity.this, CustomerDetailsActivity.class);
                    startActivity(intent);
                }else if(Values.redirect.equalsIgnoreCase("NormalType")){

                    Intent intent = new Intent(CustomerListActivity.this, BillingDetailsActivity.class);
                    startActivity(intent);
                }else if ((Values.redirect.equalsIgnoreCase("LCOType"))){

                    Intent intent = new Intent(CustomerListActivity.this, BillingDetailsActivity.class);
                    startActivity(intent);
                }else if(Values.redirect.equalsIgnoreCase("RENEW")){
                    Intent intent = new Intent(CustomerListActivity.this,RenewCustomerActivity.class);
                    startActivity(intent);
                }


            }
        });

        searchCustomer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                CustomerListActivity.this.adapter.getFilter().filter(s);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        redirect();
    }

    /**
     * Redirect.
     */
    public void redirect(){
        if(Values.redirect.equalsIgnoreCase("BuildingSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(CustomerListActivity.this, NavDrawerActivity.class);
            Values.redirect="";
            Values.listType="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("NormalSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("NormalSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(CustomerListActivity.this, NavDrawerActivity3.class);
            Values.redirect="";
            Values.listType="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("LCOSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("LCOSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(CustomerListActivity.this, NavDrawerActivity2.class);
            Values.redirect="";
            Values.listType="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("BuildingType")){
            Intent intent = new Intent(CustomerListActivity.this, NavDrawerActivity.class);
            Values.redirect="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("NormalType")){
            Intent intent = new Intent(CustomerListActivity.this, NavDrawerActivity3.class);
            Values.redirect="";
            startActivity(intent);
        }else if ((Values.redirect.equalsIgnoreCase("LCOType"))){
            Intent intent = new Intent(CustomerListActivity.this, NavDrawerActivity2.class);
            Values.redirect="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("RENEW")){
            Intent intent = new Intent(CustomerListActivity.this,RenewCustomerActivity.class);
            startActivity(intent);
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        ConnectivityManager cm = (ConnectivityManager) CustomerListActivity.this.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            //Connectivity
            new FetchCustomerList().execute();
        }
        else{
            //No connectivity
            message.displayShort("No internet connectivity");

        }

    }

    /**
     * Background task to call for customer list
     */

    private class FetchCustomerList extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(CustomerListActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            String uri;
            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.clear();
                    }
                });

                if (Values.redirect.equalsIgnoreCase("NormalType")) {
                    uri = Constants.GET_TYPE_DETAILS + "Type=C" + "&Username=" + localStore.getUserDetails().getUserName() + "&Token=" + localStore.getUserDetails().getToken() + "&lcoid=" + Values.selectedLcoId;
                    url = new URL(uri);

                } else {
                    uri = Constants.GET_TYPE_DETAILS + "Type=C" + "&Username=" + localStore.getUserDetails().getUserName() + "&Token=" + localStore.getUserDetails().getToken() + "&lcoid=" + Values.selectedLcoId + "&zoneid=" + Values.zoneId + "&areaid=" + Values.areaId + "&buildingid=" + Values.buildingId + "&stateid=" + "&ctid=";
                    url = new URL(uri);
                }

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    customerList.clear();
                    customerIdMap.clear();
                    customerNameMap.clear();
                }
            });

            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        for(int i = 0; i<dataArray.length();i++){
                            final JSONObject obj = dataArray.getJSONObject(i);
                            try {
                                String customerName=obj.getString("CustomerName");
                                String customerNumber=obj.getString("CustomerNumber");
                                String customerId=obj.getString("CustomerId");
/*                                        Customer customer=new Customer(customerId,customerNumber,customerName);*/
                                Customer customer=new Customer(customerId,customerNumber,customerName);
                                customers.add(customer);
                                String customerIdName=customerNumber+"\n"+customerName;
                                customerNameMap.put(customerIdName,customer);
                                        /*customerIdNameMap.put(customerId,customerName);
                                        customerIdCustMap.put(customerId,customer);*/
                                customerNameIdList.add(customerIdName);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            adapter.notifyDataSetChanged();

                        }
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Values.selectedArea = "No Area";
                                Toast.makeText(CustomerListActivity.this, "Invalid Login Details or Session Timed out", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CustomerListActivity.this, "Cannot connect to the server, please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                progressDialog.dismiss();
            }
        }
    }

}
