package in.cableguy.cableguy.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.UserDetails;

/**
 *
 * @author Raj Mehra
 * The type Status activity.
 *
 * This activity is to know the status of the Online payment initiated for the customer billing,
 *
 * This returns whether the payment was successful or failed
 *
 *
 * @see BillingDetailsActivity
 */
public class StatusActivity extends AppCompatActivity {
    /**
     * The Status.
     */
    TextView status;
    /**
     * The Alert dialog.
     */
    AlertDialog alertDialog;
    /**
     * The M status.
     */
    String mStatus[];
    /**
     * The Success ll.
     */
    LinearLayout successLL, /**
     * The Failed ll.
     */
    failedLL, /**
     * The Status activity.
     */
    status_activity;
    /**
     * The Retry button.
     */
    Button retryButton, /**
     * The Done button.
     */
    doneButton;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The User details.
     */
    UserDetails userDetails;
    /**
     * The Transaction ref no.
     */
    String transactionRefNo, /**
     * The Bank ref no.
     */
    bankRefNo, /**
     * The Bank id.
     */
    bankID, /**
     * The Bank merch id.
     */
    bankMerchId, /**
     * The Txn type.
     */
    txnType, /**
     * The Txn date.
     */
    txnDate, /**
     * The Auth status.
     */
    authStatus, /**
     * The Check sum.
     */
    checkSum;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        successLL = (LinearLayout) findViewById(R.id.successLL);
        failedLL = (LinearLayout) findViewById(R.id.failedLL);
        retryButton = (Button) findViewById(R.id.retryButton);
        doneButton = (Button) findViewById(R.id.doneButton);
        status_activity = (LinearLayout) findViewById(R.id.status_activity);
        progressDialog = new ProgressDialog(StatusActivity.this);
        localStore = new LocalStore(StatusActivity.this);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Values.redirect.equalsIgnoreCase("NormalType")) {
                    Log.e("Values.redirect Normal", Values.redirect);
                    Intent intent = new Intent(StatusActivity.this, SearchActivity.class);
                    Values.selectedCustomer = null;
                    Values.selectedBuilding = null;
                    Values.stbNumber = "";
                    Values.selectedCustomerNumber = "";
                    Values.cp1 = "";
                    Values.cardNumber = "";
                    Values.customerNumber = "";
                    startActivity(intent);
                    finish();


                } else if (Values.redirect.equalsIgnoreCase("LCOType")) {
                    Log.e("Values.redirect LCO", Values.redirect);
//                    Intent intent = new Intent(SuccessActivity.this, NormalTypeActivity.class);
                    Intent intent = new Intent(StatusActivity.this, SearchActivity.class); //changed for avoiding redirection
                    Values.selectedCustomer = null;
                    Values.stbNumber = "";
                    Values.selectedCustomerNumber = "";
                    Values.cp1 = "";
                    Values.cardNumber = "";
                    Values.customerNumber = "";
                    startActivity(intent);
                    finish();
                } else if (Values.redirect.equalsIgnoreCase("BuildingType")) {
                    Intent intent = new Intent(StatusActivity.this, CustomerDetailsActivity.class);
/*                    Values.selectedCustomer=null;*/
                    Log.e("Values.redirect ELSE", Values.redirect);
/*                    Values.selectedBuilding = "Select Building";
                    Values.selectedZone = "Select Zone";
                    Values.selectedArea = "Select Area";*/
                    Values.stbNumber = "";
                    Values.selectedCustomerNumber = "";
                    Values.cp1 = "";
                    Values.cardNumber = "";
                    Values.customerNumber = "";
                    startActivity(intent);
                    finish();
                } else if (Values.MODULE.equalsIgnoreCase("KYCE")) {
                    Intent intent = new Intent(StatusActivity.this, ECAFCustomerTypeActivity.class);
                    Values.setAddressProofUrl = "";
                    Values.setCustomerPhotoUrl = "";
                    Values.setIdProofUrl = "";
                    Values.setSignUrl = "";
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else if (Values.MODULE.equalsIgnoreCase("KYCI")) {
                    Intent intent = new Intent(StatusActivity.this, ECAFCustomerTypeActivity.class);
                    Values.setAddressProofUrl = "";
                    Values.setCustomerPhotoUrl = "";
                    Values.setIdProofUrl = "";
                    Values.setSignUrl = "";
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Values.redirect.equalsIgnoreCase("NormalType")) {
                    Log.e("Values.redirect Normal", Values.redirect);
                    Intent intent = new Intent(StatusActivity.this, SearchActivity.class);
                    Values.selectedCustomer = null;
                    Values.selectedBuilding = null;
                    Values.stbNumber = "";
                    Values.selectedCustomerNumber = "";
                    Values.cp1 = "";
                    Values.cardNumber = "";
                    Values.customerNumber = "";
                    startActivity(intent);
                    finish();


                } else if (Values.redirect.equalsIgnoreCase("LCOType")) {
                    Log.e("Values.redirect LCO", Values.redirect);
//                    Intent intent = new Intent(SuccessActivity.this, NormalTypeActivity.class);
                    Intent intent = new Intent(StatusActivity.this, SearchActivity.class); //changed for avoiding redirection
                    Values.selectedCustomer = null;
                    Values.stbNumber = "";
                    Values.selectedCustomerNumber = "";
                    Values.cp1 = "";
                    Values.cardNumber = "";
                    Values.customerNumber = "";
                    startActivity(intent);
                    finish();
                } else if (Values.redirect.equalsIgnoreCase("BuildingType")) {
                    Intent intent = new Intent(StatusActivity.this, CustomerDetailsActivity.class);
/*                    Values.selectedCustomer=null;*/
                    Log.e("Values.redirect ELSE", Values.redirect);
/*                    Values.selectedBuilding = "Select Building";
                    Values.selectedZone = "Select Zone";
                    Values.selectedArea = "Select Area";*/
                    Values.stbNumber = "";
                    Values.selectedCustomerNumber = "";
                    Values.cp1 = "";
                    Values.cardNumber = "";
                    Values.customerNumber = "";
                    startActivity(intent);
                    finish();
                } else if (Values.MODULE.equalsIgnoreCase("KYCE")) {
                    Intent intent = new Intent(StatusActivity.this, ECAFCustomerTypeActivity.class);
                    Values.setAddressProofUrl = "";
                    Values.setCustomerPhotoUrl = "";
                    Values.setIdProofUrl = "";
                    Values.setSignUrl = "";
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else if (Values.MODULE.equalsIgnoreCase("KYCI")) {
                    Intent intent = new Intent(StatusActivity.this, ECAFCustomerTypeActivity.class);
                    Values.setAddressProofUrl = "";
                    Values.setCustomerPhotoUrl = "";
                    Values.setIdProofUrl = "";
                    Values.setSignUrl = "";
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
        });
        Bundle bundle = this.getIntent().getExtras();
        mStatus = bundle.getString("status").toString().split("\\|");
        transactionRefNo = mStatus[2];
        bankRefNo = mStatus[3];
        bankID = mStatus[5];
        bankMerchId = mStatus[6];
        txnType = mStatus[7];
        txnDate = mStatus[13];
        authStatus = mStatus[14];
        checkSum = mStatus[25];
        Log.e("Status", bundle.getString("status"));
//        status.setText(bundle.getString("status"));
        Log.e("mSTATUS", mStatus[14]);

        if (authStatus.equalsIgnoreCase("0300")) {
            if(failedLL.getVisibility()==View.VISIBLE || successLL.getVisibility() == View.GONE ){
                failedLL.setVisibility(View.GONE);
                successLL.setVisibility(View.VISIBLE);
            }

            status_activity.setBackgroundColor(getResources().getColor(R.color.successColor));
            new InsertOnlinePayment().execute();

        } else if (authStatus.equalsIgnoreCase("0399") ) {
            if(failedLL.getVisibility()==View.GONE || successLL.getVisibility() == View.VISIBLE ){
                failedLL.setVisibility(View.VISIBLE);
                successLL.setVisibility(View.GONE);
            }
            status_activity.setBackgroundColor(getResources().getColor(R.color.errorColor));
            new InsertOnlinePayment().execute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(StatusActivity.this, mStatus[24], Toast.LENGTH_SHORT).show();
                }
            });
        } else if (authStatus.equalsIgnoreCase("NA")) {
            if(failedLL.getVisibility()==View.GONE || successLL.getVisibility() == View.VISIBLE ){
                failedLL.setVisibility(View.VISIBLE);
                successLL.setVisibility(View.GONE);
            }
            status_activity.setBackgroundColor(getResources().getColor(R.color.errorColor));
            new InsertOnlinePayment().execute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(StatusActivity.this, mStatus[24], Toast.LENGTH_SHORT).show();
                }
            });
        } else if (authStatus.equalsIgnoreCase("0002")) {
            if(failedLL.getVisibility()==View.GONE || successLL.getVisibility() == View.VISIBLE ){
                failedLL.setVisibility(View.VISIBLE);
                successLL.setVisibility(View.GONE);
            }
            status_activity.setBackgroundColor(getResources().getColor(R.color.errorColor));
            new InsertOnlinePayment().execute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(StatusActivity.this, mStatus[24], Toast.LENGTH_SHORT).show();
                }
            });
        } else if (authStatus.equalsIgnoreCase("0001")) {
            if(failedLL.getVisibility()==View.GONE || successLL.getVisibility() == View.VISIBLE ){
                failedLL.setVisibility(View.VISIBLE);
                successLL.setVisibility(View.GONE);
            }
            status_activity.setBackgroundColor(getResources().getColor(R.color.errorColor));
            new InsertOnlinePayment().execute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(StatusActivity.this, mStatus[24], Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            if(failedLL.getVisibility()==View.GONE || successLL.getVisibility() == View.VISIBLE ){
                failedLL.setVisibility(View.VISIBLE);
                successLL.setVisibility(View.GONE);
            }
            status_activity.setBackgroundColor(getResources().getColor(R.color.errorColor));
            new InsertOnlinePayment().execute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(StatusActivity.this, mStatus[24], Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    /**
     *
     * Background task for inserting online payment on cableguy portal
     */
    private class InsertOnlinePayment extends AsyncTask<String, Void, JSONObject>{
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(StatusActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                String uri = Constants.INSERT_ONLINE_PAYMENT+"orderid="+Values.ORDER_ID+"&RESPCODE="+mStatus[14]+"&RESPMSG=" +mStatus[24]+
                        "&TXNDATE="+txnDate+"&GATEWAYNAME=BillDesk&BANKTXNID="+bankRefNo+"&BANKNAME=" +bankMerchId+
                        "&STATUS="+mStatus[14]+"&CHECKSUMHASH="+checkSum+"&CURRENCY=INR&TXNID=" +Values.customerIdSelected + "-" + Values.ORDER_ID+
                        "&OL_PAYMENTMODE=ONLINE&TXNAMOUNT="+Values.ORDER_AMOUNT+"&username="+localStore.getUserDetails().getUserName()+"&Lcoid=" +Values.selectedLcoId+
                        "&Subsid="+Values.customerIdSelected+"&PaymentMode=ONLINE&payamt="+Values.ORDER_AMOUNT+"&Remark=&Token="+localStore.getUserDetails().getToken();
                uri = uri.replace(" ","%20");
                url = new URL(uri);
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                final JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(StatusActivity.this, "Payment Successful", Toast.LENGTH_SHORT).show();
                                Values.ORDER_ID ="";
                            }
                        });
                        break;
                    case "2":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(StatusActivity.this, "Something went wrong, Please Try again", Toast.LENGTH_SHORT).show();
                                Values.ORDER_ID ="";
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(StatusActivity.this, "Some error occured, Please check your internet and try again!", Toast.LENGTH_SHORT).show();
                                Values.ORDER_ID ="";
                            }
                        });
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                progressDialog.dismiss();
            }
        }
    }
}
