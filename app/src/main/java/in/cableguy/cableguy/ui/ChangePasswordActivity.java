package in.cableguy.cableguy.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.MessageHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * The type Change password activity.
 * @author Raj Mehra
 */
public class ChangePasswordActivity extends AppCompatActivity {
    /**
     * The Password ed.
     */
    EditText passwordED, /**
     * The Confirm password ed.
     */
    confirmPasswordED;
    /**
     * The Submit button.
     */
    Button submitButton;
    /**
     * The Message.
     */
    MessageHelper message;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(ChangePasswordActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        overridePendingTransition(0, 0);
        progressDialog = new ProgressDialog(ChangePasswordActivity.this);
        getSupportActionBar().setTitle("Change Password");
        passwordED=(EditText)findViewById(R.id.newPassED);
        confirmPasswordED=(EditText)findViewById(R.id.newPassED2);
        submitButton=(Button)findViewById(R.id.changePasswordButton);
        message=new MessageHelper(ChangePasswordActivity.this);
        localStore=new LocalStore(ChangePasswordActivity.this);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password=passwordED.getText().toString();
                String confirmPassword=confirmPasswordED.getText().toString();
                if(password.equals("")||confirmPassword.equals("")){
                    message.displayShort("Kindly, fill in all the values");
                }
                else if(!password.equals(confirmPassword)){
                    message.displayShort("Passwords don't match");
                }
                else{
                    ConnectivityManager cm = (ConnectivityManager) ChangePasswordActivity.this.
                            getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = cm.getActiveNetworkInfo();
                    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                        //Connectivity
                        new ChangePassword().execute();
                    }
                    else{
                        //No connectivity
                        message.displayShort("No internet connectivity");

                    }
                }
            }
        });
    }

    /**
     *
     * Background task for calling change password API
     */

    private class ChangePassword extends AsyncTask<String,Void,JSONObject>{
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ChangePasswordActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            final URL[] url = new URL[1];
            try{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            url[0] = new URL(Constants.CHANGE_PASSWORD+"Username="+localStore.getUserDetails().getUserName()+"&OldPassword="+localStore.getUserDetails().getPassword()+
                                    "&NewPassword="+passwordED.getText().toString()+"&Token="+localStore.getUserDetails().getToken()+
                                    "&Imeino=");
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }

                    }
                });
                HttpURLConnection conn = (HttpURLConnection) url[0].openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ChangePasswordActivity.this, "Password changed successfully", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Intent intent = new Intent(ChangePasswordActivity.this,LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ChangePasswordActivity.this, "Some error occurred, try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ChangePasswordActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
            }finally {
                progressDialog.dismiss();
            }
        }
    }

}
