package in.cableguy.cableguy.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;

import in.cableguy.cableguy.R;

/**
 *
 * @author Raj Mehra
 * The type Terms activity.
 */
public class TermsActivity extends AppCompatActivity {
    /**
     * The M webview.
     */
    WebView mWebview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        getSupportActionBar().setTitle(R.string.termsTitle);
        mWebview = (WebView)findViewById(R.id.termsWebView);
        String yourContentStr = String.valueOf(Html
                .fromHtml("<![CDATA[<body style=\"text-align:justify;color:#222222;font-size:14px \">"
                        + getString(R.string.termsText)
                        + "</body>]]>"));
        mWebview.loadData(yourContentStr,"text/html", "utf-8");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(TermsActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
}
