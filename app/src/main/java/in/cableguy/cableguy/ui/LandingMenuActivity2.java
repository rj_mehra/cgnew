package in.cableguy.cableguy.ui;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.prowesspride.api.Printer_GEN;
import com.prowesspride.api.Setup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.btprinter.BTPrinter;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.GlobalApp;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.OfflineDBHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author Raj Mehra
 * The type Landing menu activity 2.
 * @deprecated This has been deprecated and replaced with @link in.cableguy.ui.NavDrawerActivity2
 * @since 10 October 2017
 * @see NavDrawerActivity2
 */
public class LandingMenuActivity2 extends AppCompatActivity {
    /**
     * The Billing ll.
     */
    LinearLayout billingLL, /**
     * The Print ll.
     */
    printLL, /**
     * The Last transaction ll.
     */
    lastTransactionLL, /**
     * The Terminal report ll.
     */
    terminalReportLL, /**
     * The Change password ll.
     */
    changePasswordLL, /**
     * The Test print ll.
     */
    testPrintLL;
    /**
     * The Current balance tv.
     */
    TextView  currentBalanceTV;
    /**
     * The Bt printer.
     */
    BTPrinter btPrinter;
    /**
     * The Fos name.
     */
    TextView fosName, /**
     * The Offline cases.
     */
    offlineCases, /**
     * The Lco name.
     */
    lcoName, /**
     * The Test print tv.
     */
    testPrintTV;
    /**
     * The Current balance tr.
     */
    TableRow currentBalanceTR;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Ecaf ll.
     */
    LinearLayout ecafLL, /**
     * The Logout ll.
     */
    logoutLL;
    /**
     * The Ecaf iv.
     */
    ImageView ecafIV, /**
     * The Logout iv.
     */
    logoutIV;
    /**
     * The Ret val.
     */
    int iRetVal;
    /**
     * The Result.
     */
    boolean result;
    /**
     * The Ptr gen.
     */
    public Printer_GEN ptrGen;
    /**
     * The Trans mode.
     */
    RadioGroup transMode;
    /**
     * The Pos.
     */
    int pos;
    /**
     * The Online rb.
     */
    RadioButton onlineRB, /**
     * The Offline rb.
     */
    offlineRB;
    /**
     * The Db helper.
     */
    OfflineDBHelper dbHelper;
    /**
     * The Count.
     */
    long count;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_menu);
        overridePendingTransition(0, 0);
        getSupportActionBar().setTitle("Menu");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        printLL=(LinearLayout)findViewById(R.id.printLL);
        lastTransactionLL=(LinearLayout)findViewById(R.id.lastTransactionLL);
        terminalReportLL=(LinearLayout)findViewById(R.id.terminalReportLL);
        changePasswordLL=(LinearLayout)findViewById(R.id.changePasswordLL);
        testPrintLL=(LinearLayout)findViewById(R.id.testPrintLL);
        testPrintTV = (TextView)findViewById(R.id.testPrintTV);
        currentBalanceTR = (TableRow)findViewById(R.id.currentBalanceTR);
        billingLL = (LinearLayout)findViewById(R.id.billingLL);
        currentBalanceTV = (TextView)findViewById(R.id.currentBalance);
        ecafLL = (LinearLayout)findViewById(R.id.ecafLL);
        logoutLL = (LinearLayout)findViewById(R.id.logoutLL);
        ecafIV = (ImageView)findViewById(R.id.ecafIV);
        logoutIV = (ImageView)findViewById(R.id.logoutIV);
        offlineCases = (TextView)findViewById(R.id.offlineCasesTV);
        transMode = (RadioGroup)findViewById(R.id.transMode);
        localStore=new LocalStore(LandingMenuActivity2.this);
        Values.TRANS_MODE="Online";
        dbHelper = new OfflineDBHelper(LandingMenuActivity2.this);
        try{
            count = dbHelper.dataCount();
            Log.e("Count ",count + "");
        }catch (Exception ex){
            Log.e("Can't Count ", ex.toString());
        }
        offlineCases.setText("Pending Offline Cases   -->"+count);
        transMode = (RadioGroup)findViewById(R.id.transMode);
        onlineRB = (RadioButton)findViewById(R.id.onlineRB);
        offlineRB = (RadioButton)findViewById(R.id.offlineRB);

        if (Values.ALLOW_OFFLINE.equalsIgnoreCase("Y")) {
            transMode.setVisibility(View.VISIBLE);
        }else{

            transMode.setVisibility(View.GONE);
        }
        transMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                pos=transMode.indexOfChild(findViewById(checkedId));
                switch(pos){
                    case 0:
                        Values.TRANS_MODE="Online";
                        break;
                    case 1:
                        Values.TRANS_MODE="Offline";
                        Intent intent = new Intent(LandingMenuActivity2.this, OfflineActivity.class);
                        startActivity(intent);
                        break;
                }
            }
        });
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
            if(btPrinter.isBTConnected){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LandingMenuActivity2.this, "Printer Connected", Toast.LENGTH_SHORT).show();
                    }
                });
            }else{
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            btPrinter.OpenBTConnection();
                        }catch (Exception ex){
                            Log.e("Bluetooth Error", ex.toString());
                        }
                    }
                }).start();

            }
        }else{
            btPrinter=new BTPrinter(LandingMenuActivity2.this);
            if(btPrinter.isBTConnected){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LandingMenuActivity2.this, "Printer Connected", Toast.LENGTH_SHORT).show();
                    }
                });
            }else{
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            btPrinter.OpenBTConnection();
                        }catch (Exception ex){
                            Log.e("Bluetooth Error", ex.toString());
                        }
                    }
                }).start();

            }
        }
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
            try {
                GlobalApp.setup = new Setup();
                result = GlobalApp.setup.blActivateLibrary(LandingMenuActivity2.this,R.raw.licencefull_pride_gen);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(result){
                Log.e("Library Activated ",result+"");
            }else{
                Log.e("Library failed", result+"");
            }
        }else{

        }
        billingLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Values.TRANS_MODE.equalsIgnoreCase("Online")){
                    Intent intent = new Intent(LandingMenuActivity2.this,NormalTypeActivity.class);
                    Values.redirect = "LCOType";
                    Values.lastTransaction = "";
                    Values.listType = "";
                    try{
                        Cursor c = dbHelper.getAllData();
                        if(count>0){
                            c.moveToFirst();

                            while(!c.isAfterLast()){
                                new uploadOfflineTask().execute(c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_SUBSNAME)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_SUBSCODE)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_AMOUNT)),
                                        c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_ADDICHARGES)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_MOBILE)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_REMARK)),
                                        c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_PAYMODE)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_BANKNAME)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_CHEQUEDATE)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_CHEQUENO)),c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_USERNAME)));
                                c.moveToNext();
                            }

                            dbHelper.deleteAllData();


                        }

                    }catch (Exception ex){
                        Log.e("error ",ex.toString());
                    }
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(LandingMenuActivity2.this,OfflineActivity.class);
                    startActivity(intent);
                }
            }
        });

        ecafLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Values.ALLOWECAF.equalsIgnoreCase("N")){
                    Toast.makeText(LandingMenuActivity2.this, "Please contact support for ECAF Service", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(LandingMenuActivity2.this,ECAFCustomerTypeActivity.class);
                    startActivity(intent);
                }

            }
        });
        if(Values.CREDITLIMITFLAG.equalsIgnoreCase("Y")){
            currentBalanceTR.setVisibility(View.VISIBLE);
            currentBalanceTV.setText(Values.currentBalance);
        }
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){

        }else{
            btPrinter=new BTPrinter(LandingMenuActivity2.this);
        }
        testPrintLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String line="----------------------------";

                        String data = "       TEST PRINT"+"\n"+line+"\n"+"Powered By: www.cableguy.in"+"\n"+
                                "v "+Values.currentVersion+"\n"+"\n"+"\n"+"\n"+"\n";
                        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
                            if(result){
                                try {
                                    Log.e("Before ptrgen", "");
                                    ptrGen = new Printer_GEN(GlobalApp.setup, btPrinter.evoluteOutputStream, btPrinter.evoluteInputStream);
                                    ptrGen.iAddData((byte) 0x01,data);

                                    if(btPrinter.isBTConnected){
                                        iRetVal = ptrGen.iStartPrinting(1);
                                    }else{
                                        btPrinter.OpenBTConnection();
                                        iRetVal = ptrGen.iStartPrinting(1);
                                    }
                                } catch (Exception e) {
                                    Log.e("Printer Error", e.toString());
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(LandingMenuActivity2.this, "Printer Error", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            }else{
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(LandingMenuActivity2.this, "Cannot connect to the printer, Please try again!", Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }
                        }else{
                            btPrinter.PrintLine(data,'M');
                        }
                    }
                }).start();
            }
        });

        testPrintTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String line="----------------------------";

                        String data = "       TEST PRINT"+"\n"+line+"\n"+"Powered By: www.cableguy.in"+"\n"+
                                "v "+Values.currentVersion+"\n"+"\n"+"\n"+"\n"+"\n";
                        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
                            if(result){
                                try {
                                    Log.e("Before ptrgen", "");
                                    ptrGen = new Printer_GEN(GlobalApp.setup, btPrinter.evoluteOutputStream, btPrinter.evoluteInputStream);
                                    ptrGen.iAddData((byte) 0x01,data);
                                    if(btPrinter.isBTConnected){
                                        iRetVal = ptrGen.iStartPrinting(1);
                                    }else{
                                        btPrinter.OpenBTConnection();
                                        iRetVal = ptrGen.iStartPrinting(1);
                                    }

                                } catch (Exception e) {
                                    Log.e("Printer Error", e.toString());
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(LandingMenuActivity2.this, "Printer Error", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            }else{
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(LandingMenuActivity2.this, "Cannot connect to the printer, Please try again!", Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }
                        }else{
                            btPrinter.PrintLine(data,'M');
                        }
                    }
                }).start();
            }
        });

        printLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LandingMenuActivity2.this,ReprintActivity.class);
                startActivity(intent);
            }
        });

        lastTransactionLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Values.redirect="LCOSubsTransaction";
                Log.e("Lco Subs transaction",Values.redirect);/*
                Intent intent = new Intent(LandingMenuActivity2.this,LastTransactionActivity.class);*/
                Values.lastTransaction = "LCOSubsTransaction";
                Intent intent = new Intent(LandingMenuActivity2.this,LastTransactionActivity.class);
                startActivity(intent);
            }
        });

        terminalReportLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LandingMenuActivity2.this,ReportActivity.class);
                startActivity(intent);

            }
        });

        changePasswordLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LandingMenuActivity2.this,ChangePasswordActivity.class);
                startActivity(intent);

            }
        });
        logoutLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LandingMenuActivity2.this, CloseActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP );
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(LandingMenuActivity2.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    /**
     * Background task for calling Offline API for transaction upload
     */

    private class uploadOfflineTask extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();


        /**
         * The Username off.
         */
        String usernameOff, /**
         * The Subs name.
         */
        subsName, /**
         * The Subs code.
         */
        subsCode, /**
         * The Paid amount.
         */
        paidAmount, /**
         * The Add charges.
         */
        addCharges, /**
         * The Mobile.
         */
        mobile, /**
         * The Remark.
         */
        remark, /**
         * The Pay mode.
         */
        payMode, /**
         * The Bank name.
         */
        bankName, /**
         * The Cheque date.
         */
        chequeDate, /**
         * The Cheque no.
         */
        chequeNo;

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            usernameOff = params[10];
            subsName = params[0];
            subsCode = params[1];
            paidAmount = params[2];
            addCharges = params[3];
            mobile = params[4];
            remark = params[5];
            payMode = params[6];
            bankName = params[7];
            chequeDate = params[8];
            chequeNo = params[9];
            try {
                String uri = Constants.OFFLINE_API + "Username=" + usernameOff + "&SubsName=" + subsName + "&SubsCode=" + subsCode + "&PayMode=" + payMode
                        + "&Amount=" + paidAmount + "&AddiCharges=" + addCharges + "&Mobile=" + mobile + "&Remark=" + remark
                        + "&ChqDt=" + chequeDate + "&ChqNo=" + chequeNo + "&BankName=" + bankName;

                url = new URL(uri.replaceAll(" ","%20"));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            String status;
            JSONArray dataArray;
            try {
                status = json.getString("STATUS");
                dataArray = json.getJSONArray("Data");
                switch (status) {
                    case "0" :
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                offlineCases.setText("Pending Offline Cases   -->"+count);
                            }
                        });
                        Toast.makeText(LandingMenuActivity2.this, "All pending offline task has been uploaded, Thank you", Toast.LENGTH_SHORT).show();
                        break;
                    case "1":
                        Toast.makeText(LandingMenuActivity2.this, "Some error occurred uploading offline data, please login again or try later", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(LandingMenuActivity2.this, "No Data found, Offline Tasks Already uploaded", Toast.LENGTH_SHORT).show();
                        break;
                }
            } catch (Exception ex) {
            }
        }
    }
}
