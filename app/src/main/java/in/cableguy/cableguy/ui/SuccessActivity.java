package in.cableguy.cableguy.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Values;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 *
 * @author Raj Mehra
 * The type Success activity.
 *
 * Activity to show successful transaction
 */
public class SuccessActivity extends AppCompatActivity {
    /**
     * The Next button.
     */
    Button nextButton;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);
        overridePendingTransition(0, 0);
        nextButton = (Button)findViewById(R.id.nextButton);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Values.redirect.equalsIgnoreCase("NormalType")){
                    Log.e("Values.redirect Normal", Values.redirect);
                    Intent intent = new Intent(SuccessActivity.this, SearchActivity.class);
                    Values.selectedCustomer=null;
                    Values.selectedBuilding=null;
                    Values.stbNumber ="";
                    Values.selectedCustomerNumber = "";
                    Values.cp1 = "";
                    Values.cardNumber = "";
                    Values.customerNumber = "";
                    startActivity(intent);
                    finish();


                }else if(Values.redirect.equalsIgnoreCase("LCOType")){
                    Log.e("Values.redirect LCO", Values.redirect);
//                    Intent intent = new Intent(SuccessActivity.this, NormalTypeActivity.class);
                    Intent intent = new Intent(SuccessActivity.this, SearchActivity.class); //changed for avoiding redirection
                    Values.selectedCustomer = null;
                    Values.stbNumber ="";
                    Values.selectedCustomerNumber = "";
                    Values.cp1 = "";
                    Values.cardNumber = "";
                    Values.customerNumber = "";
                    startActivity(intent);
                    finish();
                }else if(Values.redirect.equalsIgnoreCase("BuildingType")){
                    Intent intent = new Intent(SuccessActivity.this,CustomerDetailsActivity.class);
/*                    Values.selectedCustomer=null;*/
                    Log.e("Values.redirect ELSE", Values.redirect);
/*                    Values.selectedBuilding = "Select Building";
                    Values.selectedZone = "Select Zone";
                    Values.selectedArea = "Select Area";*/
                    Values.stbNumber ="";
                    Values.selectedCustomerNumber = "";
                    Values.cp1 = "";
                    Values.cardNumber = "";
                    Values.customerNumber = "";
                    startActivity(intent);
                    finish();
                }else if(Values.MODULE.equalsIgnoreCase("KYCE")){
                    Intent intent = new Intent(SuccessActivity.this,ECAFCustomerTypeActivity.class);
                    Values.setAddressProofUrl="";
                    Values.setCustomerPhotoUrl="";
                    Values.setIdProofUrl="";
                    Values.setSignUrl="";
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }else if(Values.MODULE.equalsIgnoreCase("KYCI")){
                    Intent intent = new Intent(SuccessActivity.this,ECAFCustomerTypeActivity.class);
                    Values.setAddressProofUrl="";
                    Values.setCustomerPhotoUrl="";
                    Values.setIdProofUrl="";
                    Values.setSignUrl="";
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        moveTaskToBack(true);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
