package in.cableguy.cableguy.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.adapter.SubsLastTransactionAdapter;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.MessageHelper;
import in.cableguy.cableguy.helper.Transaction;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author Raj Mehra
 * The type Last transaction list activity.
 *
 * This lists the transaction which is based on the selection from LastTransactionActivity
 * @see LastTransactionActivity
 *
 */
public class LastTransactionListActivity extends AppCompatActivity {
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Message.
     */
    MessageHelper message;
    /**
     * The Transactions.
     */
    ArrayList<Transaction> transactions=new ArrayList<>();
    /**
     * The Adapter.
     */
    SubsLastTransactionAdapter adapter;
    /**
     * The Transaction list rv.
     */
    RecyclerView transactionListRV;
    /**
     * The No txn tv.
     */
    TextView noTxnTV;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_last_transaction);
        overridePendingTransition(0, 0);
        localStore=new LocalStore(LastTransactionListActivity.this);
        message=new MessageHelper(LastTransactionListActivity.this);
        adapter=new SubsLastTransactionAdapter(transactions,LastTransactionListActivity.this);
        transactionListRV=(RecyclerView)findViewById(R.id.lastTransactionRV);
        noTxnTV=(TextView)findViewById(R.id.noTxnTV);
        getSupportActionBar().setTitle("Last Transaction");
        noTxnTV.setVisibility(View.GONE);
        transactionListRV.setAdapter(adapter);
        // Set layout manager to position the items
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(LastTransactionListActivity.this, LinearLayoutManager.VERTICAL, false);
        transactionListRV.setLayoutManager(layoutManager);
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onResume() {
        super.onResume();
        ConnectivityManager cm = (ConnectivityManager) LastTransactionListActivity.this.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            //Connectivity
            if(Values.listType.equals("subs")){
                Values.FETCH_TRANSACTION =0;
                new FetchTransactions().execute();
            }
            else{
                Values.FETCH_TRANSACTION = 1;
                new FetchTransactions().execute();
            }

        }
        else{
            //No connectivity
            message.displayShort("No internet connectivity");

        }

    }


    /**
     *
     * Background task for fetching transactions
     */

    private class FetchTransactions extends AsyncTask<String, Void, JSONObject>{
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LastTransactionListActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url = null;
            try{
                if(Values.FETCH_TRANSACTION==0){
                    url = new URL(Constants.SUBSCRIBER_TRANSACTION_LAST_5+"Subsid="+Values.selectedCustomer.getId()+"&Username="+localStore.getUserDetails().getUserName()+
                            "&Token="+localStore.getUserDetails().getToken());
                }else if(Values.FETCH_TRANSACTION==1){
                    url = new URL(Constants.USER_TRANSACTION_LAST_3+"Username="+localStore.getUserDetails().getUserName()+"&Token="+localStore.getUserDetails().getToken());
                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LastTransactionListActivity.this, "Some error occurred, Please try again!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            final JSONArray dataArray;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    transactions.clear();
                }
            });

            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        LastTransactionListActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                for(int i=0; i<dataArray.length(); i++) {
                                    try {

                                        noTxnTV.setVisibility(View.GONE);
                                        final JSONObject obj = dataArray.getJSONObject(i);
                                        String subscriberName, subscriberNumber, amount, paymentMode, dateTime, receiptNo;
                                        subscriberNumber = obj.getString("SubscriberNumber");
                                        subscriberName = obj.getString("SubscriberName");
                                        amount = obj.getString("Amount");
                                        paymentMode = obj.getString("PaymentMode");
                                        dateTime = obj.getString("ReceiptDate");
                                        receiptNo = obj.getString("ReceiptNo");
                                        Transaction transaction = new Transaction();
                                        switch (paymentMode) {
                                            case "C":
                                                transaction.setPaymentMode("Cash");
                                                break;
                                            case "Q":
                                                transaction.setPaymentMode("Cheque");
                                                break;
                                            case "O":
                                                transaction.setPaymentMode("Online");
                                                break;
                                            default:
                                                transaction.setPaymentMode("");
                                        }
                                        transaction.setAmount(amount);
                                        transaction.setDateTime(dateTime);
                                        transaction.setReceiptNo(receiptNo);
                                        transaction.setSubscriberName(subscriberName);
                                        transaction.setSubscriberNumber(subscriberNumber);
                                        transactions.add(transaction);
                                        LastTransactionListActivity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                adapter.notifyDataSetChanged();
                                            }
                                        });
                                    } catch (Exception ex) {

                                    }
                                }
                                }});
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                noTxnTV.setVisibility(View.VISIBLE);
                                Toast.makeText(LastTransactionListActivity.this, "Some error occurred, Please try again", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                noTxnTV.setVisibility(View.VISIBLE);
                                Toast.makeText(LastTransactionListActivity.this, "Cannot connect to server, Please check yo", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        noTxnTV.setVisibility(View.VISIBLE);
                    }
                });
            }finally {
                progressDialog.dismiss();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(LastTransactionListActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
}
