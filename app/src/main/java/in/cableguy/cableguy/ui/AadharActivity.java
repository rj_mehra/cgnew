package in.cableguy.cableguy.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Values;

/**
 * @author Raj Mehra
 * The Aadhar activity.
 */
public class AadharActivity extends AppCompatActivity {
    /**
     * The Submit button.
     */
    Button submitButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aadhar);
        getSupportActionBar().setTitle("Enter Aadhar");
        submitButton = (Button)findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(AadharActivity.this, "Aadhar based kyc functionality will be added soon!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /**
         * Checks if Aadhar option is enabled
         *
         */
        Values.isAadhar = false;
    }
}
