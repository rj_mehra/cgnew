package in.cableguy.cableguy.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.Customer;
import in.cableguy.cableguy.helper.DrawingView;

import static in.cableguy.cableguy.constants.Values.mBitmap;
import static in.cableguy.cableguy.constants.Values.mCanvas;

/**
 *
 * @author Raj Mehra
 * The type Signature save activity.
 *
 * This activity is for taking and saving signature for Ecaf
 *
 * @see DocumentActivity
 *
 */
public class SignatureSaveActivity extends AppCompatActivity {
    /**
     * The Save button.
     */
    Button saveButton;
    /**
     * The Reset button.
     */
    Button resetButton;
    /**
     * The This activity.
     */
    Activity thisActivity;
    /**
     * The Drawing view.
     */
    DrawingView drawingView;
    /**
     * The Customer.
     */
    Customer customer;
    final private static int My_PERMIMSSIONS_READ_PHONE = 300;
    final private static int MY_PERMISSIONS_LOCATION =400;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature_save);
        getSupportActionBar().setTitle("Sign");
        saveButton=(Button)findViewById(R.id.saveTemplate);
        resetButton=(Button)findViewById(R.id.resetCanvas);
        drawingView=(DrawingView)findViewById(R.id.drawingView);
        thisActivity=SignatureSaveActivity.this;

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(thisActivity,SignatureSaveActivity.class);
                startActivity(i);
                finish();
            }
        });
        drawingView.setCustomViewListener(new DrawingView.CustomViewListener() {
            @Override
            public void onUpdateValue(String updatedValue) {

            }

            @Override
            public void plotCoordinates(Float x, Float y) {

            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Write permission ",Values.WRITE_EXT_STORAGE_PERM+"");
                Log.e("Read permission ",Values.READ_EXT_STORAGE_PERM+"");
                if(Values.WRITE_EXT_STORAGE_PERM && Values.READ_EXT_STORAGE_PERM){
                    saveImage();
                }
                else{
                    callForRead();
                }


            }
        });

    }

    /**
     * Save image.
     */
    public void saveImage(){
        mBitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        new Thread(new Runnable() {
            @Override
            public void run() {
                storeImage(getBitmapFromView(drawingView));
            }
        }).start();
    }

    /**
     * Gets bitmap from view.
     *
     * @param view the view
     * @return the bitmap from view
     */
    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    /**
     * Call for read.
     */
    public void callForRead() {

        int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
        try {
            if (currentAPIVersion >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(SignatureSaveActivity.this,
                        Manifest.permission.READ_PHONE_STATE)
                        != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(SignatureSaveActivity.this,
                                Manifest.permission.WAKE_LOCK)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(SignatureSaveActivity.this,
                                Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED||
                        ContextCompat.checkSelfPermission(SignatureSaveActivity.this,
                                Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(SignatureSaveActivity.this,
                                Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(SignatureSaveActivity.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(SignatureSaveActivity.this,
                                Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(SignatureSaveActivity.this,
                                Manifest.permission.BLUETOOTH)
                                != PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "called");
                    ActivityCompat.requestPermissions(SignatureSaveActivity.this,
                            new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.WAKE_LOCK,
                                    Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.BLUETOOTH},
                            My_PERMIMSSIONS_READ_PHONE);

                    Values.READ_EXT_STORAGE_PERM = true;
                    Values.WRITE_EXT_STORAGE_PERM = true;

                }else if(ContextCompat.checkSelfPermission(SignatureSaveActivity.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(SignatureSaveActivity.this,
                                Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(SignatureSaveActivity.this,
                                Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(SignatureSaveActivity.this,
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_LOCATION);

                }else{
                    Values.READ_EXT_STORAGE_PERM = true;
                    Values.WRITE_EXT_STORAGE_PERM = true;
                }
            }
            else{
                Values.READ_EXT_STORAGE_PERM = true;
                Values.WRITE_EXT_STORAGE_PERM = true;
            }
        } catch (Exception ex) {
            Log.e("Permission Exception", ex+" ");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case My_PERMIMSSIONS_READ_PHONE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Values.READ_PERM_GRANT=true;

                    //message.displayShort("READ DONE "+Values.READ_PERM_GRANT);
                }
                else{
                    //message.displayShort("READ DONE "+Values.READ_PERM_GRANT);

//                    message.displayLong(UIMessages.PERMS_REQUIRED_1);
                }
                break;
            case MY_PERMISSIONS_LOCATION:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Values.LOC_PERM_GRANT=true;
                    //message.displayShort("LOC DONE "+Values.LOC_PERM_GRANT);

                }
                else{
                    //message.displayShort("LOC DONE "+Values.LOC_PERM_GRANT);
//                    message.displayLong(UIMessages.PERMS_REQUIRED_1);

                }
                break;

            default:
                break;
        }
    }

    private void storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        DocumentActivity.signFileUri= Values.signImageUri= Uri.fromFile(pictureFile);
        thisActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                /*progressDialog = new KYCProgressDialog(thisActivity);
                progressDialog.show(UIMessages.PROCESSING, null, true);*/
            }
        });
        if (pictureFile == null) {
            Log.d("",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
            Intent i=new Intent(SignatureSaveActivity.this,DocumentActivity.class);
            i.putExtra("Sign","true");
            startActivity(i);
            Log.d("image ", "Saved Successfully" );
            thisActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    message.displayShort("Saved Successfully");
                }
            });
            /*thisActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            });*/
        } catch (final FileNotFoundException e) {
            Log.d("error ", "File not found: " + e.getMessage());
            thisActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    /*message.displayLong("FILE NOT FOUND"+e.toString());
                    progressDialog.dismiss();*/
                }
            });
        } catch (final IOException e) {

            Log.d("Image ", "Error accessing file: " + e.getMessage());
            thisActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                   /* message.displayLong("IOE "+e.toString());
                    progressDialog.dismiss();*/
                }
            });
        }
    }
    /** Create a File for saving an image or video */
    private  File getOutputMediaFile(){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()+"/CG/UploadedImages");
                /*+ "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");*/

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());

        File mediaFile;
        String mImageName=Values.ECAF_CUSTOMER.getIn_subsname()+"_signimg_" +timeStamp +".png";
        Values.signImageName=mImageName;
        DocumentActivity.signImageName=mImageName;
        Values.signImageFile=mediaStorageDir.getPath() + File.separator + mImageName;
        mediaFile = new File(Values.signImageFile);
        return mediaFile;
    }

    private void beginUpload(String filePath) {
        if (filePath == null) {
            Toast.makeText(this, "Could not find the filepath of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(filePath);
        Log.e("NameofImage", file.getName());
        /*TransferObserver observer = transferUtility.upload(Constants.BUCKET_NAME, file.getName(),
                file);*/
        /*
         * Note that usually we set the transfer listener after initializing the
         * transfer. However it isn't required in this sample app. The flow is
         * click upload button -> start an activity for image selection
         * startActivityForResult -> onActivityResult -> beginUpload -> onResume
         * -> set listeners to in progress transfers.
         */
        // observer.setTransferListener(new UploadListener());
    }
}
