package in.cableguy.cableguy.ui;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Values;

/**
 *
 * @author Raj Mehra
 * The type Verification type activity.
 *
 * This activity is for use during ecaf which decides whether to allow manual or aadhar based KYC
 *
 */
public class VerificationTypeActivity extends AppCompatActivity {
    /**
     * The Manual ll.
     */
    LinearLayout manualLL, /**
     * The Aadhar ll.
     */
    aadharLL;
    /**
     * The Manual iv.
     */
    ImageView manualIV, /**
     * The Aadhar iv.
     */
    aadharIV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_type);
        getSupportActionBar().setTitle("Select Visit Type");
        manualLL = (LinearLayout)findViewById(R.id.manualLL);
        aadharLL = (LinearLayout)findViewById(R.id.aadharLL);
        aadharIV = (ImageView)findViewById(R.id.aadharIV);
        manualIV = (ImageView)findViewById(R.id.manualIV);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.residenceColor));

            ActionBar actionBar = getSupportActionBar();
            try{
                actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.residenceColor)));
            }catch (Exception ex){
                Log.e("Action Bar exception", ex.toString());
            }
        }
        manualLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Values.isNew){
                    Intent intent = new Intent(VerificationTypeActivity.this,ECAFNewCustomerActivity.class);
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(VerificationTypeActivity.this,ECAFExistingCustomerActivity.class);
                    startActivity(intent);
                }
            }
        });

        aadharLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Values.isAadhar = true;
                Intent intent = new Intent(VerificationTypeActivity.this, AadharActivity.class);
                startActivity(intent);
            }
        });

        Glide.with(this).load(R.drawable.manual).apply(new RequestOptions().fitCenter().placeholder(R.drawable.noimage)).transition(new DrawableTransitionOptions().crossFade())
                .into(manualIV);
        Glide.with(this).load(R.drawable.aadhar).apply(new RequestOptions().fitCenter().placeholder(R.drawable.noimage)).transition(new DrawableTransitionOptions().crossFade())
                .into(aadharIV);
    }
}
