package in.cableguy.cableguy.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Values;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author Raj Mehra
 * The type Customer details activity.
 *
 * This activity is for building wise user
 */
public class CustomerDetailsActivity extends AppCompatActivity {
    /**
     * The Selected zone.
     */
    Button selectedZone, /**
     * The Selected building.
     */
    selectedBuilding, /**
     * The Selected customer.
     */
    selectedCustomer;
    /**
     * The Next button.
     */
    Button nextButton;
    /**
     * The Intent.
     */
    Intent intent;
    /**
     * The Retrieve.
     */
    Bundle retrieve;
    private AdView mAdView;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(CustomerDetailsActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_details);
        overridePendingTransition(0, 0);
        getSupportActionBar().setTitle("Search for Customer");

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        selectedZone = (Button)findViewById(R.id.selectedZone);
        selectedBuilding = (Button)findViewById(R.id.selectedBuilding);
        selectedCustomer = (Button)findViewById(R.id.selectedCustomer);
        retrieve=getIntent().getExtras();
        nextButton = (Button)findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Values.selectedZone.equalsIgnoreCase("Select Zone")||
                        Values.selectedBuilding==null||Values.selectedCustomerName.equalsIgnoreCase("Select Customer")){
                    Toast.makeText(CustomerDetailsActivity.this,"Please select all the values",Toast.LENGTH_LONG).show();
                }
                else{
                    if(!Values.onCustomerSelect.equals("subslasttxn")){
                        Values.searchType = "CC";
                        Intent intent = new Intent(CustomerDetailsActivity.this, BillingDetailsActivity.class);
                        startActivity(intent);
                    }
                    else{
                        Values.listType="subs";
                        Intent intent = new Intent(CustomerDetailsActivity.this, LastTransactionListActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });

        selectedZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(CustomerDetailsActivity.this,ZoneListActivity.class);
                startActivity(intent);
            }
        });



        selectedBuilding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Values.selectedZone.equalsIgnoreCase("Select Zone")){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CustomerDetailsActivity.this, "Please select Zone and area.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    intent = new Intent(CustomerDetailsActivity.this,BuildingListActivity.class);
                    finish();
                    startActivity(intent);
                }
            }
        });

        selectedCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Values.selectedZone.equalsIgnoreCase("Select Zone")||Values.selectedBuilding==null){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CustomerDetailsActivity.this, "Please select zone, area and building", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    intent = new Intent(CustomerDetailsActivity.this,CustomerListActivity.class);
                    Values.customerListRedirect="CustomerDetailsActivity";
                    startActivity(intent);
                    finish();
                }
            }
        });
        selectedZone.setText(Values.selectedZone);
        selectedBuilding.setText(Values.buildingName);
        selectedCustomer.setText(Values.selectedCustomerName);


    }

    /**
     * On nothing selected.
     *
     * @param arg0 the arg 0
     */
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        redirect();
    }

    /**
     * Redirect.
     */
    public void redirect(){
        if(Values.redirect.equalsIgnoreCase("BuildingSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(CustomerDetailsActivity.this, NavDrawerActivity.class);
            Values.redirect="";
            Values.listType="";
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }else if(Values.redirect.equalsIgnoreCase("NormalSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("NormalSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(CustomerDetailsActivity.this, NavDrawerActivity3.class);
            Values.redirect="";
            Values.listType="";
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }else if(Values.redirect.equalsIgnoreCase("LCOSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("LCOSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(CustomerDetailsActivity.this, NavDrawerActivity2.class);
            Values.redirect="";
            Values.listType="";
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }else if(Values.redirect.equalsIgnoreCase("BuildingType")){
            Intent intent = new Intent(CustomerDetailsActivity.this, NavDrawerActivity.class);
            Values.redirect="";
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }else if(Values.redirect.equalsIgnoreCase("NormalType")){
            Intent intent = new Intent(CustomerDetailsActivity.this, NavDrawerActivity3.class);
            Values.redirect="";
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }else if ((Values.redirect.equalsIgnoreCase("LCOType"))){
            Intent intent = new Intent(CustomerDetailsActivity.this, NavDrawerActivity2.class);
            Values.redirect="";
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }


    }
}
