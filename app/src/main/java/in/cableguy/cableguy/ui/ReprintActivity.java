package in.cableguy.cableguy.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.prowesspride.api.Printer_GEN;
import com.prowesspride.api.Setup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.btprinter.BTPrinter;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.GlobalApp;
import in.cableguy.cableguy.helper.LocalStore;

/**
 *
 * @author Raj Mehra
 * The type Reprint activity.
 *
 *
 */
public class ReprintActivity extends AppCompatActivity {
    /**
     * The Customer name tv.
     */
    TextView customerNameTV, /**
     * The Address tv.
     */
    addressTV, /**
     * The Mobile number tv.
     */
    mobileNumberTV, /**
     * The Due date tv.
     */
    dueDateTV, /**
     * The Subscriber code tv.
     */
    subscriberCodeTV, /**
     * The Subscriber id tv.
     */
    subscriberIdTV, /**
     * The Noof tv.
     */
    noofTV, /**
     * The Arrears tv.
     */
    arrearsTV, /**
     * The Stbcharges tv.
     */
    stbchargesTV,
    /**
     * The Basics tv.
     */
    basicsTV, /**
     * The Sertax tv.
     */
    sertaxTV, /**
     * The Enttax tv.
     */
    enttaxTV, /**
     * The Total tv.
     */
    totalTV, /**
     * The Paidamt tv.
     */
    paidamtTV, /**
     * The Balance tv.
     */
    balanceTV, /**
     * The Addcharges tv.
     */
    addchargesTV, /**
     * The Amtpaid tv.
     */
    amtpaidTV, /**
     * The Remarks tv.
     */
    remarksTV, /**
     * The Sgst tv.
     */
    sgstTV, /**
     * The Cgst tv.
     */
    cgstTV, /**
     * The Igst tv.
     */
    igstTV,
    /**
     * The Invoice no tv.
     */
    invoiceNoTV, /**
     * The Receipt no tv.
     */
    receiptNoTV;
    /**
     * The Print button.
     */
    Button printButton;
    /**
     * The Bt printer.
     */
    BTPrinter btPrinter;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    /**
     * The Lco name.
     */
    String lcoName, /**
     * The Lco address.
     */
    lcoAddress, /**
     * The Lco phone 1.
     */
    lcoPhone1, /**
     * The Lco phone 2.
     */
    lcoPhone2, /**
     * The Cust name.
     */
    custName, /**
     * The Cust mobile.
     */
    custMobile, /**
     * The Bank name.
     */
    bankName, /**
     * The Cheque number.
     */
    chequeNumber, /**
     * The Pay mode.
     */
    payMode, /**
     * The Pay date.
     */
    payDate, /**
     * The Receipt number.
     */
    receiptNumber, /**
     * The Sgst.
     */
    sgst, /**
     * The Cgst.
     */
    cgst, /**
     * The Igst.
     */
    igst;
    /**
     * The Ret val.
     */
    int iRetVal, /**
     * The Invoice no.
     */
    invoiceNo;
    /**
     * The Result.
     */
    boolean result;
    /**
     * The Ptr gen.
     */
    public  Printer_GEN ptrGen;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reprint);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        printButton = (Button)findViewById(R.id.printButton);
        customerNameTV = (TextView)findViewById(R.id.customerNameTV);
        addressTV = (TextView)findViewById(R.id.addressTV);
        mobileNumberTV = (TextView)findViewById(R.id.mobileNumber);
        dueDateTV = (TextView)findViewById(R.id.dueDate);
        subscriberCodeTV = (TextView)findViewById(R.id.subscriberCode);
        subscriberIdTV = (TextView)findViewById(R.id.subscriberId);
        noofTV = (TextView)findViewById(R.id.noofTV);
        arrearsTV = (TextView)findViewById(R.id.arrearsTV);
        stbchargesTV = (TextView)findViewById(R.id.stbchargesTV);
        basicsTV = (TextView)findViewById(R.id.basicsTV);
        sertaxTV = (TextView)findViewById(R.id.sertaxTV);
        enttaxTV = (TextView)findViewById(R.id.enttaxTV);
        totalTV = (TextView)findViewById(R.id.totalTV);
        paidamtTV = (TextView)findViewById(R.id.paidamtTV);
        balanceTV = (TextView)findViewById(R.id.balanceTV);
        addchargesTV = (TextView)findViewById(R.id.addchargesTV);
        amtpaidTV = (TextView)findViewById(R.id.amtpaidTV);
        remarksTV = (TextView)findViewById(R.id.remarksTV);
        cgstTV = (TextView)findViewById(R.id.cgstTV);
        igstTV = (TextView)findViewById(R.id.igstTV);
        printButton = (Button)findViewById(R.id.printButton);
        invoiceNoTV = (TextView)findViewById(R.id.invoiceNoTV);
        receiptNoTV = (TextView)findViewById(R.id.receiptNoTV);
        getSupportActionBar().setTitle("Duplicate Receipt");
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){

        }else{
            btPrinter=new BTPrinter(ReprintActivity.this);
        }
        localStore = new LocalStore(ReprintActivity.this);
        progressDialog = new ProgressDialog(ReprintActivity.this);
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
            try {
                GlobalApp.setup = new Setup();
                result = GlobalApp.setup.blActivateLibrary(ReprintActivity.this,R.raw.licencefull_pride_gen);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(result){
                Log.e("Library Activated ",result+"");
            }else{
                Log.e("Library failed", result+"");
            }
        }else{

        }
        new Reprint().execute();

        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //String line="----------------------------";
                        String line="------------------------";
                        String data = "\n"+"\n"+"\n"+"\n";
                        data = data+"      Duplicate Copy"+"\n";
                        data = data+"    Invoice Cum Receipt"+"\n"+"\n"+"\n";
                        data = data+lcoName+"\n"+lcoAddress+"\n"+lcoPhone1+"\n"+lcoPhone2+"\n"+line+"\n"
                        +"Cust Id:"+subscriberCodeTV.getText().toString()+"\n"+"Name:"+customerNameTV.getText().toString()+"\n"
                        +"Add:"+addressTV.getText().toString()+"\n"+"Mob:" + mobileNumberTV.getText().toString()+"\n"
                        +line+"\n"+"No.of Tv:"+noofTV.getText().toString()+"\n";
                        try {
                            data = data +"Receipt No:"+receiptNumber+"\n"+"Invoice No:"+invoiceNo+"\n"+""+payDate+"\n"+"\n";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        data = data +line+"\n"+ "Arrears        :"+arrearsTV.getText().toString()+"\n"+"Add STB Charges:"+stbchargesTV.getText().toString()+"\n"+
                                "Basics         :"+basicsTV.getText().toString()+"\n";
                        if(Values.etstFlag.equalsIgnoreCase("Y")){
                            if(sertaxTV.getText().toString().equalsIgnoreCase("0")&& cgstTV.getText().toString().equalsIgnoreCase("0")){
                                data = data+"SGST(0%)       :"+sertaxTV.getText().toString()+"\n"+"CGST(0%)       :"+cgstTV.getText().toString()+"\n"+"IGST(18%)       :"+igstTV.getText().toString();
                            }else{
                                data = data+"SGST(9%)       :"+sertaxTV.getText().toString()+"\n"+"CGST(9%)       :"+cgstTV.getText().toString()+"\n"+"IGST(0%)       :"+igstTV.getText().toString();
                            }
                        }
                        Double prev = 0.0;
                        Double totalPayment = 0.0;
                        try{
                            prev = Double.parseDouble(paidamtTV.getText().toString())-Double.parseDouble(amtpaidTV.getText().toString());
                            totalPayment = Double.parseDouble(amtpaidTV.getText().toString())+Double.parseDouble(addchargesTV.getText().toString());
                        }catch (Exception ex){
                            Log.e("Error",ex.toString());
                        }
                        data = data+"\nEnt Tax        :"+enttaxTV.getText().toString()+"\n"+"Total          :"+totalTV.getText().toString()+"\n"+"Prev. paid     :"+prev+"\n"+
                                line+"\n"+"Addn Charge    :"+addchargesTV.getText().toString()+"\n"+
                                "Current Payment:"+amtpaidTV.getText().toString()+"\n"+"Total Paid     :"+totalPayment+"\n"+
                                "Balance        :"+balanceTV.getText().toString()+"\n"+line+"\n";
                        if(payMode.equalsIgnoreCase("C"))
                        data = data+"Paid By:Cash"+"\n";
                        else{
                            data = data+"Paid By  :Cheque"+"\n"+"Bank Name:     "+bankName+"\n"+"Cheque No:    "+chequeNumber+"\n";
                        }
                        data = data+line+"\n"+"Remark:"+remarksTV.getText().toString()+"\n"+line+"\n";
                        if(Values.etstFlag=="Y"){

                            data = data+"\n"+"Amount is non refundable\n"+
                                    "      Powered By CABLEGUY\n" +
                                    "      WWW.CABLEGUY.IN\n" +
                                    "      Version :"+Values.currentVersion+""+"\n"+"\n"+"\n"+"\n"+"\n";
                        }
                        else{
                            data = data + "      Powered By CABLEGUY\n" +
                                          "      WWW.CABLEGUY.IN\n"+
                                          "      Version :"+Values.currentVersion+""+"\n"+"\n"+"\n"+"\n"+"\n";
                        }
                        data = data +"\n"+"\n"+"\n"+"\n";
                        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
                            if(result){
                                try {
                                    ptrGen = new Printer_GEN(GlobalApp.setup, btPrinter.evoluteOutputStream, btPrinter.evoluteInputStream);
                                    ptrGen.iAddData((byte) 0x01,data);
                                    iRetVal = ptrGen.iStartPrinting(1);
                                } catch (Exception e) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ReprintActivity.this, "Printer Error", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            }else{
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(ReprintActivity.this, "Cannot connect to the printer, Please try again!", Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }
                        }else{
                            btPrinter.PrintLine(data,'M');
                        }
                    }
                }).start();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(ReprintActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }

    /**
     *
     * Background task for fetching the last transaction done by user
     */

    private class Reprint extends AsyncTask<String,Void,JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ReprintActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                url = new URL(Constants.USER_SUBS_TRANSACTION_DET+"Username="+localStore.getUserDetails().getUserName()+"&Token="+localStore.getUserDetails().getToken());
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                final JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "0":
                        ReprintActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    lcoName = obj.getString("OPERNAME");
                                    lcoPhone1 = obj.getString("OPERCONTACT");
                                    try{
                                        lcoPhone2 = obj.getString("OPERALTCONTACT");

                                    }catch (Exception ex){

                                        lcoPhone2 = "";
                                    }
                                    lcoAddress = obj.getString("OPERADDRESS");
                                    bankName = obj.getString("BANKNAME");
                                    chequeNumber = obj.getString("CHQNO");
                                    payDate = obj.getString("PAYDATE");
                                    payMode = obj.getString("PAYMODE");
                                    customerNameTV.setText(obj.getString("SUBSNAME"));
                                    addressTV.setText(obj.getString("SUBSADDRESS"));
                                    mobileNumberTV.setText(obj.getString("MOBILENO"));
                                    dueDateTV.setText(obj.getString("DueDate"));
                                    subscriberCodeTV.setText(obj.getString("SUBSNO"));
                                    subscriberIdTV.setText(obj.getString("SUBSID"));
                                    noofTV.setText(obj.getString("NoOfTVs"));
                                    arrearsTV.setText(obj.getString("AREARS"));
                                    stbchargesTV.setText(obj.getString("ADDITIONAL"));
                                    basicsTV.setText(obj.getString("BASIC"));
                                    sertaxTV.setText(obj.getString("SGST"));
                                    cgstTV.setText(obj.getString("CGST"));
                                    igstTV.setText(obj.getString("IGST"));
                                    enttaxTV.setText(obj.getString("ENTTAX"));
                                    totalTV.setText(obj.getString("TOTAL"));
                                    paidamtTV.setText(obj.getString("PAIDAMT"));
                                    balanceTV.setText(obj.getString("BALANCE"));
                                    addchargesTV.setText(obj.getString("OTHERCHARGES"));
                                    amtpaidTV.setText(obj.getString("PAYAMT"));
                                    remarksTV.setText(obj.getString("PAYREMARK"));
                                    receiptNoTV.setText(obj.getString("RECEIPTNO"));
                                    receiptNumber = obj.getString("RECEIPTNO");
                                    invoiceNoTV.setText(obj.getString("InvoiceNo"));
                                    invoiceNo = obj.getInt("InvoiceNo");
                                }catch (Exception ex){
                                    Log.e("Error", ex.toString());
                                }


                            }
                        });
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ReprintActivity.this, "Cannot fetch last transaction, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ReprintActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
            }finally {
                progressDialog.dismiss();
            }
        }
    }
}
