package in.cableguy.cableguy.ui;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.prowesspride.api.Printer_GEN;
import com.prowesspride.api.Setup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.btprinter.BTPrinter;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.GlobalApp;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.OfflineDBHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * @author Raj Mehra
 * @see LandingMenuActivity2
 * The type Nav drawer activity 2.
 */
public class NavDrawerActivity2 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    /**
     * The Billing ll.
     */
    LinearLayout billingLL, /**
     * The Print ll.
     */
    printLL, /**
     * The Last transaction ll.
     */
    lastTransactionLL, /**
     * The Terminal report ll.
     */
    terminalReportLL, /**
     * The Test print ll.
     */
    testPrintLL, /**
     * The Renew ll.
     */
    renewLL;
    /**
     * The Current balance tv.
     */
    TextView currentBalanceTV, /**
     * The Renew tv.
     */
    renewTV;
    /**
     * The Bt printer.
     */
    BTPrinter btPrinter;
    /**
     * The Fos name.
     */
    TextView fosName, /**
     * The Offline cases.
     */
    offlineCases, /**
     * The Lco name.
     */
    lcoName, /**
     * The Test print tv.
     */
    testPrintTV;
    /**
     * The Current balance tr.
     */
    TableRow currentBalanceTR;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Ret val.
     */
    int iRetVal;
    /**
     * The Result.
     */
    boolean result;
    /**
     * The Ptr gen.
     */
    public Printer_GEN ptrGen;
    /**
     * The Trans mode.
     */
    RadioGroup transMode;
    /**
     * The Pos.
     */
    int pos;
    /**
     * The Online rb.
     */
    RadioButton onlineRB, /**
     * The Offline rb.
     */
    offlineRB;
    /**
     * The Db helper.
     */
    OfflineDBHelper dbHelper;
    /**
     * The Count.
     */
    long count;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        overridePendingTransition(0, 0);
        getSupportActionBar().setTitle("Menu");
        mAdView = (AdView) findViewById(R.id.adView);
        getSupportActionBar().setTitle("Home");
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        printLL=(LinearLayout)findViewById(R.id.printLL);
        lastTransactionLL=(LinearLayout)findViewById(R.id.lastTransactionLL);
        terminalReportLL=(LinearLayout)findViewById(R.id.terminalReportLL);
        testPrintLL=(LinearLayout)findViewById(R.id.testPrintLL);
        testPrintTV = (TextView)findViewById(R.id.testPrintTV);
        currentBalanceTR = (TableRow)findViewById(R.id.currentBalanceTR);
        billingLL = (LinearLayout)findViewById(R.id.billingLL);
        currentBalanceTV = (TextView)findViewById(R.id.currentBalance);
        transMode = (RadioGroup)findViewById(R.id.transMode);
        renewTV = (TextView)findViewById(R.id.renewTV);
        localStore=new LocalStore(NavDrawerActivity2.this);
        Values.TRANS_MODE="Online";
        offlineCases = (TextView)findViewById(R.id.offlineCasesTV);
        dbHelper = new OfflineDBHelper(NavDrawerActivity2.this);
        renewLL = (LinearLayout)findViewById(R.id.renewLL);
        try{
            count = dbHelper.dataCount();
            Log.e("Count ",count + "");
        }catch (Exception ex){
            Log.e("Can't Count ", ex.toString());
        }
        offlineCases.setText("Pending Offline Cases   -->"+count);
        transMode = (RadioGroup)findViewById(R.id.transMode);
        onlineRB = (RadioButton)findViewById(R.id.onlineRB);
        offlineRB = (RadioButton)findViewById(R.id.offlineRB);
        renewLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Values.redirect="RENEW";
                Intent intent = new Intent(NavDrawerActivity2.this,CustomerListActivity.class);
                startActivity(intent);
            }
        });
        if (Values.ALLOW_OFFLINE.equalsIgnoreCase("Y")) {
            transMode.setVisibility(View.VISIBLE);
        }else{

            transMode.setVisibility(View.GONE);
        }
        transMode.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                pos=transMode.indexOfChild(findViewById(checkedId));
                switch(pos){
                    case 0:
                        Values.TRANS_MODE="Online";
                        break;
                    case 1:
                        Values.TRANS_MODE="Offline";
                        Intent intent = new Intent(NavDrawerActivity2.this, OfflineActivity.class);
                        startActivity(intent);
                        break;
                }
            }
        });
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
            if(btPrinter.isBTConnected){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(NavDrawerActivity2.this, "Printer Connected", Toast.LENGTH_SHORT).show();
                    }
                });
            }else{
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            btPrinter.OpenBTConnection();
                        }catch (Exception ex){
                            Log.e("Bluetooth Error", ex.toString());
                        }
                    }
                }).start();

            }
        }else{
            btPrinter=new BTPrinter(NavDrawerActivity2.this);
            if(btPrinter.isBTConnected){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(NavDrawerActivity2.this, "Printer Connected", Toast.LENGTH_SHORT).show();
                    }
                });
            }else{
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            btPrinter.OpenBTConnection();
                        }catch (Exception ex){
                            Log.e("Bluetooth Error", ex.toString());
                        }
                    }
                }).start();

            }
        }
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
            try {
                GlobalApp.setup = new Setup();
                result = GlobalApp.setup.blActivateLibrary(NavDrawerActivity2.this,R.raw.licencefull_pride_gen);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(result){
                Log.e("Library Activated ",result+"");
            }else{
                Log.e("Library failed", result+"");
            }
        }else{

        }
        billingLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Values.TRANS_MODE.equalsIgnoreCase("Online")){
                    Intent intent = new Intent(NavDrawerActivity2.this,NormalTypeActivity.class);
                    Values.redirect = "LCOType";
                    Values.lastTransaction = "";
                    Values.listType = "";
                    try{
                        Cursor c = dbHelper.getAllData();
                        if(count>0){
                            c.moveToFirst();

                            while(!c.isAfterLast()){
                                new uploadOfflineTask().execute(c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_SUBSNAME)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_SUBSCODE)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_AMOUNT)),
                                        c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_ADDICHARGES)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_MOBILE)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_REMARK)),
                                        c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_PAYMODE)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_BANKNAME)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_CHEQUEDATE)), c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_CHEQUENO)),c.getString(c.getColumnIndex(Constants.OFFLINE_COLUMN_USERNAME)));
                                c.moveToNext();
                            }

                            dbHelper.deleteAllData();


                        }

                    }catch (Exception ex){
                        Log.e("error ",ex.toString());
                    }
                    startActivity(intent);
                }else{
                    Intent intent = new Intent(NavDrawerActivity2.this,OfflineActivity.class);
                    startActivity(intent);
                }
            }
        });

        if(Values.CREDITLIMITFLAG.equalsIgnoreCase("Y")){
            currentBalanceTR.setVisibility(View.VISIBLE);
            currentBalanceTV.setText(Values.currentBalance);
        }
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){

        }else{
            btPrinter=new BTPrinter(NavDrawerActivity2.this);
        }
        testPrintLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String line="----------------------------";

                        String data = "       TEST PRINT"+"\n"+line+"\n"+"Powered By: www.cableguy.in"+"\n"+
                                "v "+Values.currentVersion+"\n"+"\n"+"\n"+"\n"+"\n";
                        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
                            if(result){
                                try {
                                    Log.e("Before ptrgen", "");
                                    ptrGen = new Printer_GEN(GlobalApp.setup, btPrinter.evoluteOutputStream, btPrinter.evoluteInputStream);
                                    ptrGen.iAddData((byte) 0x01,data);

                                    if(btPrinter.isBTConnected){
                                        iRetVal = ptrGen.iStartPrinting(1);
                                    }else{
                                        btPrinter.OpenBTConnection();
                                        iRetVal = ptrGen.iStartPrinting(1);
                                    }
                                } catch (Exception e) {
                                    Log.e("Printer Error", e.toString());
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(NavDrawerActivity2.this, "Printer Error", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            }else{
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(NavDrawerActivity2.this, "Cannot connect to the printer, Please try again!", Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }
                        }else{
                            btPrinter.PrintLine(data,'M');
                        }
                    }
                }).start();
            }
        });

        testPrintTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String line="----------------------------";

                        String data = "       TEST PRINT"+"\n"+line+"\n"+"Powered By: www.cableguy.in"+"\n"+
                                "v "+Values.currentVersion+"\n"+"\n"+"\n"+"\n"+"\n";
                        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
                            if(result){
                                try {
                                    Log.e("Before ptrgen", "");
                                    ptrGen = new Printer_GEN(GlobalApp.setup, btPrinter.evoluteOutputStream, btPrinter.evoluteInputStream);
                                    ptrGen.iAddData((byte) 0x01,data);
                                    if(btPrinter.isBTConnected){
                                        iRetVal = ptrGen.iStartPrinting(1);
                                    }else{
                                        btPrinter.OpenBTConnection();
                                        iRetVal = ptrGen.iStartPrinting(1);
                                    }

                                } catch (Exception e) {
                                    Log.e("Printer Error", e.toString());
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(NavDrawerActivity2.this, "Printer Error", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            }else{
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(NavDrawerActivity2.this, "Cannot connect to the printer, Please try again!", Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }
                        }else{
                            btPrinter.PrintLine(data,'M');
                        }
                    }
                }).start();
            }
        });

        printLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NavDrawerActivity2.this,ReprintActivity.class);
                startActivity(intent);
            }
        });

        lastTransactionLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Values.redirect="LCOSubsTransaction";
                Log.e("Lco Subs transaction",Values.redirect);/*
                Intent intent = new Intent(LandingMenuActivity2.this,LastTransactionActivity.class);*/
                Values.lastTransaction = "LCOSubsTransaction";
                Intent intent = new Intent(NavDrawerActivity2.this,LastTransactionActivity.class);
                startActivity(intent);
            }
        });

        terminalReportLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NavDrawerActivity2.this,ReportActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            finish();
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == R.id.logout){
            Intent intent = new Intent(NavDrawerActivity2.this, LogoutActivity.class);
            startActivity(intent);
        }else if(id == R.id.ecaf){
        }else if(id == R.id.changePass){
            Intent intent = new Intent(NavDrawerActivity2.this,ChangePasswordActivity.class);
            startActivity(intent);
        }else if(id == R.id.valueAdded){
            Toast.makeText(NavDrawerActivity2.this, "Coming Soon!", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            if(Values.redirect.equalsIgnoreCase("BuildingType")){
                Intent intent = new Intent(NavDrawerActivity2.this,NavDrawerActivity.class);
                startActivity(intent);
            }else if(Values.redirect.equalsIgnoreCase("LCOType")){
                Intent intent = new Intent(NavDrawerActivity2.this,NavDrawerActivity2.class);
                startActivity(intent);

            }else if(Values.redirect.equalsIgnoreCase("NormalType")){
                Intent intent = new Intent(NavDrawerActivity2.this,NavDrawerActivity3.class);
                startActivity(intent);
            }

        } else if (id == R.id.ecaf) {
/*            Intent intent = new Intent(NavDrawerActivity2.this, ECAFCustomerTypeActivity.class);
            startActivity(intent);*/

            Values.redirect="ECAFLCO";
            Intent intent = new Intent(NavDrawerActivity2.this, NormalTypeActivity.class);
            startActivity(intent);

        } else if (id == R.id.valueAdded) {

        } else if (id == R.id.changePass) {
            Intent intent = new Intent(NavDrawerActivity2.this, ChangePasswordActivity.class);
            startActivity(intent);

        } else if (id == R.id.logout) {
            Intent intent = new Intent(NavDrawerActivity2.this,LogoutActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_share) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    /**
     * Background task for uploading offline task automatically
     */
    private class uploadOfflineTask extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();


        /**
         * The Username off.
         */
        String usernameOff, /**
         * The Subs name.
         */
        subsName, /**
         * The Subs code.
         */
        subsCode, /**
         * The Paid amount.
         */
        paidAmount, /**
         * The Add charges.
         */
        addCharges, /**
         * The Mobile.
         */
        mobile, /**
         * The Remark.
         */
        remark, /**
         * The Pay mode.
         */
        payMode, /**
         * The Bank name.
         */
        bankName, /**
         * The Cheque date.
         */
        chequeDate, /**
         * The Cheque no.
         */
        chequeNo;

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            usernameOff = params[10];
            subsName = params[0];
            subsCode = params[1];
            paidAmount = params[2];
            addCharges = params[3];
            mobile = params[4];
            remark = params[5];
            payMode = params[6];
            bankName = params[7];
            chequeDate = params[8];
            chequeNo = params[9];
            try {
                String uri = Constants.OFFLINE_API + "Username=" + usernameOff + "&SubsName=" + subsName + "&SubsCode=" + subsCode + "&PayMode=" + payMode
                        + "&Amount=" + paidAmount + "&AddiCharges=" + addCharges + "&Mobile=" + mobile + "&Remark=" + remark
                        + "&ChqDt=" + chequeDate + "&ChqNo=" + chequeNo + "&BankName=" + bankName;

                url = new URL(uri.replaceAll(" ","%20"));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            String status;
            JSONArray dataArray;
            try {
                status = json.getString("STATUS");
                dataArray = json.getJSONArray("Data");
                switch (status) {
                    case "0" :
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                offlineCases.setText("Pending Offline Cases   -->"+count);
                            }
                        });
                        Toast.makeText(NavDrawerActivity2.this, "All pending offline task has been uploaded, Thank you", Toast.LENGTH_SHORT).show();
                        break;
                    case "1":
                        Toast.makeText(NavDrawerActivity2.this, "Some error occurred uploading offline data, please login again or try later", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(NavDrawerActivity2.this, "No Data found, Offline Tasks Already uploaded", Toast.LENGTH_SHORT).show();
                        break;
                }
            } catch (Exception ex) {
            }
        }
    }
}
