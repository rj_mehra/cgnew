package in.cableguy.cableguy.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import in.cableguy.cableguy.R;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 *
 * @author Raj Mehra
 * The type Wallet list activity.
 *
 * @deprecated No longer in use, kept for future use in case multiple wallets are integrated
 */
public class WalletListActivity extends AppCompatActivity {

    /**
     * The Paytm.
     */
    Button paytm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_list);
        overridePendingTransition(0, 0);
        paytm = (Button)findViewById(R.id.paytm);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
