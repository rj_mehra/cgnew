package in.cableguy.cableguy.ui;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import in.cableguy.cableguy.R;

/**
 * @author Raj Mehra
 * The type Contact activity.
 */
public class ContactActivity extends AppCompatActivity {
    /**
     * The variables listed below are for phone numbers listed on the UI, please refer the UI for respective variable names
     */
    /**
     * The Call 1.
     */
    TextView call1, /**
     * The Call 2.
     */
    call2, /**
     * The Call 3.
     */
    call3, /**
     * The Call 4.
     */
    call4, /**
     * The Call 5.
     */
    call5, /**
     * The Email 1.
     */
    email1, /**
     * The Email 2.
     */
    email2;
    final private static int MY_PERMISSIONS_CALL_PHONE = 500;
    /**
     * The Whatsapp 1 number.
     */
    TextView whatsApp1, /**
     * The Whatsapp 2 number.
     */
    whatsApp2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        call1 = (TextView)findViewById(R.id.call1);
        call2 = (TextView)findViewById(R.id.call2);
        call3 = (TextView)findViewById(R.id.call3);
        call4 = (TextView)findViewById(R.id.call4);
        call5 = (TextView)findViewById(R.id.call5);
        email1 = (TextView)findViewById(R.id.email1);
        email2 = (TextView)findViewById(R.id.email2);
        whatsApp1 = (TextView)findViewById(R.id.whatsApp1);
        whatsApp2 = (TextView)findViewById(R.id.whatsApp2);

        whatsApp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openWhatsApp("919702499222");
            }
        });
        whatsApp2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openWhatsApp("919702797021");
            }
        });
        call1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "9702499222"));
                int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ContactActivity.this,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ContactActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_CALL_PHONE);
                    } else {
                        startActivity(intent);

                    }
                }
            }
        });
        call2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "9702797021"));
                int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ContactActivity.this,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ContactActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_CALL_PHONE);
                    } else {
                        startActivity(intent);

                    }
                }
            }
        });
        call3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "9971604330"));
                int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ContactActivity.this,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ContactActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_CALL_PHONE);
                    } else {
                        startActivity(intent);

                    }
                }
            }
        });
        call4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "9015450916"));
                int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ContactActivity.this,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ContactActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_CALL_PHONE);
                    } else {
                        startActivity(intent);

                    }
                }
            }
        });
        call5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "8689923456"));
                int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ContactActivity.this,
                            Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ContactActivity.this,
                                new String[]{Manifest.permission.CALL_PHONE},
                                MY_PERMISSIONS_CALL_PHONE);
                    } else {
                        startActivity(intent);

                    }
                }
            }
        });

        email1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "sales@cableguy.in" });
                intent.putExtra(Intent.EXTRA_SUBJECT, "Help App not working");
                /*intent.putExtra(Intent.EXTRA_TEXT, "mail body");*/
                startActivity(Intent.createChooser(intent, ""));
            }
        });

        email2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { "support@cableguy.in" });
                intent.putExtra(Intent.EXTRA_SUBJECT, "Help App not working");
                /*intent.putExtra(Intent.EXTRA_TEXT, "mail body");*/
                startActivity(Intent.createChooser(intent, ""));
            }
        });
    }

    /**
     * @param number
     * Method to open whatsapp if it is installed on user's phone
     *
     * If it is not installed the app will open play store for the user to install.
     */
    private void openWhatsApp(String number) {
        boolean isWhatsappInstalled = whatsappInstalledOrNot("com.whatsapp");
        if (isWhatsappInstalled) {

            Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(number) + "@s.whatsapp.net");//phone number without "+" prefix

            startActivity(sendIntent);
        } else {
            Uri uri = Uri.parse("market://details?id=com.whatsapp");
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            Toast.makeText(this, "WhatsApp not Installed",
                    Toast.LENGTH_SHORT).show();
            startActivity(goToMarket);
        }
    }

    /**
     * This method checks whether whatsapp is installed in the phone or not.
     * @param uri
     * @return URI for the Play store
     *
     */
    private boolean whatsappInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

}
