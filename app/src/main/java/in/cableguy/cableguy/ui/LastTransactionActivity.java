package in.cableguy.cableguy.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Values;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 *
 * @author Raj Mehra
 *
 * The type Last transaction activity.
 *
 * This activity lists the last transaction activity for both customer and the user i.e. FOS
 */
public class LastTransactionActivity extends AppCompatActivity {

    /**
     * The Subs last transaction button.
     */
    Button subsLastTransactionButton, /**
     * The User last transaction button.
     */
    userLastTransactionButton;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_transaction);
        overridePendingTransition(0, 0);
        subsLastTransactionButton=(Button)findViewById(R.id.subsLastTransactionButton);
        userLastTransactionButton=(Button)findViewById(R.id.userLastTransactionButton);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        getSupportActionBar().setTitle("Last Transaction");

        /**
         * Subscriber Last transaction button which displays list of subscribers to select and consequently listing their transactions
         */
        subsLastTransactionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Values.onCustomerSelect="subslasttxn";
                Values.selectedCustomerName="Select Customer";
                Values.selectedZone="Select Zone";
                Values.selectedArea="Select Area";
                Values.selectedBuilding=null;
                if(Values.redirect.equalsIgnoreCase("NormalSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("NormalSubsTransaction")){
                    Values.listType="subs";
                    Log.e("Normal Subs transaction", Values.redirect);
                    Intent i=new Intent(LastTransactionActivity.this,NormalSearchActivity.class);
                    startActivity(i);
                    Values.onCustomerSelect="";
                }else if(Values.redirect.equalsIgnoreCase("LCOSubsTransaction") || Values.lastTransaction.equalsIgnoreCase("LCOSubsTransaction")){
                    Log.e("LCO Subs transaction",Values.redirect);
                    Values.listType="subs";
/*                    Intent i=new Intent(LastTransactionActivity.this,SearchActivity.class);*/
                    Intent i=new Intent(LastTransactionActivity.this,NormalTypeActivity.class);
                    startActivity(i);
                    Values.onCustomerSelect="";
                }
                else if(Values.redirect.equalsIgnoreCase("BuildingSubsTransaction") || Values.lastTransaction.equalsIgnoreCase("BuildingSubsTransaction")){
                    Log.e("Building transaction",Values.redirect);
                    Values.listType="subs";
                    Intent i=new Intent(LastTransactionActivity.this,CustomerDetailsActivity.class);
                    startActivity(i);
                    Values.onCustomerSelect="";
                }
            }
        });

        /**
         * User Last transaction button lists transactions for the user i.e. FOS
         *
         *
         */
        userLastTransactionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Values.listType="user";
                Intent i=new Intent(LastTransactionActivity.this,LastTransactionListActivity.class);
                startActivity(i);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(LastTransactionActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
