package in.cableguy.cableguy.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Values;

/**
 *
 * @author Raj Mehra
 * The type Offline success activity.
 *
 *
 */
public class OfflineSuccessActivity extends AppCompatActivity {
    /**
     * The Next button.
     */
    Button nextButton;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_success);
        nextButton = (Button)findViewById(R.id.nextButton);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OfflineSuccessActivity.this, OfflineActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        redirect();
    }

    /**
     * Redirect.
     */
    public void redirect(){
        if(Values.redirect.equalsIgnoreCase("BuildingSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(OfflineSuccessActivity.this, NavDrawerActivity.class);
            Values.redirect="";
            Values.listType="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("NormalSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("NormalSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(OfflineSuccessActivity.this, NavDrawerActivity3.class);
            Values.redirect="";
            Values.listType="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("LCOSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("LCOSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(OfflineSuccessActivity.this, NavDrawerActivity2.class);
            Values.redirect="";
            Values.listType="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("BuildingType")){
            Intent intent = new Intent(OfflineSuccessActivity.this, NavDrawerActivity.class);
            Values.redirect="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("NormalType")){
            Intent intent = new Intent(OfflineSuccessActivity.this, NavDrawerActivity3.class);
            Values.redirect="";
            startActivity(intent);
        }else if ((Values.redirect.equalsIgnoreCase("LCOType"))){
            Intent intent = new Intent(OfflineSuccessActivity.this, NavDrawerActivity2.class);
            Values.redirect="";
            startActivity(intent);
        }


    }
}
