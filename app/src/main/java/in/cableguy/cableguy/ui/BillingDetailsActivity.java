package in.cableguy.cableguy.ui;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.billdesk.sdk.PaymentOptions;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.firebase.crash.FirebaseCrash;
import com.prowesspride.api.Printer_GEN;
import com.prowesspride.api.Setup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.btprinter.BTPrinter;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.ConvertToISOString;
import in.cableguy.cableguy.helper.Customer;
import in.cableguy.cableguy.helper.GlobalApp;
import in.cableguy.cableguy.helper.HMACEncoder;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.MessageHelper;
import in.cableguy.cableguy.pg.CallBack;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static in.cableguy.cableguy.R.id.mobileNumber;
import static in.cableguy.cableguy.constants.Values.customerIdSelected;
import static in.cableguy.cableguy.constants.Values.customerNumber;

/**
 * The type Billing details activity.
 * @author Raj Mehra
 *
 * Irrespective of the user type i.e. Building, Normal or LCO
 *
 * This activity is shown for collection, this is done to avoid redundancy which would have occurred by creating separate files for every type of user billing.
 */
public class BillingDetailsActivity extends AppCompatActivity {
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static int MY_PERMISSIONS_CALL_PHONE = 100;
    private static final int REQUEST_ENABLE_BT = 3;
    private BluetoothAdapter mBluetoothAdapter = null;
    /**
     * The Retrieve variable to get data from previous activity.
     */
    Bundle retrieve;
    /**
     * The Customer name.
     */
    String customerName, /**
     * The Building name.
     */
    buildingName, /**
     * The City name.
     */
    cityName;
    /**
     * The Customer model.
     */
    Customer customer;
    /**
     * The Submit button.
     */
    Button submit;
    /**
     * The Ret value for evolute printer.
     */
    int iRetVal;
    /**
     * The Ptr gen.
     */
    public  Printer_GEN ptrGen;
    /**
     * The Evresult.
     */
    boolean evresult;
    /**
     * The Cash button.
     */
    RadioButton cash, /**
     * The Cheque button.
     */
    cheque, /**
     * The Online button.
     */
    online;
    /**
     * The On cheque selection.
     */
    TableRow onChequeSelection;
    /**
     * The Payment type.
     */
    RadioGroup paymentType;
    /**
     * The Pos.
     */
    int pos, /**
     * The Length.
     */
    length;
    /**
     * The Customer name tv.
     */
    TextView customerNameTV, /**
     * The Arrears tv.
     */
    arrearsTV, /**
     * The Address tv.
     */
    addressTV, /**
     * The Basics tv.
     */
    basicsTV, /**
     * The Stb charges tv.
     */
    stbChargesTV, /**
     * The Ser tax tv.
     */
    serTaxTV, /**
     * The Ent tax tv.
     */
    entTaxTV, /**
     * The Total tv.
     */
    totalTV, /**
     * The Paid amt tv.
     */
    paidAmtTV, /**
     * The Balance tv.
     */
    balanceTV, /**
     * The Mobile number tv.
     */
    mobileNumberTV,
    /**
     * The Cgst tv.
     */
    cgstTV, /**
     * The Igst tv.
     */
    igstTV;
    /**
     * The Amtpaid ed.
     */
    EditText amtpaidED, /**
     * The Remarks ed.
     */
    remarksED, /**
     * The Bank name ed.
     */
    bankNameED, /**
     * The Cheque no ed.
     */
    chequeNoED, /**
     * The Addcharges ed.
     */
    addchargesED, /**
     * The Mobile number ed.
     */
    mobileNumberED;
    /**
     * The Add channel.
     */
    Button addChannel, /**
     * The Add bouquet.
     */
    addBouquet, /**
     * The Plan change.
     */
    planChange;
    /**
     * The Bluetooth device list.
     */
    List<BluetoothDevice> bluetoothDeviceList=new ArrayList<>();
    /**
     * The Message.
     */
    MessageHelper message;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    /**
     * The Bt printer.
     */
    BTPrinter btPrinter;
    /**
     * The Pay mode.
     */
    String payMode="Cash";
    /**
     * The Phone 1.
     */
    String phone1, /**
     * The Phone 2.
     */
    phone2, /**
     * The Call.
     */
    call;
    /**
     * The Edit button.
     */
    Button editButton, /**
     * The Cancel button.
     */
    cancelButton, /**
     * The Save button.
     */
    saveButton;
    /**
     * The Alt number layout.
     */
    LinearLayout altNumberLayout;
    /**
     * The Due date.
     */
    TextView dueDate;
    /**
     * The Alt mobile number.
     */
    EditText altMobileNumber;
    /**
     * The Current date time string.
     */
    String currentDateTimeString;
    /**
     * The Transaction time.
     */
    String transactionTime, /**
     * The Stbno.
     */
    stbno, /**
     * The Subscode.
     */
    subscode, /**
     * The Outstanding.
     */
    outstanding;
    /**
     * The Reload.
     */
    ImageView reload;
    /**
     * The Payonline.
     */
    Button payonline;
    /**
     * The Noof tv.
     */
    TextView noofTV, /**
     * The Subscriber code tv.
     */
    subscriberCodeTV, /**
     * The Subscriber id tv.
     */
    subscriberIdTV;
    /**
     * The Pay now button.
     */
    Button payNowButton, /**
     * The Slc button.
     */
    slcButton, /**
     * The Slc submit.
     */
    slcSubmit;
    /**
     * The Mode tl.
     */
    TableLayout modeTL, /**
     * The Amount tl.
     */
    amountTL;
    /**
     * The Footer.
     */
    RelativeLayout footer;
    /**
     * The Slc list sp.
     */
    Spinner slcListSP;
    /**
     * The Bank.
     */
    String bank;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_details);
        overridePendingTransition(0, 0);
        getSupportActionBar().setTitle("Customer Details");
        retrieve = getIntent().getExtras();
        customer = Values.selectedCustomer;
        submit = (Button) findViewById(R.id.submit);
        cash = (RadioButton) findViewById(R.id.cash);
        cheque = (RadioButton) findViewById(R.id.cheque);
        paymentType=(RadioGroup)findViewById(R.id.paymenttype);
        onChequeSelection = (TableRow) findViewById(R.id.onchequeselection);
        customerNameTV=(TextView) findViewById(R.id.customerNameTV);
        addchargesED=(EditText)findViewById(R.id.addchargesED);
        amtpaidED=(EditText)findViewById(R.id.amtpaidED);
        bankNameED=(EditText)findViewById(R.id.banknameED);
        chequeNoED=(EditText)findViewById(R.id.chequenoED);
        remarksED=(EditText)findViewById(R.id.remarksED);
        arrearsTV=(TextView) findViewById(R.id.arrearsTV);
        addressTV=(TextView) findViewById(R.id.addressTV);
        stbChargesTV=(TextView)findViewById(R.id.stbchargesTV);
        basicsTV=(TextView) findViewById(R.id.basicsTV);
        serTaxTV=(TextView) findViewById(R.id.sertaxTV);
        entTaxTV=(TextView) findViewById(R.id.enttaxTV);
        totalTV=(TextView) findViewById(R.id.totalTV);
        paidAmtTV=(TextView) findViewById(R.id.paidamtTV);
        balanceTV=(TextView) findViewById(R.id.balanceTV);
        mobileNumberED = (EditText) findViewById(mobileNumber);
        igstTV = (TextView)findViewById(R.id.igstTV);
        onChequeSelection.setVisibility(View.GONE);
        message=new MessageHelper(BillingDetailsActivity.this);
        localStore=new LocalStore(BillingDetailsActivity.this);
        progressDialog=new ProgressDialog(BillingDetailsActivity.this);
        btPrinter=new BTPrinter(BillingDetailsActivity.this);
        editButton = (Button)findViewById(R.id.updateDetails);
        cancelButton = (Button)findViewById(R.id.cancel);
        dueDate = (TextView)findViewById(R.id.dueDate);
        saveButton = (Button)findViewById(R.id.save);
        payonline = (Button)findViewById(R.id.payonline);
        altNumberLayout = (LinearLayout)findViewById(R.id.altNumberLayout);
        altMobileNumber = (EditText)findViewById(R.id.altMobileNumber);
        mobileNumberED.setInputType(InputType.TYPE_NULL);
        noofTV = (TextView)findViewById(R.id.noofTV);
        subscriberCodeTV = (TextView)findViewById(R.id.subscriberCode);
        subscriberIdTV = (TextView)findViewById(R.id.subscriberId);
        bank = bankNameED.getText().toString();
        cgstTV = (TextView)findViewById(R.id.cgstTV);
        igstTV= (TextView)findViewById(R.id.igstTV);
        //for SLC
        if(btPrinter.isBTConnected){

        }else{
            new Thread(new Runnable() {
                @Override
                public void run() {
                    btPrinter.OpenBTConnection();
                }
            }).start();

        }
        payNowButton = (Button)findViewById(R.id.payNowButton);
        slcButton = (Button)findViewById(R.id.slcButton);
        modeTL = (TableLayout)findViewById(R.id.modeTL);
        footer = (RelativeLayout)findViewById(R.id.footer);
        amountTL = (TableLayout)findViewById(R.id.amountTL);
        slcListSP = (Spinner)findViewById(R.id.slcListSP);
        slcSubmit = (Button)findViewById(R.id.slcSubmit);
        payNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Values.ORDER_AMOUNT = amtpaidED.getText().toString();
                if(modeTL.getVisibility()==View.GONE||footer.getVisibility()==View.GONE||amountTL.getVisibility()==View.GONE){
                    modeTL.setVisibility(View.VISIBLE);
                    footer.setVisibility(View.VISIBLE);
                    amountTL.setVisibility(View.VISIBLE);
                    remarksED.setVisibility(View.VISIBLE);
                }else{
                    modeTL.setVisibility(View.GONE);
                    footer.setVisibility(View.GONE);
                    amountTL.setVisibility(View.GONE);
                    remarksED.setVisibility(View.GONE);
                }

                if(slcListSP.getVisibility()==View.VISIBLE || slcSubmit.getVisibility()==View.VISIBLE){
                    slcListSP.setVisibility(View.GONE);
                    slcSubmit.setVisibility(View.GONE);
                }
            }
        });
        slcButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(slcListSP.getVisibility()==View.GONE ||slcSubmit.getVisibility()==View.GONE || remarksED.getVisibility()==View.GONE ){
                    slcListSP.setVisibility(View.VISIBLE);
                    slcSubmit.setVisibility(View.VISIBLE);
                    remarksED.setVisibility(View.VISIBLE);
                    Values.SLC_ENABLED = true;
                }else{
                    slcListSP.setVisibility(View.GONE);
                    slcSubmit.setVisibility(View.GONE);
                    remarksED.setVisibility(View.GONE);
                    Values.SLC_ENABLED = false;
                }
                if(modeTL.getVisibility()==View.GONE||footer.getVisibility()==View.GONE||amountTL.getVisibility()==View.GONE){

                }else{
                    modeTL.setVisibility(View.GONE);
                    footer.setVisibility(View.GONE);
                    amountTL.setVisibility(View.GONE);
                }
            }
        });
        slcSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Values.SLC_OPTION = slcListSP.getSelectedItem().toString();
                    new SubmitSLC().execute();
            }
        });


        //end of SLC specific actions
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){

        }else{
            btPrinter=new BTPrinter(BillingDetailsActivity.this);
        }

        payonline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //payMode="Online";
                onChequeSelection.setVisibility(View.GONE);
                if(Values.ISONLINEPAY.equalsIgnoreCase("N")){

                    final AlertDialog.Builder errorAlert = new AlertDialog.Builder(BillingDetailsActivity.this);
                    errorAlert.setMessage("You do not have Payment Gateway Service Enabled, Please contact "+localStore.getUserDetails().getLcoName()+" !");
                    errorAlert.setCancelable(true);

                    errorAlert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });
                    AlertDialog errorAlertDialog = errorAlert.create();
                    errorAlertDialog.show();
                }else{
                    Values.ORDER_AMOUNT = amtpaidED.getText().toString();
                    if(amtpaidED.getText().toString().equalsIgnoreCase("")){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(BillingDetailsActivity.this, "Please enter some amount to continue", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }else{
                        new OnlinePayment().execute();
                    }

                }

            }
        });



        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelButton.setVisibility(View.VISIBLE);
                altNumberLayout.setVisibility(View.VISIBLE);
                dueDate.setClickable(true);
                saveButton.setVisibility(View.VISIBLE);
                editButton.setVisibility(View.GONE);
                mobileNumberED.setInputType(InputType.TYPE_CLASS_NUMBER);
                mobileNumberED.requestFocus();
                dueDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pickDate(view);
                    }
                });


                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cancelButton.setVisibility(View.GONE);
                        altNumberLayout.setVisibility(View.GONE);
                        dueDate.setClickable(false);
                        saveButton.setVisibility(View.GONE);
                        editButton.setVisibility(View.VISIBLE);
                        mobileNumberED.setInputType(InputType.TYPE_NULL);


                    }
                });

                saveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cancelButton.setVisibility(View.GONE);
                        altNumberLayout.setVisibility(View.GONE);
                        dueDate.setClickable(false);
                        saveButton.setVisibility(View.GONE);
                        editButton.setVisibility(View.VISIBLE);
                        mobileNumberED.setInputType(InputType.TYPE_NULL);
                        new SubscriberDetailsModify().execute(dueDate.getText().toString(), mobileNumberED.getText().toString(), altMobileNumber.getText().toString());

                    }
                });
            }
        });


        currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
            try {
                GlobalApp.setup = new Setup();
                evresult = GlobalApp.setup.blActivateLibrary(BillingDetailsActivity.this,R.raw.licencefull_pride_gen);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(evresult){
                Log.e("Library Activated ",evresult+"");
            }else{
                Log.e("Library failed", evresult+"");
            }
        }else{

        }
        phone1 = localStore.getUserDetails().getPhone1();
        phone2 = localStore.getUserDetails().getPhone2();
        try{
            customerNameTV.setText(customer.getName());
        }
        catch (Exception ex){
            Log.e("CustomerNameTV" , "NULL " +ex.toString());
        }

        paymentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                pos=paymentType.indexOfChild(findViewById(checkedId));
                switch(pos){
                    case 0:
                        payMode="Cash";
                        onChequeSelection.setVisibility(View.GONE);
                        break;
                    case 1:
                        payMode="Cheque";
                        onChequeSelection.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });
        paymentType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(BillingDetailsActivity.this, paymentType.getCheckedRadioButtonId() + "", Toast.LENGTH_SHORT).show();
            }
        });
        submit.setOnClickListener(submitListener);
        planChange = (Button)findViewById(R.id.changePlan);
        planChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BillingDetailsActivity.this, PlanActivity.class);
                startActivity(intent);
            }
        });
    }
    private void generatePAYLOAD(String amount){

        //CATV|292373|NA|240.0|NA|NA|NA|INR|NA|R|catv|NA|NA|F|0201200001|raj@cableguy.in|NA|NA|NA|NA|NA|http://35.154.198.134/admin/getresponse.aspx|aa812e681190ec38295ad64f8a35632644dc418d77d46c1afde3fd0b412d3a63

        String payonline = "CATV|"+ Values.customerIdSelected+"-"+Values.ORDER_ID+"|NA|"+amount+"|NA|NA|NA|INR|NA|R|catv|NA|NA|F|"+Values.selectedCustomerNumber+"|"+"NA"+
                "|NA|"+Values.MERCHANTID+"|NA|NA|NA|"+Values.RETURN_URL;
        try {
/*           Values.PAY_LOAD = payonline+"|"+"CP1005!"+HMACEncoder.HmacSHA256(payonline,Values.CHECKSUM_KEY)+"!NA!NA!NA";*/

            CallBack callbackObj = new CallBack();
            Values.PAY_LOAD = payonline+"|"+HMACEncoder.HmacSHA256(payonline,Values.CHECKSUM_KEY);
            Intent intent = new Intent(BillingDetailsActivity.this,PaymentOptions.class);
            intent.putExtra("msg",Values.PAY_LOAD);
            Log.e("Values ", Values.PAY_LOAD);
            intent.putExtra("user-email","");
            intent.putExtra("user-mobile",mobileNumberED.getText().toString());
            intent.putExtra("orientation", Configuration.ORIENTATION_PORTRAIT);
            intent.putExtra("callback",callbackObj);

            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(BillingDetailsActivity.this, "Some error occured", Toast.LENGTH_SHORT).show();
                }
            });

        }
    }

    /**
     *  Background task for modifying subscriber details
     */
    private class SubscriberDetailsModify extends AsyncTask<String, Void, JSONObject>{
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog = new ProgressDialog(BillingDetailsActivity.this);
                    progressDialog.setCancelable(true);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();
                }
            });
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            final URL url;
            String dueDate = params[0];
            String cp1 = params[1];
            String cp2 = params[2];

            try{
                String uri = Constants.SUBSCRIBER_DETAILS_MODIFY+"username="+localStore.getUserDetails().getUserName()+"&Subsid="+Values.customerIdSelected+
                        "&Duedate="+dueDate+"&Contact1="+cp1+"&Contact2="+cp2+"&Token="+localStore.getUserDetails().getToken();
                url = new URL(uri.replaceAll(" ","%20"));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(BillingDetailsActivity.this, "Successfully updated", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(BillingDetailsActivity.this, "Failed to update", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(BillingDetailsActivity.this, "Unable to connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Modify Exception ",ex.toString());
            }finally {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        redirect();
    }

    /**
     * Redirect. to respective activity based on the transaction or screen
     */
    public void redirect(){
        if(Values.redirect.equalsIgnoreCase("BuildingSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(BillingDetailsActivity.this, CustomerDetailsActivity.class);
            Values.redirect="BuildingType";
            Values.listType="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("NormalSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("NormalSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(BillingDetailsActivity.this, NavDrawerActivity3.class);
            Values.redirect="";
            Values.listType="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("LCOSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("LCOSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(BillingDetailsActivity.this, NavDrawerActivity2.class);
            Values.redirect="";
            Values.listType="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("BuildingType")){
            Intent intent = new Intent(BillingDetailsActivity.this, CustomerDetailsActivity.class);
            Values.redirect="BuildingType";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("NormalType")){
            Intent intent = new Intent(BillingDetailsActivity.this, NavDrawerActivity3.class);
            Values.redirect="";
            startActivity(intent);
        }else if ((Values.redirect.equalsIgnoreCase("LCOType"))){
            Intent intent = new Intent(BillingDetailsActivity.this, NavDrawerActivity2.class);
            Values.redirect="";
            startActivity(intent);
        }


    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * Generate print string string.
     *
     * @param value the value for printing
     * @return the string that needs to printed which is encoded and decoded by the printer
     */
    public String generatePrintString(String value){
        if(value.length()<=length){
            for(;value.length()==length;){
                value+=" ";
            }
        }
        else{
            String s;
            s=value.substring(0,length);
            String s1=value.substring(length);
            if(s1.length()<length){
                for(;s1.length()==length;){
                    s1+=" ";
                }
            }
            value=s+s1;
        }
        return value;
    }

    /**
     * The Submit listener.
     */
    View.OnClickListener submitListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String bankName=bankNameED.getText().toString();
            String chequeNo=chequeNoED.getText().toString();
            LayoutInflater inflater = getLayoutInflater();
            View alertLayout  = inflater.inflate(R.layout.confirmation_dialog,null);
            AlertDialog.Builder alertDialog =new AlertDialog.Builder(BillingDetailsActivity.this);
            alertDialog.setTitle("Confirm Payment");
            alertDialog.setView(alertLayout);
            alertDialog.setCancelable(false);
            mAdView = (AdView) alertLayout.findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
            final TextView alertCustomerID = (TextView)alertLayout.findViewById(R.id.alertCustomerIDTV);
            final TextView alertCustomerName = (TextView)alertLayout.findViewById(R.id.alertSubscriberNameTV);
            final TextView alertAmount = (TextView)alertLayout.findViewById(R.id.alertChargeAmtTV);
            final TextView alertPhoneNumber = (TextView)alertLayout.findViewById(R.id.alertMobilenoTV);
            final TextView alertPayType = (TextView)alertLayout.findViewById(R.id.alertPayTypeTV);

            alertCustomerID.setText(subscriberCodeTV.getText().toString());
            alertCustomerName.setText(customerNameTV.getText().toString());
            alertAmount.setText("Rs. "+amtpaidED.getText().toString());
            alertPhoneNumber.setText(mobileNumberED.getText().toString());
            alertPayType.setText(payMode);
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            if(amtpaidED.getText().toString().equals("")||Double.parseDouble(amtpaidED.getText().toString())==0.0){
                message.displayShort("Kindly, fill in some amount");
            }
            else if(onChequeSelection.getVisibility()==View.VISIBLE && (bankName.equals("")||chequeNo.equals(""))){
                message.displayShort("Kindly, fill in proper bank details");
            }
            else {
                ConnectivityManager cm = (ConnectivityManager) BillingDetailsActivity.this.
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                    //Connectivity
                    alertDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                                new InsertPayment().execute(
                                        amtpaidED.getText().toString(),remarksED.getText().toString(),addchargesED.getText().toString(),
                                        bankNameED.getText().toString(),chequeNoED.getText().toString());


                        }
                    });

                    AlertDialog dialog = alertDialog.create();
                    dialog.show();
                } else {
                    message.displayShort("No internet connectivity");

                }
            }

        }
    };
    /**
     * The Selected date.
     */
    String selectedDate;

    /**
     * Pick date method for showing calendar to select date.
     *
     * @param view the view
     */
    public void pickDate(View view) {
        final Calendar c1 = Calendar.getInstance();
        DatePickerDialog datedialog = new DatePickerDialog(BillingDetailsActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(
                            final DatePicker view,
                            int year,
                            int monthOfYear,
                            int dayOfMonth) {
                        final String date1 = year + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + String.format("%02d", dayOfMonth);
                        AlertDialog.Builder dialog = new AlertDialog.Builder(BillingDetailsActivity.this);
                        dialog.setTitle("Date Selection");
                        dialog.setMessage("Do you want to select " + date1 + " ?");
                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                            }
                        });
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                selectedDate = date1;
                                final DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                Date d = new Date();
                                try {
                                    d = format.parse(date1);
                                } catch (Exception e) {
                                }
                                selectedDate = ConvertToISOString.getISO8601StringForDate(d);
                           BillingDetailsActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        dueDate.setText(date1);
                                    }
                                });
                            }
                        });
                        dialog.show();
                    }

                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DATE));
        datedialog.getDatePicker().setMinDate(c1.getTimeInMillis());
        datedialog.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        ConnectivityManager cm = (ConnectivityManager) BillingDetailsActivity.this.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            //Connectivity
            new FetchCustomerData().execute();
        }
        else{
            //No connectivity
            message.displayShort("No internet connectivity");

        }

    }

    /**
     *  Background task to fetch customer data by calling respective API
     */

    private class FetchCustomerData extends AsyncTask<String, Void, JSONObject>{
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog = new ProgressDialog(BillingDetailsActivity.this);
                    progressDialog.setCancelable(true);
                    progressDialog.setMessage("Loading...");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();
                }
            });
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            String fetch_cust_uri;
            try{
                URL url = null;
                /*fetch_cust_uri =Constants.SUBS_SEARCH+"mode="+Values.searchType+"&Lcoid="+Values.selectedLcoId+
                        "&SubsName="+Values.selectedCustomerName+"&SubsNo="+Values.selectedCustomerNumber+"&MobNo="+
                        Values.cp1+"&CRDNO="+Values.cardNumber+"&STBNO="+Values.stbNumber+"&UserID="+localStore.getUserDetails().getUserName()+
                        "&Token="+localStore.getUserDetails().getToken();*/

                if(Values.searchType.equalsIgnoreCase("CC")){
                    fetch_cust_uri= Constants.SUBS_SEARCH+"mode=CC&Lcoid="+ Values.selectedLcoId+"&SubsName="+Values.selectedCustomerName+
                            "&SubsNo="+Values.selectedCustomerNumber +"&MobNo=&CRDNO=&STBNO=&UserID="+localStore.getUserDetails().getUserName()+
                            "&Token="+localStore.getUserDetails().getToken();
                    url = new URL(fetch_cust_uri.replaceAll(" ","%20"));


                }else if(Values.searchType.equalsIgnoreCase("CN")){
                    fetch_cust_uri= Constants.SUBS_SEARCH+"mode=CN&Lcoid="+ Values.selectedLcoId+"&SubsName="+Values.selectedCustomerName+
                            "&SubsNo="+Values.selectedCustomer.getNumber()+"&MobNo=&CRDNO=&STBNO=&UserID="+localStore.getUserDetails().getUserName()+
                            "&Token="+localStore.getUserDetails().getToken();
                    url = new URL(fetch_cust_uri.replaceAll(" ","%20"));

                }else if(Values.searchType.equalsIgnoreCase("CP1")){
                    if(Values.selectedCustomerNumber.length()<10){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(BillingDetailsActivity.this, "Mobile number cannot be less than 10 digits", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }else{
                        try {
                            fetch_cust_uri= Constants.SUBS_SEARCH+"mode=CP1&Lcoid="+ Values.selectedLcoId+"&SubsName=&SubsNo=&MobNo="
                                    +Values.selectedCustomerNumber +"&CRDNO=&STBNO=&UserID="+localStore.getUserDetails().getUserName()+
                                    "&Token="+localStore.getUserDetails().getToken();
                            url = new URL(fetch_cust_uri.replaceAll(" ","%20"));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }

                    }

                }else if(Values.searchType.equalsIgnoreCase("CRD")){
                    try {
                        fetch_cust_uri = Constants.SUBS_SEARCH+"mode=CRD&Lcoid="+ Values.selectedLcoId+"&SubsName=&SubsNo=&MobNo=&CRDNO="+Values.selectedCustomerNumber +
                                "&STBNO=&UserID="+localStore.getUserDetails().getUserName()+
                                "&Token="+localStore.getUserDetails().getToken();
                        url = new URL(fetch_cust_uri.replaceAll(" ","%20"));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                }else if(Values.searchType.equalsIgnoreCase("STB")){
                    try {
                        fetch_cust_uri = Constants.SUBS_SEARCH+"mode=STB&Lcoid="+ Values.selectedLcoId+"&SubsName=&SubsNo=&MobNo=&CRDNO=&STBNO="
                                +Values.selectedCustomerNumber +"&UserID="+localStore.getUserDetails().getUserName()+
                                "&Token="+localStore.getUserDetails().getToken();
                        url = new URL(fetch_cust_uri.replaceAll(" ","%20"));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);
                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                final JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "0":
                        try{
                            addressTV.setText(obj.getString("SUBSADDRESS"));
                            arrearsTV.setText(obj.getString("AREARS"));
                            balanceTV.setText(obj.getString("BALANCE"));
                            basicsTV.setText(obj.getString("BASIC"));
                            paidAmtTV.setText(obj.getString("PAIDAMT"));
                            Values.PREVIOUSLY_PAID_AMOUNT = obj.getString("PAIDAMT");
                            serTaxTV.setText(obj.getString("SGST"));
                            cgstTV.setText(obj.getString("CGST"));
                            igstTV.setText(obj.getString("IGST"));
                            //entTaxTV.setText(obj.getString("ENTTAX"));
                            totalTV.setText(obj.getString("TOTAL"));
                            mobileNumberED.setText(obj.getString("MOBILENO"));
                            call = obj.getString("MOBILENO");
                            stbChargesTV.setText(obj.getString("ADDITIONAL"));
                            noofTV.setText(obj.getString("NoOfTVs"));
                            subscriberCodeTV.setText(obj.getString("SUBSNO"));
                            Values.customerIdSelected = obj.getString("SUBSID");
                            subscriberIdTV.setText(Values.customerIdSelected);
                            //added to show default payable amount
                            if(Double.parseDouble(obj.getString("BALANCE"))<=0){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(BillingDetailsActivity.this, "Already Paid, hence Balance < 0", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }else{
                                amtpaidED.setText(obj.getString("BALANCE"));
                            }

                            if(!Values.AMTEDITFLAG.equalsIgnoreCase("Y")){
                                amtpaidED.setEnabled(false);
                                amtpaidED.setFocusable(false);
                            }
                            dueDate.setText(obj.getString("DueDate"));

                        }catch (Exception ex){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(BillingDetailsActivity.this, "Some error occured", Toast.LENGTH_SHORT).show();
                                }
                            });
                         }
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Values.selectedArea = "No Area";
                                Toast.makeText(BillingDetailsActivity.this, "Invalid Login Details or Session Timed out", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(BillingDetailsActivity.this, "Cannot connect to the server, please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                progressDialog.dismiss();
            }
        }
    }

    /**
     * Background task for inserting payment by calling collection API
     */

    private class InsertPayment extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();
        /**
         * The Remarks.
         */
        String remarks = "", /**
         * The Addcharges.
         */
        addcharges="", /**
         * The Payamt.
         */
        payamt="";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(BillingDetailsActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url = null;
            String payamt = params[0];
            String remarks = params[1];
            String addcharges = params[2];
            String bank = params[3];
            String cheque = params[4];
            String result=null;
            final String disamt="0";
            if(remarks.equalsIgnoreCase("")){
                remarks = "";
            }
            if(addcharges.equalsIgnoreCase("")){
                addcharges="0";
            }
            String uri = "";
            try {
                if(payMode.equalsIgnoreCase("Cheque")){
                    Log.e("Cheque ", payMode);
                    try {
                        uri = Constants.INSERT_PAYMENT + "username=" + localStore.getUserDetails().getUserName() + "&disflag=N&Lcoid=" +
                                Values.selectedLcoId + "&Subsid=" + Values.customerIdSelected + "&PaymentMode=Q&AdditionalCharges="
                                + addcharges + "&disamt=0" + disamt + "&payamt=" + payamt + "&BankName=" + bank +
                                "&ChequeNo=" + cheque + "&Remark=" + remarks + "&Geolocation="
                                + "&Token=" + localStore.getUserDetails().getToken();
                        url = new URL(uri.replaceAll(" ","%20"));
                    }catch (Exception ex) {
                        Log.e("Exception Cheque ", ex.toString());
                    }
                }else if(payMode.equalsIgnoreCase("Cash")){
                    Log.e("Cash ", payMode);
                    try{
                        uri = Constants.INSERT_PAYMENT+"username="+localStore.getUserDetails().getUserName()+"&disflag=N&Lcoid=" +
                                Values.selectedLcoId+"&Subsid="+Values.customerIdSelected+"&PaymentMode=C&AdditionalCharges="
                                +addcharges+"&disamt=0"+disamt+"&payamt="+payamt+"&BankName="+null+
                                "&ChequeNo="+null+"&Remark="+remarks+"&Geolocation="
                                +"&Token="+localStore.getUserDetails().getToken();
                        url = new URL(uri.replaceAll(" ","%20"));
                    }catch (Exception ex){
                        Log.e("Exception Cash ", ex.toString());

                    }


                }
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (Exception ex){
                Log.e("Exception ",ex.toString());
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                final JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "0":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                try{
                                    AlertDialog.Builder dialog=new AlertDialog.Builder(BillingDetailsActivity.this);
                                    dialog.setTitle("Successful Payment");
                                    dialog.setMessage("Payment successful. Kindly, note your Receipt No : "+obj.getString("receiptno"));
                                    dialog.setPositiveButton("Print Receipt ", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //code to print
                                            customerName=customerNameTV.getText().toString();
                                            String mobileNumber = mobileNumberED.getText().toString();
                                            String address=addressTV.getText().toString();
                                            String arrears=arrearsTV.getText().toString();
                                            String stbCharges=stbChargesTV.getText().toString();
                                            String basics=basicsTV.getText().toString();
                                            //String enttax=entTaxTV.getText().toString();
                                            String sertax=serTaxTV.getText().toString();
                                            String total=totalTV.getText().toString();
                                            String paidamt=paidAmtTV.getText().toString();
                                            String balance=balanceTV.getText().toString();
                                            String addcharges=addchargesED.getText().toString();
                                            String amtpaid=amtpaidED.getText().toString();
                                            try {
                                                subscode = obj.getString("subscode");
                                                stbno = obj.getString("stbno");
                                                outstanding = obj.getString("outstanding");
                                                Values.selectedlcoName = obj.getString("lconame");
                                                Values.selectedlcoAddress = obj.getString("lcoaddress");
                                                Values.selectedlcoPhone1 = obj.getString("lcocontact");
                                                Values.selectedlcoPhone2 = obj.getString("lcoaltcontact");

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            try {
                                                transactionTime = obj.getString("ReceiptDateTime");
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    // btPrinter.OpenBTConnection();
                                                    String line="------------------------";
                                                    String mobileNumber = mobileNumberED.getText().toString();
                                                    String address=addressTV.getText().toString();
                                                    String noofTvs = noofTV.getText().toString();
                                                    String arrears=arrearsTV.getText().toString();
                                                    String stbCharges=stbChargesTV.getText().toString();
                                                    String basics=basicsTV.getText().toString();
                                                    //String enttax=entTaxTV.getText().toString();
                                                    String sertax=serTaxTV.getText().toString();
                                                    String cgst = cgstTV.getText().toString();
                                                    String igst = igstTV.getText().toString();
                                                    String total=totalTV.getText().toString();
                                                    String paidamt=paidAmtTV.getText().toString();
                                                    String balance=balanceTV.getText().toString();
                                                    String addcharges=addchargesED.getText().toString();
                                                    String amtpaid=amtpaidED.getText().toString();
                                                    String remark=remarksED.getText().toString();
                                                    Double totalamt;
                                                    if(addcharges.equalsIgnoreCase("")){
                                                        addcharges = "0";
                                                        totalamt = Double.parseDouble(amtpaid)+Integer.parseInt(addcharges);

                                                    }
                                                    else{
                                                        totalamt = Double.parseDouble(amtpaid)+Double.parseDouble(addcharges);

                                                    }

                                                    String data = "   Invoice Cum Receipt   \n\n"+
                                                            Values.selectedlcoName+"\n"+Values.selectedlcoAddress+"\n"+Values.selectedlcoPhone1+"\n"+Values.selectedlcoPhone2+"\n"+line+"\n"+"Cust Id:"+subscode+"\n"+"Name:"+customerName+"\n"+
                                                            "Add:"+address+"\n"+"Mob:" +mobileNumber+"\n"+line+"\n"+"Due Date: "+dueDate.getText().toString()+"\n"+
                                                            "No of Tvs:"+noofTvs+"\n";
                                                    try {
                                                        data = data+"Receipt No:"+obj.getString("receiptno")+"\n"+"Invoice No:"+obj.getInt("InvoiceNo")+"\n"+""+transactionTime;
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    Date date = new Date();
                                                    data = data+"\n"+line+"\n"+"Arrears        :"+""+arrears+"\n"+"Add STB Charges:"+stbCharges+"\n"+"Basics         :"+basics+"\n";
                                                    if(Values.etstFlag.equalsIgnoreCase("Y")){
                                                        if(sertax.equalsIgnoreCase("0")&& cgst.equalsIgnoreCase("0")){
                                                            data = data+"SGST(0%)       :"+sertax+"\n"+"CGST(0%)       :"+cgst+"\n"+"IGST(18%)      :"+igst;
                                                        }else{
                                                            data = data+"SGST(9%)       :"+sertax+"\n"+"CGST(9%)       :"+cgst+"\n"+"IGST(0%)       :"+igst;
                                                        }
                                                    }

                                                    data=data+"Total          :"+total+"\n"+"Prev. paid     :"+paidamt+"\n";
                                                    if(addcharges.equalsIgnoreCase("")){
                                                        data = data +line+"\n"+ "Addn Charge      : \n";
                                                    }
                                                    else {
                                                        data = data+line+"\n"+"Addn Charge    :"+addcharges+"\n";
                                                    }
                                                    data = data+"Current Payment:"+amtpaid+"\n"+"Total Paid     :"+totalamt+"\n"+
                                                            "Balance        :"+outstanding+"\n"+line+"\n";
                                                    if(payMode.equalsIgnoreCase("cash"))
                                                        data= data+"Paid By:Cash"+"\n"+line+"\n";
                                                    else{
                                                        String bankName=bankNameED.getText().toString();
                                                        String chequeno=chequeNoED.getText().toString();
                                                        data = data+"Paid By  :Cheque\n"+"Bank Name: "+bankName+"\n"+"Cheque No: "+chequeno+"\n"+line+"\n";
                                                    }
                                                    data = data+"Remark :"+remark+"\n"+line+"\n";

                                                    if(Values.etstFlag=="Y"){

                                                        data = data+"\n"+"Amount is non refundable\n"+
                                                                "      Powered By CABLEGUY\n"+
                                                                "      WWW.CABLEGUY.IN\n" +
                                                                "      Version :"+Values.currentVersion+""+"\n"+"\n"+"\n"+"\n"+"\n";
                                                    }
                                                    else{
                                                        data = data +
                                                                "      Powered By CABLEGUY\n"+
                                                                "      WWW.CABLEGUY.IN\n"+
                                                                "      Version :"+Values.currentVersion+""+"\n"+"\n"+"\n"+"\n"+"\n";
                                                    }

                                                    if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
                                                        if(evresult){
                                                            try {
                                                                ptrGen = new Printer_GEN(GlobalApp.setup, btPrinter.evoluteOutputStream, btPrinter.evoluteInputStream);
                                                                ptrGen.iAddData((byte) 0x01,data);
                                                                iRetVal = ptrGen.iStartPrinting(1);
                                                            } catch (Exception e) {
                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        Toast.makeText(BillingDetailsActivity.this, "Printer Error", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });

                                                            }
                                                        }else{
                                                            runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Toast.makeText(BillingDetailsActivity.this, "Cannot connect to the printer, Please try again!", Toast.LENGTH_SHORT).show();
                                                                }
                                                            });

                                                        }
                                                    }else{
                                                        btPrinter.PrintLine(data,'M');
                                                    }
                                                    Values.stbNumber ="";
                                                    Values.selectedCustomerNumber = "";
                                                    Values.cp1 = "";
                                                    Values.cardNumber = "";
                                                    customerNumber = "";
                                                    customerIdSelected="";
                                                }
                                            }).start();
                                            Intent intent = new Intent(BillingDetailsActivity.this,SuccessActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    });
                                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(BillingDetailsActivity.this,SuccessActivity.class);
                                            startActivity(intent);
                                            finish();
                                            dialog.dismiss();
                                        }
                                    });
                                    dialog.setCancelable(false);
                                    dialog.show();
                                }
                                catch (JSONException e){
                                }
                            }
                        });
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final AlertDialog.Builder errorAlert = new AlertDialog.Builder(BillingDetailsActivity.this);
                                errorAlert.setMessage("There was some error, please try again!");
                                errorAlert.setCancelable(true);

                                errorAlert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.cancel();
                                    }
                                });
                                AlertDialog errorAlertDialog = errorAlert.create();
                                errorAlertDialog.show();

/*                                Toast.makeText(BillingDetailsActivity.this, "There was some error, please try again!", Toast.LENGTH_SHORT).show();*/
                            }
                        });
                        break;
                    case "2":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final AlertDialog.Builder errorAlert = new AlertDialog.Builder(BillingDetailsActivity.this);
                                errorAlert.setMessage("You do not have sufficient balance to carry this transaction, please contact LCO");
                                errorAlert.setCancelable(true);

                                errorAlert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.cancel();
                                    }
                                });
                                AlertDialog errorAlertDialog = errorAlert.create();
                                errorAlertDialog.show();
/*                                Toast.makeText(BillingDetailsActivity.this, "Insufficient Balance, Contact LCO", Toast.LENGTH_SHORT).show();*/
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(BillingDetailsActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                progressDialog.dismiss();
            }
        }
    }

    /**
     * Background task for online payment order id generation
     */
    private class OnlinePayment extends AsyncTask<String, Void, JSONObject>{
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";
        /**
         * The Subs id.
         */
        String subsId, /**
         * The Username.
         */
        username, /**
         * The Payamt.
         */
        payamt, /**
         * The Token.
         */
        token, /**
         * The Remarks.
         */
        remarks;
        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            subsId=Values.customerIdSelected;
            username=localStore.getUserDetails().getUserName();
            payamt=Values.ORDER_AMOUNT;
            token=localStore.getUserDetails().getToken();
            remarks=remarksED.getText().toString();
            if(remarks.equalsIgnoreCase("")){
                remarks = "";
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog = new ProgressDialog(BillingDetailsActivity.this);
                    progressDialog.setCancelable(true);
                    progressDialog.setMessage("Loading ...");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();
                }
            });
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                String uri = Constants.ONLINE_PAYMENT+"username="+localStore.getUserDetails().getUserName()+"&Subsid="+subsId+"&Phone="+phone1+"&Email="+"&payamt="+payamt+"&Remark="+remarks+"&Token="+localStore.getUserDetails().getToken();
                url = new URL(uri.replaceAll(" ","%20"));
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                final JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "0":
                        Values.ORDER_ID=obj.getString("ORDERID");
                        generatePAYLOAD(Values.ORDER_AMOUNT);
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(BillingDetailsActivity.this, "Invalid Details, Please Try again", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(BillingDetailsActivity.this, "Cannot connect to the Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Online Pay Exception ",ex.toString());
            }finally {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public void onDestroy() {
        //unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    /**
     * List Bluetooth devices.
     */
    public void listDevices(){
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        /*IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mReceiver, filter);
        mBluetoothAdapter.startDiscovery();*/
        //For Only Paired Devices
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        List<String> s = new ArrayList<String>();
        for(BluetoothDevice bt : pairedDevices){
            s.add(bt.getName());
        }
/*
        Intent i=new Intent(BillingDetailsActivity.this,BluetoothDevicesActivity.class);
        startActivity(i);
*/

        //populate this list and setup connection to selected bluetooth device
        //Refer
        /*
        http://www.codeproject.com/Articles/814814/Android-Connectivity
         */


    }
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                //discovery starts, we can show progress dialog or perform other tasks
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //discovery finishes, dismis progress dialog
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //bluetooth device found
                BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                bluetoothDeviceList.add(device);
            }
        }
    };


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    listDevices();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Toast.makeText(BillingDetailsActivity.this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    //finish();
                }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.topbarmenu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_refresh:
                new FetchCustomerData().execute();
                break;
            case R.id.action_call:
                try{
                    if(call.equalsIgnoreCase("0")||call.equalsIgnoreCase("")||call.length() < 10 ){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(BillingDetailsActivity.this, "Invalid Phone number", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }else{
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + call));
                        int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
                        if (currentAPIVersion >= Build.VERSION_CODES.M) {
                            if (ContextCompat.checkSelfPermission(BillingDetailsActivity.this,
                                    Manifest.permission.CALL_PHONE)
                                    != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(BillingDetailsActivity.this,
                                        new String[]{Manifest.permission.CALL_PHONE},
                                        MY_PERMISSIONS_CALL_PHONE);
                            } else {
                                startActivity(intent);

                            }
                        }
                        else{
                            startActivity(intent);

                        }

                    }

                }
                catch (Exception ex){
                    FirebaseCrash.log(ex.toString());
                }
                break;

            case R.id.action_btConnect:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if(btPrinter.isBTConnected){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(BillingDetailsActivity.this, "Printer is already connected", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }else{
                            btPrinter.OpenBTConnection();
                            if(btPrinter.isBTConnected){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(BillingDetailsActivity.this, "Connected Succesfully", Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }else{
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(BillingDetailsActivity.this, "Cannot connect to printer, Please try again!", Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }
                        }

                    }
                }).start();
                break;
            case R.id.contactSupport:
                Intent intent = new Intent(BillingDetailsActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }

    /**
     * Background task to submit reasons for customer no show
     */

    private class SubmitSLC extends AsyncTask<String, Void, JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Slc option.
         */
        String slcOption;
        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();
        /**
         * The Remarks.
         */
        String remarks = "", /**
         * The Addcharges.
         */
        addcharges="", /**
         * The Payamt.
         */
        payamt="";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(BillingDetailsActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    payamt=amtpaidED.getText().toString().replace("₹","");
                    remarks=remarksED.getText().toString();
                    addcharges=addchargesED.getText().toString().replace("₹","");
                }
            });
            String result=null;
            final String disamt="0";
            if(remarks.equalsIgnoreCase("")){
                remarks = "";
            }
            if(addcharges.equalsIgnoreCase("")){
                addcharges="0";
            }
            try {
                String uri;
                URL url = null;
                if(Values.CHEQUE_PAYMENT){
                    uri = Constants.INSERT_PAYMENT+"username="+localStore.getUserDetails().getUserName()+"&disflag=N&Lcoid=" +
                            Values.selectedLcoId+"&Subsid="+Values.customerIdSelected+"&PaymentMode=Q&AdditionalCharges="
                            +addcharges+"&disamt=0"+disamt+"&payamt="+payamt+"&BankName="+bank+
                            "&ChequeNo="+cheque+"&Remark="+remarks+"&Geolocation="
                            +"&Token="+localStore.getUserDetails().getToken();
                    url = new URL(uri.replaceAll(" ","%20"));
                }else if(Values.SLC_ENABLED){
                    if(Values.SLC_OPTION.equalsIgnoreCase("Select an Option")){
                        Toast.makeText(BillingDetailsActivity.this, "Please select an option", Toast.LENGTH_SHORT).show();
                    }else{
                        try {
                            uri = Constants.INSERT_PAYMENT+"username="+localStore.getUserDetails().getUserName()+"&disflag=N&Lcoid=" +
                                    Values.selectedLcoId+"&Subsid="+Values.customerIdSelected+"&PaymentMode=SLC&AdditionalCharges="
                                    +addcharges+"&disamt=0"+disamt+"&payamt="+payamt+"&BankName="+Values.SLC_OPTION+
                                    "&ChequeNo=0"+"&Remark="+remarks+"&Geolocation="
                                    +"&Token="+localStore.getUserDetails().getToken();
                            url = new URL(uri.replaceAll(" ","%20"));
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                }else{
                    try{
                        uri = Constants.INSERT_PAYMENT+"username="+localStore.getUserDetails().getUserName()+"&disflag=N&Lcoid=" +
                                Values.selectedLcoId+"&Subsid="+Values.customerIdSelected+"&PaymentMode=C&AdditionalCharges="
                                +addcharges+"&disamt=0"+disamt+"&payamt="+payamt+"&BankName="+null+
                                "&ChequeNo="+null+"&Remark="+remarks+"&Geolocation="
                                +"&Token="+localStore.getUserDetails().getToken();
                        url = new URL(uri.replaceAll(" ","%20"));

                    }catch (Exception ex){
                        Log.e("Error", ex.toString());
                    }

                }
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn", ":" + conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode", "" + statuscode);
                Log.e("response", conn.getResponseMessage());
                if (statuscode == 200) {
                    is = new BufferedInputStream(conn.getInputStream());
                } else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue", "value 2" + json);
            } catch (Exception e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                final JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "1":
                        BillingDetailsActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{

                                    AlertDialog.Builder dialog=new AlertDialog.Builder(BillingDetailsActivity.this);
                                    dialog.setTitle("Successful");
/*                                    dialog.setMessage("Payment successful. Kindly, note your Receipt No : "+jsonObject.getString("receiptno"));*/
                                    dialog.setPositiveButton("Print Receipt ", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //code to print
                                            customerName=customerNameTV.getText().toString();
                                            String mobileNumber = mobileNumberED.getText().toString();
                                            String address=addressTV.getText().toString();
                                            String arrears=arrearsTV.getText().toString();
                                            String stbCharges=stbChargesTV.getText().toString();
                                            String basics=basicsTV.getText().toString();
                                            String enttax=entTaxTV.getText().toString();
                                            String sertax=serTaxTV.getText().toString();
                                            String total=totalTV.getText().toString();
                                            String paidamt=paidAmtTV.getText().toString();
                                            String balance=balanceTV.getText().toString();
                                            String addcharges=addchargesED.getText().toString();
                                            String amtpaid=amtpaidED.getText().toString();
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    // btPrinter.OpenBTConnection();
                                                    String line="----------------------------";
                                                    String mobileNumber = mobileNumberED.getText().toString();
                                                    String address=addressTV.getText().toString();
                                                    String remark=remarksED.getText().toString();
                                                    Calendar c = Calendar.getInstance();
                                                    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                                                    String formattedDate = df.format(c.getTime());
                                                    String data = "         Visit Report \n\n"+
                                                            Values.selectedlcoName+"\n"+Values.selectedlcoAddress+"\n"+Values.selectedlcoPhone1+"\n"+Values.selectedlcoPhone2+"\n"+line+"\n"+"Customer Code: "+ subscriberCodeTV.getText().toString()+"\n"+"Name: "+customerName+"\n"+
                                                            "Add: "+address+"\n"+"Mob: " +mobileNumber+"\n"+line+"\n"+"Date : "+formattedDate+"\n";
                                                    data = data +"Reason :"+slcListSP.getSelectedItem().toString()+"\n";
                                                    data = data+"Remark: "+remark+"\n"+line+"\n"
                                                            + "Powered By CABLEGUY \n"
                                                            + "WWW.CABLEGUY.IN \n"+"v "+Values.currentVersion+""+"\n"+"\n"+"\n"+"\n"+"\n";
                                                    if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
                                                        if(evresult){
                                                            try {
                                                                ptrGen = new Printer_GEN(GlobalApp.setup, btPrinter.evoluteOutputStream, btPrinter.evoluteInputStream);
                                                                ptrGen.iAddData((byte) 0x01,data);
                                                                iRetVal = ptrGen.iStartPrinting(1);
                                                            } catch (Exception e) {
                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        Toast.makeText(BillingDetailsActivity.this, "Printer Error", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });

                                                            }
                                                        }else{
                                                            runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    Toast.makeText(BillingDetailsActivity.this, "Cannot connect to the printer, Please try again!", Toast.LENGTH_SHORT).show();
                                                                }
                                                            });

                                                        }
                                                    }else{
                                                        btPrinter.PrintLine(data,'M');
                                                    }
                                                    Values.stbNumber ="";
                                                    Values.selectedCustomerNumber = "";
                                                    Values.cp1 = "";
                                                    Values.cardNumber = "";
                                                    customerNumber = "";
                                                    customerIdSelected="";
                                                }
                                            }).start();
                                            Intent intent = new Intent(BillingDetailsActivity.this,SuccessActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }
                                    });
                                    dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(BillingDetailsActivity.this,SuccessActivity.class);
                                            startActivity(intent);
                                            finish();
                                            dialog.dismiss();
                                        }
                                    });
                                    dialog.setCancelable(false);
                                    dialog.show();
                                }
                                catch (Exception e){
                                }
                            }
                        });
                        break;
                    case "0":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final AlertDialog.Builder errorAlert = new AlertDialog.Builder(BillingDetailsActivity.this);
                                errorAlert.setMessage("There was some error, please try again!");
                                errorAlert.setCancelable(true);

                                errorAlert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.cancel();
                                    }
                                });
                                AlertDialog errorAlertDialog = errorAlert.create();
                                errorAlertDialog.show();

/*                                Toast.makeText(BillingDetailsActivity.this, "There was some error, please try again!", Toast.LENGTH_SHORT).show();*/
                            }
                        });
                        break;
                    case "2":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                final AlertDialog.Builder errorAlert = new AlertDialog.Builder(BillingDetailsActivity.this);
                                errorAlert.setMessage("You do not have sufficient balance to carry this transaction, please contact LCO");
                                errorAlert.setCancelable(true);

                                errorAlert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.cancel();
                                    }
                                });
                                AlertDialog errorAlertDialog = errorAlert.create();
                                errorAlertDialog.show();
/*                                Toast.makeText(BillingDetailsActivity.this, "Insufficient Balance, Contact LCO", Toast.LENGTH_SHORT).show();*/
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(BillingDetailsActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                progressDialog.dismiss();
            }
        }
    }

}
