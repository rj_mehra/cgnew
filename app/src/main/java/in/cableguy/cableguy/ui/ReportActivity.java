package in.cableguy.cableguy.ui;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.prowesspride.api.Printer_GEN;
import com.prowesspride.api.Setup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.btprinter.BTPrinter;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.GlobalApp;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.MessageHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 *
 * @author Raj Mehra
 * The type Report activity.
 *
 * This activity lists the terminal report for the user
 */
public class ReportActivity extends AppCompatActivity {
    /**
     * The Message.
     */
    MessageHelper message;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Report tl.
     */
    TableLayout reportTL;
    /**
     * The Submit button.
     */
    Button submitButton;
    /**
     * The Print button.
     */
    Button printButton;
    /**
     * The From date tv.
     */
    TextView fromDateTV, /**
     * The To date tv.
     */
    toDateTV;
    /**
     * The Customer count tv.
     */
    TextView customerCountTV, /**
     * The Cash count tv.
     */
    cashCountTV, /**
     * The Cheque count tv.
     */
    chequeCountTV, /**
     * The Online count tv.
     */
    onlineCountTV, /**
     * The Cash amount tv.
     */
    cashAmountTV, /**
     * The Cheque amount tv.
     */
    chequeAmountTV, /**
     * The Online amount tv.
     */
    onlineAmountTV;
    /**
     * The From date.
     */
    Date fromDate;
    /**
     * The Bt printer.
     */
    BTPrinter btPrinter;
    /**
     * The Formatted date.
     */
    String formattedDate;
    /**
     * The Ret val.
     */
    int iRetVal;
    /**
     * The Evresult.
     */
    boolean evresult;
    /**
     * The Ptr gen.
     */
    public Printer_GEN ptrGen;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        overridePendingTransition(0, 0);
        getSupportActionBar().setTitle("Report");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        reportTL=(TableLayout)findViewById(R.id.reportTL);
        submitButton=(Button)findViewById(R.id.submitButton);
        customerCountTV=(TextView)findViewById(R.id.customerCountTV);
        cashCountTV=(TextView)findViewById(R.id.cashCountTV);
        chequeCountTV=(TextView)findViewById(R.id.chequeCountTV);
        onlineCountTV=(TextView)findViewById(R.id.onlineCountTV);
        cashAmountTV=(TextView)findViewById(R.id.cashAmountTV);
        chequeAmountTV=(TextView)findViewById(R.id.chequeAmountTV);
        onlineAmountTV=(TextView)findViewById(R.id.onlineAmountTV);
        printButton=(Button)findViewById(R.id.printButton);
        message=new MessageHelper(ReportActivity.this);
        localStore=new LocalStore(ReportActivity.this);
        fromDateTV=(TextView)findViewById(R.id.fromDateTV);
        toDateTV=(TextView)findViewById(R.id.toDateTV);
        btPrinter=new BTPrinter(ReportActivity.this);
        fromDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFromDate(v);
            }
        });
        toDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fromDateTV.getText().toString().equals("From Date")){
                    message.displayShort("Kindly, select from Date before");
                }
                else{
                    pickToDate(v);
                }
            }
        });
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){

        }else{
            btPrinter=new BTPrinter(ReportActivity.this);
        }
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
            try {
                GlobalApp.setup = new Setup();
                evresult = GlobalApp.setup.blActivateLibrary(ReportActivity.this,R.raw.licencefull_pride_gen);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(evresult){
                Log.e("Library Activated ",evresult+"");
            }else{
                Log.e("Library failed", evresult+"");
            }
        }else{

        }
        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String line="------------------------";
                        String fDate = fromDateTV.getText().toString();
                        String tDate = toDateTV.getText().toString();
                        String customerCount=customerCountTV.getText().toString();
                        String cashCount=cashCountTV.getText().toString();
                        String chequeCount=chequeCountTV.getText().toString();
                        String onlineCount=onlineCountTV.getText().toString();
                        String cashAmount=cashAmountTV.getText().toString();
                        String chequeAmount=chequeAmountTV.getText().toString();
                        String onlineAmount=onlineAmountTV.getText().toString();
                        String data = "     Terminal Report"+"\n"+line+"\n"+localStore.getUserDetails().getLcoName()+"\n"+
                                localStore.getUserDetails().getLcoAddress();
                        if(localStore.getUserDetails().getPhone1()==null){

                        }else{
                            data = data +localStore.getUserDetails().getPhone1()+"\n";
                        }
                        if(localStore.getUserDetails().getPhone2()==null){

                        }else{
                            data = data + "localStore.getUserDetails().getPhone2()"+"\n";
                        }
/*                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);*/
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss", Locale.US);
                        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                        data = data+"\n"+"Printed by "+localStore.getUserDetails().getUserName()+"\n"+"Print Time "+sdf.format(new Date())+"\n"+line+"\n"+"from Date :"+fDate+"\n"+"to Date :"+tDate+"\n"+"Customer Count: "+customerCount+"\n"+"Cash Count: "+cashCount+"\n"+"Cheque Count: " +chequeCount+"\n"+
                                "Online Count: "+onlineCount+"\n"+"Cash Amount: "+cashAmount+"\n"+"Cheque Amount: " +chequeAmount+"\n"+
                                "Online Amount: " +onlineAmount+"\n"+line+"\n"+"Powered By: www.cableguy.in"+"\n"+"v "+ Values.currentVersion+"\n"+"\n"+"\n"+"\n"+"\n";
                        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
                            if(evresult){
                                Log.e("Result inside activity",evresult+"");
                                try {
                                    ptrGen = new Printer_GEN(GlobalApp.setup, btPrinter.evoluteOutputStream, btPrinter.evoluteInputStream);
                                    ptrGen.iAddData((byte) 0x01,data);
                                    iRetVal = ptrGen.iStartPrinting(1);
                                } catch (Exception e) {
                                    Log.e("Printer Error", e.toString());
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(ReportActivity.this, "Printer Error", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            }else{
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(ReportActivity.this, "Cannot connect to the printer, Please try again!", Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }
                        }else{
                            btPrinter.PrintLine(data,'M');
                        }
                    }
                }).start();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fromDateTV.getText().toString().equals("From Date")||toDateTV.getText().toString().equals("To Date")){

                    message.displayShort("Kindly, select from and to Date");
                }
                else{
                    ConnectivityManager cm = (ConnectivityManager) ReportActivity.this.
                            getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netInfo = cm.getActiveNetworkInfo();
                    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                        //Connectivity
                        new FetchReport().execute(fromDateTV.getText().toString(),toDateTV.getText().toString());
                    }
                    else{
                        //No connectivity
                        message.displayShort("No internet connectivity");

                    }
                }

            }
        });

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c.getTime());
        Log.e("date",formattedDate);
        ConnectivityManager cm = (ConnectivityManager) ReportActivity.this.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            //Connectivity
            fromDateTV.setText(formattedDate);
            toDateTV.setText(formattedDate);
            new FetchDefReport().execute();
        }
        else {
            //No connectivity
            message.displayShort("No internet connectivity");

        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(ReportActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * Background task for fetching reports
     */

    private class FetchReport extends AsyncTask<String,Void,JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(ReportActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url ;
            try{
                String fromDate = params[0];
                String toDate = params[1];
                url = new URL(Constants.GET_COLLECTION_REPORT+"FromDate="+fromDate+"&ToDate="+toDate+
                        "&Username="+localStore.getUserDetails().getUserName()+"&Token="+localStore.getUserDetails().getToken());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                final JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "0":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    customerCountTV.setText(obj.getString("TotalCustomerCount"));
                                    cashCountTV.setText(obj.getString("TotalCashCount"));
                                    chequeCountTV.setText(obj.getString("TotalChequeCount"));
                                    onlineCountTV.setText(obj.getString("TotalOnlineCount"));
                                    cashAmountTV.setText(obj.getString("TotalCashAmount"));
                                    chequeAmountTV.setText(obj.getString("TotalChequeAmount"));
                                    onlineAmountTV.setText(obj.getString("TotalOnlineAmount"));
                                }catch(Exception ex){
                                    Log.e("Fetch Report ", ex.toString());
                                }
                            }
                        });
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ReportActivity.this, "Cannot fetch report, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ReportActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
            }finally {
                progressDialog.dismiss();
            }
        }
    }

    /**
     * Pick from date.
     *
     * @param view the view
     */
    public void pickFromDate(View view) {
        final Calendar c1 = Calendar.getInstance();
        DatePickerDialog datedialog = new DatePickerDialog(ReportActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(
                            final DatePicker view,
                            int year,
                            int monthOfYear,
                            int dayOfMonth) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(NewAttendanceActivity.this, year+" : "+String.format("%02d",monthOfYear )+" : "+String.format("%02d",dayOfMonth), Toast.LENGTH_SHORT).show();
                        final String date1 =  year+"-" + String.format("%02d", (monthOfYear + 1)) + "-"  +String.format("%02d", dayOfMonth) ;

//                        final String date1 =  String.format("%02d", dayOfMonth)+"-"+ String.format("%02d", (monthOfYear + 1)) + "-"  +year;
                        AlertDialog.Builder dialog = new AlertDialog.Builder(ReportActivity.this);
                        dialog.setTitle("Date Selection");
                        dialog.setMessage("Do you want to select " + date1 + " ?");
                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                            }
                        });
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                ReportActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        fromDateTV.setText(date1);


                                    }
                                });
                            }
                        });
                        dialog.show();
                    }

                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DATE));
        datedialog.getDatePicker()
                .setMaxDate(
                        c1.getTimeInMillis()
                );
        /*if(view.getId()==R.id.toDateTV){

        }*/

        datedialog.show();
    }

    /**
     * Pick to date.
     *
     * @param view the view
     */
    public void pickToDate(View view) {
        final Calendar c1 = Calendar.getInstance();
        DatePickerDialog datedialog = new DatePickerDialog(ReportActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(
                            final DatePicker view,
                            int year,
                            int monthOfYear,
                            int dayOfMonth) {
                        // TODO Auto-generated method stub
                        //Toast.makeText(NewAttendanceActivity.this, year+" : "+String.format("%02d",monthOfYear )+" : "+String.format("%02d",dayOfMonth), Toast.LENGTH_SHORT).show();
                        final String date1 =  year+"-" + String.format("%02d", (monthOfYear + 1)) + "-"  +String.format("%02d", dayOfMonth) ;
/*                        final String date1 =  String.format("%02d", dayOfMonth)+"-"+ String.format("%02d", (monthOfYear + 1)) + "-"  +year;*/
                        AlertDialog.Builder dialog = new AlertDialog.Builder(ReportActivity.this);
                        dialog.setTitle("Date Selection");
                        dialog.setMessage("Do you want to select " + date1 + " ?");
                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                            }
                        });
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                ReportActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        toDateTV.setText(date1);

                                    }
                                });
                            }
                        });
                        dialog.show();
                    }

                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DATE));

        datedialog.getDatePicker()
                .setMaxDate(
                        c1.getTimeInMillis()
                );
        /*datedialog.getDatePicker().setMinDate(
                fromDate.getTime()
        );*/

        datedialog.show();
    }

    /**
     *
     * Background task for fetching current date's report by default
     */

    private class FetchDefReport extends AsyncTask<String,Void,JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDialog = new ProgressDialog(ReportActivity.this);
                    progressDialog.setCancelable(true);
                    progressDialog.setMessage("Loading ...");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();

                }
            });
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                url = new URL(Constants.GET_COLLECTION_REPORT+"FromDate="+formattedDate+"&ToDate="+formattedDate+
                        "&Username="+localStore.getUserDetails().getUserName()+"&Token="+localStore.getUserDetails().getToken());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                final JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "0":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    customerCountTV.setText(obj.getString("TotalCustomerCount"));
                                    cashCountTV.setText(obj.getString("TotalCashCount"));
                                    chequeCountTV.setText(obj.getString("TotalChequeCount"));
                                    onlineCountTV.setText(obj.getString("TotalOnlineCount"));
                                    cashAmountTV.setText(obj.getString("TotalCashAmount"));
                                    chequeAmountTV.setText(obj.getString("TotalChequeAmount"));
                                    onlineAmountTV.setText(obj.getString("TotalOnlineAmount"));
                                }catch(Exception ex){
                                    Log.e("Fetch Report ", ex.toString());
                                }
                            }
                        });
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ReportActivity.this, "Cannot fetch report, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ReportActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
            }finally {
                progressDialog.dismiss();
            }
        }
    }


}
