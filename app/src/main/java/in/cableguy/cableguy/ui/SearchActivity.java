package in.cableguy.cableguy.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.Customer;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.MessageHelper;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
/**
 *
 * @author Raj Mehra
 *
 * The type  Search activity.
 * This is the search activity relating to Normal User access, search options involve mobile,stb,cardno,subscode and name
 *
 * in this activity case, LCO selection is skipped
 *
 * Flow is
 *
 * Search for Customer (This activity) > Billingdetails activity
 *
 * @see SearchActivity
 * @see BillingDetailsActivity
 *
 */
public class SearchActivity extends AppCompatActivity {
    /**
     * The Search customer id ed.
     */
    EditText searchCustomerIdED;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Message.
     */
    MessageHelper message;
    /**
     * The Search customer id button.
     */
    Button searchCustomerIdButton, /**
     * The Search customer name button.
     */
    searchCustomerNameButton;
    /**
     * The Search option list.
     */
    Spinner searchOptionList;
    /**
     * The Options adapter.
     */
    ArrayAdapter<String> optionsAdapter;
    /**
     * The List.
     */
    List<String> list;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        overridePendingTransition(0, 0);
        getSupportActionBar().setTitle("Search Customer");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        searchCustomerIdED=(EditText)findViewById(R.id.searchCustomerIdED);
        searchCustomerIdButton=(Button)findViewById(R.id.searchCustomerIDButton);
        searchCustomerNameButton=(Button)findViewById(R.id.searchCustomerNameButton);
        searchOptionList = (Spinner)findViewById(R.id.searchOptionList);
        list = new ArrayList<String>();
        list.add("Select Option");
        list.add("Search By Customer Code");
        list.add("Search By Mobile");
        list.add("Search By Card Number");
        list.add("Search By STB Number");
        optionsAdapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_item, list);
        optionsAdapter.setDropDownViewResource(R.layout.spinner_layout);
        searchOptionList.setAdapter(optionsAdapter);
        searchOptionList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selected = adapterView.getItemAtPosition(i).toString();
                Values.searchType="none";

                switch (i){
                    case 0 :
                        Log.e("Nothing Selected ", i+"");
                        Values.searchType="none";
                        break;
                    case 1 :
                        Log.e("Search Type ", "Customer Code "+i);
                        Values.searchType="CC";
                        searchCustomerIdED.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_NUMBER);
                        searchCustomerIdED.setSelection(searchCustomerIdED.getText().length());
                        break;
                    case 2 :
                        Log.e("Search Type ", "Mobile "+i);
                        Values.searchType="CP1";
                        Values.selectedCustomerName = "";
                        Values.cardNumber="";
                        Values.stbNumber="";
                        Values.customerNumber = "";
                        searchCustomerIdED.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_NUMBER);
                        searchCustomerIdED.setSelection(searchCustomerIdED.getText().length());
                        break;
                    case 3 :
                        Log.e("Search Type ", "Card Number "+i);
                        Values.searchType="CRD";
                        Values.selectedCustomerName = "";
                        Values.cp1="";
                        Values.customerNumber = "";
                        Values.stbNumber="";
                        searchCustomerIdED.setInputType(InputType.TYPE_CLASS_TEXT);
                        searchCustomerIdED.setSelection(searchCustomerIdED.getText().length());
                        break;
                    case 4 :
                        Log.e("Search Type ", "STB "+i);
                        Values.searchType="STB";
                        Values.cardNumber="";
                        Values.customerNumber = "";
                        Values.selectedCustomerName = "";
                        Values.cp1="";
                        searchCustomerIdED.setInputType(InputType.TYPE_CLASS_TEXT);
                        searchCustomerIdED.setSelection(searchCustomerIdED.getText().length());
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        localStore=new LocalStore(SearchActivity.this);
        message=new MessageHelper(SearchActivity.this);
        searchCustomerNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Values.customerListRedirect="SearchActivity";
                Values.searchType="CN";
                Intent i =new Intent(SearchActivity.this,CustomerListActivity.class);
                startActivity(i);
                finish();
            }
        });
        searchCustomerIdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (Values.searchType){
                    case "CC" :
                        Values.searchType="CC";
                        new FetchCustomer().execute();
                        break;
                    case "CP1" :
                        Values.searchType="CP1";
                        new FetchCustomer().execute();
                        break;
                    case "CRD" :
                        Values.searchType="CRD";
                        new FetchCustomer().execute();
                        break;
                    case "STB" :
                        Values.searchType="STB";
                        new FetchCustomer().execute();
                        break;
                    default:
                        Toast.makeText(SearchActivity.this, "Select an option and try again", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
        searchCustomerIdED.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){

                }
            }
        });
        searchCustomerIdED.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //fetchCustomer();
            }
        });
    }

    /**
     * Background task to check if customer exists with the entered search term
     */

    private class FetchCustomer extends AsyncTask<String,Void,JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";
        /**
         * The Customer number.
         */
        String customerNumber;
        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            customerNumber = searchCustomerIdED.getText().toString();
            Values.selectedCustomerNumber = customerNumber;
            progressDialog = new ProgressDialog(SearchActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {

            try{
                URL url = null;
                HttpURLConnection conn;
                String uri = "";
                if(Values.searchType.equalsIgnoreCase("CC")){
                    uri = Constants.SUBS_SEARCH+"mode=CC&Lcoid="+ Values.selectedLcoId+"&SubsName="+Values.selectedCustomerName+
                            "&SubsNo="+customerNumber+"&MobNo=&CRDNO=&STBNO=&UserID="+localStore.getUserDetails().getUserName()+
                            "&Token="+localStore.getUserDetails().getToken();
                    url = new URL(uri.replaceAll(" ","%20"));
                }else if(Values.searchType.equalsIgnoreCase("CN")){
                    uri = Constants.SUBS_SEARCH+"mode=CN&Lcoid="+ Values.selectedLcoId+"&SubsName="+Values.selectedCustomerName+
                            "&SubsNo="+Values.selectedCustomer.getNumber()+"&MobNo=&CRDNO=&STBNO=&UserID="+localStore.getUserDetails().getUserName()+
                            "&Token="+localStore.getUserDetails().getToken();
                    url = new URL(uri.replaceAll(" ","%20"));

                }else if(Values.searchType.equalsIgnoreCase("CP1")){

                    
                            if(customerNumber.length()<10){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(SearchActivity.this, "Mobile number cannot be less than 10 digits", Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }else{
                                try {
                                    uri = Constants.SUBS_SEARCH+"mode=CP1&Lcoid="+ Values.selectedLcoId+"&SubsName=&SubsNo=&MobNo="
                                            +customerNumber+"&CRDNO=&STBNO=&UserID="+localStore.getUserDetails().getUserName()+
                                            "&Token="+localStore.getUserDetails().getToken();
                                    url= new URL(uri.replaceAll(" ", "%20"));
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }

                            }

                }else if(Values.searchType.equalsIgnoreCase("CRD")){
                     uri = Constants.SUBS_SEARCH+"mode=CRD&Lcoid="+ Values.selectedLcoId+"&SubsName=&SubsNo=&MobNo=&CRDNO="+customerNumber+
                                        "&STBNO=&UserID="+localStore.getUserDetails().getUserName()+
                                        "&Token="+localStore.getUserDetails().getToken();
                                url = new URL(uri.replaceAll(" ","%20"));


                }else if(Values.searchType.equalsIgnoreCase("STB")){
                                uri = Constants.SUBS_SEARCH+"mode=STB&Lcoid="+ Values.selectedLcoId+"&SubsName=&SubsNo=&MobNo=&CRDNO=&STBNO="
                                        +customerNumber+"&UserID="+localStore.getUserDetails().getUserName()+
                                        "&Token="+localStore.getUserDetails().getToken();
                                url= new URL(uri.replaceAll(" ","%20"));
                    
                }else{

                }
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (Exception e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                final JSONObject obj = dataArray.getJSONObject(0);
                switch (status) {
                    case "0":
                        try{
                            String customerName = obj.getString("SUBSNAME");
                            String customerId = obj.getString("SUBSID");
                            Customer customer = new Customer(customerId, customerNumber, customerName);
                            Values.selectedCustomer = customer;
                            if(Values.redirect.equalsIgnoreCase("BuildingSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("BuildingSubsTransaction")){
                                Log.e("Values Building ",Values.redirect);
                                Values.listType="subs";
                                Intent intent = new Intent(SearchActivity.this, LastTransactionListActivity.class);
                                startActivity(intent);
                            }else if(Values.redirect.equalsIgnoreCase("NormalSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("NormalSubsTransaction")){
                                Log.e("Values Normal ",Values.redirect);
                                Values.listType="subs";
                                Intent intent = new Intent(SearchActivity.this, LastTransactionListActivity.class);
                                startActivity(intent);
                            }else if(Values.redirect.equalsIgnoreCase("LCOSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("LCOSubsTransaction")){
                                Log.e("Values LCO ",Values.redirect);
                                Values.listType="subs";
                                Intent intent = new Intent(SearchActivity.this, LastTransactionListActivity.class);
                                startActivity(intent);
                            }else{

                                Intent i = new Intent(SearchActivity.this, BillingDetailsActivity.class);
                                startActivity(i);
                            }
                        }catch (Exception ex){
                            Log.e("Subs error ", ex.toString());
                        }
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(SearchActivity.this, "No Record Found, Please try again", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(SearchActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
            }finally {
                progressDialog.dismiss();
            }
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cgsupport,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.contactSupport:
                Intent intent = new Intent(SearchActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        redirect();
    }

    /**
     * Redirect.
     */
    public void redirect(){
        if(Values.redirect.equalsIgnoreCase("BuildingSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(SearchActivity.this, NavDrawerActivity.class);
            Values.redirect="";
            Values.listType="";
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }else if(Values.redirect.equalsIgnoreCase("NormalSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("NormalSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(SearchActivity.this, NavDrawerActivity3.class);
            Values.redirect="";
            Values.listType="";
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }else if(Values.redirect.equalsIgnoreCase("LCOSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("LCOSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(SearchActivity.this, NavDrawerActivity2.class);
            Values.redirect="";
            Values.listType="";
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }else if(Values.redirect.equalsIgnoreCase("BuildingType")){
            Intent intent = new Intent(SearchActivity.this, NavDrawerActivity.class);
            Values.redirect="";
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }else if(Values.redirect.equalsIgnoreCase("NormalType")){
            Intent intent = new Intent(SearchActivity.this, NavDrawerActivity3.class);
            Values.redirect="";
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }else if ((Values.redirect.equalsIgnoreCase("LCOType"))){
            Intent intent = new Intent(SearchActivity.this, NavDrawerActivity2.class);
            Values.redirect="";
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }


    }
}
