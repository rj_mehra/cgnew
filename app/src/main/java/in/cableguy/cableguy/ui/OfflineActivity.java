package in.cableguy.cableguy.ui;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.prowesspride.api.Printer_GEN;
import com.prowesspride.api.Setup;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.btprinter.BTPrinter;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.ConvertToISOString;
import in.cableguy.cableguy.helper.GlobalApp;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.OfflineDBHelper;

/**
 *
 * @author Raj Mehra
 * The type Offline activity.
 *
 * This activity is for offline transactions which stores the data on the phone and sends it later
 */
public class OfflineActivity extends AppCompatActivity {
    /**
     * The Subs name ed.
     */
    EditText subsNameED, /**
     * The Subs code ed.
     */
    subsCodeED, /**
     * The Paid amount ed.
     */
    paidAmountED, /**
     * The Add charges ed.
     */
    addChargesED, /**
     * The Mobile ed.
     */
    mobileED, /**
     * The Remark ed.
     */
    remarkED, /**
     * The Bank name ed.
     */
    bankNameED, /**
     * The Cheque no ed.
     */
    chequeNoED;
    /**
     * The Cheque date tv.
     */
    TextView chequeDateTV;
    /**
     * The Pay mode rg.
     */
    RadioGroup payModeRG;
    /**
     * The Cash rb.
     */
    RadioButton cashRB, /**
     * The Cheque rb.
     */
    chequeRB;
    /**
     * The Submit button.
     */
    Button submitButton;
    /**
     * The Pos.
     */
    int pos;
    /**
     * The Db helper.
     */
    OfflineDBHelper dbHelper;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Subs name.
     */
    String subsName, /**
     * The Subs code.
     */
    subsCode, /**
     * The Paid amount.
     */
    paidAmount, /**
     * The Add charges.
     */
    addCharges, /**
     * The Mobile.
     */
    mobile, /**
     * The Remark.
     */
    remark, /**
     * The Pay mode.
     */
    payMode, /**
     * The Bank name.
     */
    bankName, /**
     * The Cheque date.
     */
    chequeDate, /**
     * The Cheque no.
     */
    chequeNo;
    /**
     * The Cheque date tr.
     */
    TableRow chequeDateTR, /**
     * The Cheque no tr.
     */
    chequeNoTR, /**
     * The Bank name tr.
     */
    bankNameTR;
    /**
     * The Bt printer.
     */
    BTPrinter btPrinter;
    /**
     * The Ret val.
     */
    int iRetVal;
    /**
     * The Ptr gen.
     */
    public  Printer_GEN ptrGen;
    /**
     * The Evresult.
     */
    boolean evresult;
    private AdView mAdView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline);
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        subsNameED = (EditText)findViewById(R.id.subsNameED);
        subsCodeED = (EditText)findViewById(R.id.subsCodeED);
        paidAmountED = (EditText)findViewById(R.id.paidAmountED);
        addChargesED = (EditText)findViewById(R.id.addChargesED);
        mobileED = (EditText)findViewById(R.id.mobileED);
        remarkED = (EditText)findViewById(R.id.remarkED);
        payModeRG = (RadioGroup)findViewById(R.id.payModeRG);
        cashRB = (RadioButton)findViewById(R.id.cashRB);
        chequeRB = (RadioButton)findViewById(R.id.chequeRB);
        submitButton = (Button)findViewById(R.id.submitButton);
        bankNameED = (EditText)findViewById(R.id.bankNameED);
        chequeDateTV = (TextView)findViewById(R.id.chequeDateTV);
        chequeNoED = (EditText)findViewById(R.id.chequeNoED);
        bankNameTR = (TableRow)findViewById(R.id.bankNameTR);
        chequeNoTR = (TableRow)findViewById(R.id.chequeNoTR);
        chequeDateTR = (TableRow)findViewById(R.id.chequeDateTR);
        localStore = new LocalStore(OfflineActivity.this);
        cashRB.setChecked(true);
        Values.PAY_MODE = "C";

        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){

        }else{
            btPrinter=new BTPrinter(OfflineActivity.this);
        }
        if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
            try {
                GlobalApp.setup = new Setup();
                evresult = GlobalApp.setup.blActivateLibrary(OfflineActivity.this,R.raw.licencefull_pride_gen);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(evresult){
                Log.e("Library Activated ",evresult+"");
            }else{
                Log.e("Library failed", evresult+"");
            }
        }else{

        }

        payModeRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                pos=payModeRG.indexOfChild(findViewById(checkedId));
                switch(pos){
                    case 0:
                        Values.PAY_MODE = "C";
                        if(bankNameTR.getVisibility()==View.VISIBLE || chequeNoTR.getVisibility()==View.VISIBLE || chequeDateTR.getVisibility()==View.VISIBLE){
                            bankNameTR.setVisibility(View.GONE);
                            bankName = "";
                            chequeNo = "";
                            chequeDate = "";
                            chequeNoTR.setVisibility(View.GONE);
                            chequeDateTR.setVisibility(View.GONE);
                        }

                        break;
                    case 1:
                        Values.PAY_MODE = "Q";
                        if(bankNameTR.getVisibility()==View.GONE || chequeNoTR.getVisibility()==View.GONE || chequeDateTR.getVisibility()==View.GONE){
                            bankNameTR.setVisibility(View.VISIBLE);
                            chequeNoTR.setVisibility(View.VISIBLE);
                            chequeDateTR.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        });

        chequeDateTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickDate(v);
            }
        });

        dbHelper = new OfflineDBHelper(OfflineActivity.this);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                subsName = subsNameED.getText().toString();
                subsCode = subsCodeED.getText().toString();
                paidAmount = paidAmountED.getText().toString();
                addCharges = addChargesED.getText().toString();
                mobile = mobileED.getText().toString();
                remark = remarkED.getText().toString();
                bankName = bankNameED.getText().toString();
                chequeDate = chequeDateTV.getText().toString();
                chequeNo = chequeNoED.getText().toString();
                if(subsCode.equalsIgnoreCase("")|| paidAmount.equalsIgnoreCase("") || paidAmount.equalsIgnoreCase("0") || mobile.equalsIgnoreCase("")){

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(OfflineActivity.this, "Please enter amount and subscriber code to continue", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{

                    try{
                        if(dbHelper.insertData(localStore.getUserDetails().getUserName(),subsNameED.getText().toString(), subsCodeED.getText().toString(),Values.PAY_MODE,paidAmountED.getText().toString(),
                                addChargesED.getText().toString(),mobileED.getText().toString(),remarkED.getText().toString(),chequeDateTV.getText().toString(),chequeNoED.getText().toString(), bankNameED.getText().toString())){
                            Toast.makeText(OfflineActivity.this, "Successfully saved", Toast.LENGTH_SHORT).show();

                            if(btPrinter.isBTConnected){

                            }else{
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try{
                                            btPrinter.OpenBTConnection();
                                        }catch (Exception ex){
                                            Log.e("ex ",ex.toString());
                                        }

                                    }
                                }).start();

                            }
                            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
                            Date currentLocalTime = cal.getTime();
                            DateFormat date = new SimpleDateFormat("yyyy.MM.dd 'at' HH:mm:ss");
// you can get seconds by adding  "...:ss" to it
                            date.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));

                            String localTime = date.format(currentLocalTime);
                            String line = "------------------------";
                            Log.e("LCO Address ", Values.selectedlcoAddress);
                            String data="";
                            if (Values.PAY_MODE.equalsIgnoreCase("C")) {
                                data = "   Payment Receipt   \n\n"+
                                        Values.selectedlcoName+"\n"+Values.selectedlcoAddress+"\n"+Values.selectedlcoPhone1+"\n"+Values.selectedlcoPhone2+"\n"+line+"\n"+
                                        localTime+"\n"+"Cust Id:"+subsCodeED.getText().toString()+"\n"+"Name:"+subsNameED.getText().toString()+"\n"
                                        + "Mobile:"+mobileED.getText().toString()+"\n"+"Payment Mode:   Cash"+"\n"+"Amount Paid:    "+paidAmountED.getText().toString() +"\n"+"Additional:     "+addChargesED.getText().toString() +"\nRemarks:"+remarkED.getText().toString()+"\n";


                            }else if(Values.PAY_MODE.equalsIgnoreCase("Q")){
                                data = "   Payment Receipt   \n\n"+
                                        Values.selectedlcoName+"\n"+Values.selectedlcoAddress+"\n"+Values.selectedlcoPhone1+"\n"+Values.selectedlcoPhone2+"\n"+line+"\n"+
                                        localTime+"\n"+"Cust Id:"+subsCodeED.getText().toString()+"\n"+"Name:"+subsNameED.getText().toString()+"\n"
                                        + "Mobile:"+mobileED.getText().toString()+"\n"+"Payment Mode:   Cheque"+"\n"+"Bank Name:   "+bankName+"\n"+"Cheuque No:  "+chequeNo+"\n"+"Cheuque Date  "+chequeDate+"\n"+"Amount Paid:    "+paidAmountED.getText().toString() +"\n"+"Additional:     "+addChargesED.getText().toString() +"\nRemarks:"+remarkED.getText().toString()+"\n";


                            }
                            if(Values.etstFlag=="Y"){

                                data = data+"\n"+"Amount is non refundable\n"+
                                        "     Powered By CABLEGUY\n       "+
                                        "     WWW.CABLEGUY.IN\n" +
                                        "     Version :"+Values.currentVersion+""+"\n"+"\n"+"\n"+"\n"+"\n";
                            }
                            else{
                                data = data+"\n" +
                                        "     Powered By CABLEGUY\n      "+
                                        "     WWW.CABLEGUY.IN\n"+
                                        "     Version :"+Values.currentVersion+""+"\n"+"\n"+"\n"+"\n"+"\n";
                            }
                            if(Values.PRINTER_TYPE.equalsIgnoreCase("E")){
                                if(evresult){
                                    try {
                                        ptrGen = new Printer_GEN(GlobalApp.setup, btPrinter.evoluteOutputStream, btPrinter.evoluteInputStream);
                                        ptrGen.iAddData((byte) 0x01,data);
                                        iRetVal = ptrGen.iStartPrinting(1);
                                    } catch (Exception e) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(OfflineActivity.this, "Printer Error", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                    }
                                }else{
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(OfflineActivity.this, "Cannot connect to the printer, Please try again!", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            }else{
                                btPrinter.PrintLine(data,'M');
                            }

                            Intent intent = new Intent(OfflineActivity.this, OfflineSuccessActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        }else{
                            Toast.makeText(OfflineActivity.this, "Save failed, Please Try again!", Toast.LENGTH_SHORT).show();
                        }

                    }catch (Exception ex){
                        Toast.makeText(OfflineActivity.this, "Some error occured, please try again!", Toast.LENGTH_SHORT).show();
                        Log.e("DB Error ", ex.toString());
                    }
                }

            }
        });

        try{
            long count = dbHelper.dataCount();
            Log.e("Count ",count + "");
        }catch (Exception ex){
            Log.e("Can't Count ", ex.toString());
        }

    }

    /**
     * The Selected date.
     */
    String selectedDate;

    /**
     * Pick date.
     *
     * @param view the view
     */
    public void pickDate(View view) {
        final Calendar c1 = Calendar.getInstance();
        DatePickerDialog datedialog = new DatePickerDialog(OfflineActivity.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(
                            final DatePicker view,
                            int year,
                            int monthOfYear,
                            int dayOfMonth) {
                        final String date1 = year + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + String.format("%02d", dayOfMonth);
                        AlertDialog.Builder dialog = new AlertDialog.Builder(OfflineActivity.this);
                        dialog.setTitle("Date Selection");
                        dialog.setMessage("Do you want to select " + date1 + " ?");
                        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                            }
                        });
                        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                selectedDate = date1;
                                final DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                                Date d = new Date();
                                try {
                                    d = format.parse(date1);
                                } catch (Exception e) {
                                }
                                selectedDate = ConvertToISOString.getISO8601StringForDate(d);
                                OfflineActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        chequeDateTV.setText(date1);
                                    }
                                });
                            }
                        });
                        dialog.show();
                    }

                }, c1.get(Calendar.YEAR), c1.get(Calendar.MONTH), c1.get(Calendar.DATE));
        datedialog.getDatePicker().setMinDate(c1.getTimeInMillis());
        datedialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        redirect();
    }

    /**
     * Redirect.
     */
    public void redirect(){
        if(Values.redirect.equalsIgnoreCase("BuildingSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(OfflineActivity.this, NavDrawerActivity.class);
            Values.redirect="";
            Values.listType="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("NormalSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("NormalSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(OfflineActivity.this, NavDrawerActivity3.class);
            Values.redirect="";
            Values.listType="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("LCOSubsTransaction")||Values.lastTransaction.equalsIgnoreCase("LCOSubsTransaction")){
            Values.listType="subs";
            Intent intent = new Intent(OfflineActivity.this, NavDrawerActivity2.class);
            Values.redirect="";
            Values.listType="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("BuildingType")){
            Intent intent = new Intent(OfflineActivity.this, NavDrawerActivity.class);
            Values.redirect="";
            startActivity(intent);
        }else if(Values.redirect.equalsIgnoreCase("NormalType")){
            Intent intent = new Intent(OfflineActivity.this, NavDrawerActivity3.class);
            Values.redirect="";
            startActivity(intent);
        }else if ((Values.redirect.equalsIgnoreCase("LCOType"))){
            Intent intent = new Intent(OfflineActivity.this, NavDrawerActivity2.class);
            Values.redirect="";
            startActivity(intent);
        }


    }
}
