package in.cableguy.cableguy.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.ECAFCustomer;
import in.cableguy.cableguy.helper.LocalStore;


/**
 * @author Raj Mehra
 * The type Ecaf new customer activity.
 */
public class ECAFNewCustomerActivity extends AppCompatActivity {
    /**
     * The Next button.
     */
    Button nextButton;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    /**
     * The Local store.
     */
    LocalStore localStore;
    /**
     * The Cust name.
     */
    EditText custName, /**
     * The Cust address.
     */
    custAddress, /**
     * The Email id.
     */
    emailId, /**
     * The Pin code.
     */
    pinCode, /**
     * The Street.
     */
    street, /**
     * The Flat no.
     */
    flatNo, /**
     * The Phone number.
     */
    phoneNumber, /**
     * The Stb number.
     */
    stbNumber, /**
     * The Card number.
     */
    cardNumber, /**
     * The Inst charges.
     */
    instCharges, /**
     * The Stb charges.
     */
    stbCharges, /**
     * The Alt phone number.
     */
    altPhoneNumber;
    /**
     * The Cust type.
     */
    RadioGroup custType;
    /**
     * The Ecaf customer.
     */
    ECAFCustomer ecafCustomer;
    /**
     * The Area.
     */
    Spinner area, /**
     * The Location.
     */
    location, /**
     * The Building.
     */
    building, /**
     * The City.
     */
    city, /**
     * The State sp.
     */
    stateSP;
    /**
     * The State list.
     */
    ArrayList<String> stateList, /**
     * The Location list.
     */
    locationList, /**
     * The City list.
     */
    cityList, /**
     * The Building list.
     */
    BuildingList, /**
     * The State ids.
     */
    stateIds, /**
     * The Location id.
     */
    locationId, /**
     * The City ids.
     */
    cityIds, /**
     * The Building id.
     */
    buildingId;
    /**
     * The Bldgid.
     */
    String bldgid, /**
     * The State id.
     */
    stateId, /**
     * The City id.
     */
    cityId, /**
     * The Locid.
     */
    locid, /**
     * The Cust type rg.
     */
    custTypeRG, /**
     * The Req type.
     */
    reqType, /**
     * The Subsid.
     */
    subsid;
    /**
     * The Adapter.
     */
    ArrayAdapter<String> adapter, /**
     * The State adapter.
     */
    stateAdapter, /**
     * The City adapter.
     */
    cityAdapter, /**
     * The Loc adapter.
     */
    locAdapter, /**
     * The Building adapter.
     */
    buildingAdapter;
    /**
     * The Pos.
     */
    int pos;
    /**
     * The Add child stb.
     */
    Button addChildSTB;
    /**
     * The Child stbno 1 tr.
     */
    TableRow childSTBNO1TR, /**
     * The Child stbno 2 tr.
     */
    childSTBNO2TR, /**
     * The Child stbcard 1 tr.
     */
    childSTBCARD1TR, /**
     * The Child stbcard 2 tr.
     */
    childSTBCARD2TR;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecafnew_customer);
        custName = (EditText)findViewById(R.id.custName);
        emailId = (EditText)findViewById(R.id.emailId);
        custAddress = (EditText) findViewById(R.id.custAddress);
        city = (Spinner)findViewById(R.id.city);
        pinCode = (EditText)findViewById(R.id.pinCode);
        childSTBNO1TR = (TableRow)findViewById(R.id.childSTBNO1TR);
        childSTBNO2TR = (TableRow)findViewById(R.id.childSTBNO2TR);
        childSTBCARD1TR = (TableRow)findViewById(R.id.childSTBCARD1TR);
        childSTBCARD2TR = (TableRow)findViewById(R.id.childSTBCARD2TR);
        /*area = (Spinner)findViewById(R.id.area);*/
        location = (Spinner)findViewById(R.id.location);
        street = (EditText)findViewById(R.id.street);
        building = (Spinner)findViewById(R.id.building);
        flatNo = (EditText)findViewById(R.id.flatNo);
        phoneNumber = (EditText)findViewById(R.id.phoneNumber);
        stbNumber = (EditText)findViewById(R.id.stbNumber);
        cardNumber = (EditText)findViewById(R.id.cardNumber);
        altPhoneNumber = (EditText)findViewById(R.id.altPhoneNumber);
        instCharges = (EditText)findViewById(R.id.instCharges);
        stbCharges = (EditText)findViewById(R.id.stbCharges);
        custType = (RadioGroup)findViewById(R.id.customerTypeRadio);
        altPhoneNumber = (EditText) findViewById(R.id.altPhoneNumber);
        localStore = new LocalStore(ECAFNewCustomerActivity.this);
        addChildSTB = (Button)findViewById(R.id.addChildSTB);
        stateSP = (Spinner)findViewById(R.id.stateSP);
        stateList = new ArrayList<String>();
        cityList = new ArrayList<String>();
        locationList = new ArrayList<String>();
        BuildingList = new ArrayList<String>();
        stateIds = new ArrayList<String>();
        cityIds = new ArrayList<String>();
        locationId = new ArrayList<String>();
        buildingId = new ArrayList<String>();
        getSupportActionBar().setTitle("New Customer");
        ecafCustomer = new ECAFCustomer();
        new FetchState().execute();
        Values.setAddressProofUrl="";
        Values.setCustomerPhotoUrl="";
        Values.setIdProofUrl="";
        Values.setSignUrl="";
        progressDialog = new ProgressDialog(ECAFNewCustomerActivity.this);
        nextButton=(Button)findViewById(R.id.nextButton);
        stateAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,android.R.id.text1);

        cityAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,android.R.id.text1);

        locAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,android.R.id.text1);

        buildingAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,android.R.id.text1);

        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        buildingAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stateSP.setAdapter(stateAdapter);
        city.setAdapter(cityAdapter);
        location.setAdapter(locAdapter);
        building.setAdapter(buildingAdapter);
        addChildSTB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Values.VISIBILITY_COUNTER==0){
                    Values.VISIBILITY_COUNTER=1;
                    addChildSTB.setText("Add Second Child STB");
                    childSTBCARD1TR.setVisibility(View.VISIBLE);
                    childSTBNO1TR.setVisibility(View.VISIBLE);
                }else if(Values.VISIBILITY_COUNTER==1){
                    Values.VISIBILITY_COUNTER=2;
                    addChildSTB.setText("Click to remove both");
                    childSTBCARD2TR.setVisibility(View.VISIBLE);
                    childSTBNO2TR.setVisibility(View.VISIBLE);
                }else if(Values.VISIBILITY_COUNTER==2){
                    Values.VISIBILITY_COUNTER=0;
                    addChildSTB.setText("Add Child STB");
                    childSTBCARD1TR.setVisibility(View.GONE);
                    childSTBNO1TR.setVisibility(View.GONE);
                    childSTBCARD2TR.setVisibility(View.GONE);
                    childSTBNO2TR.setVisibility(View.GONE);

                }
            }
        });
        stateSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Values.STATE_ID = stateIds.get(position);
                stateId =stateIds.get(position);
                new FetchCity().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Values.CITY_ID = cityIds.get(position);
                Values.STATE_ID = "";
                cityId = cityIds.get(position);
                new FetchLocation().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Values.LOC_ID = locationId.get(position);
                Values.STATE_ID = "";
                Values.CITY_ID = "";
                locid = locationId.get(position);
                new FetchBuilding().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        building.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Values.BUILDING_ID = buildingId.get(position);
                bldgid = buildingId.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        custType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                pos=custType.indexOfChild(findViewById(checkedId));
                switch(pos){
                    case 0:
                        custTypeRG="R";
                        break;
                    case 1:
                        custTypeRG="C";
                        break;
                }
            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ecafCustomer = new ECAFCustomer();
                ecafCustomer.setIn_subsid("0");

                if(Values.SELF_CODE.equalsIgnoreCase("Y")){
                    ecafCustomer.setIn_subscd(cardNumber.getText().toString());
                }else{
                    ecafCustomer.setIn_subscd("");
                }
                if(stbNumber.getText().toString().equalsIgnoreCase("")||cardNumber.getText().toString().equalsIgnoreCase("")){
                    Toast.makeText(ECAFNewCustomerActivity.this, "Please select all fields", Toast.LENGTH_SHORT).show();
                }else {
                    ecafCustomer.setIn_subsname(custName.getText().toString());
                    ecafCustomer.setIn_address1(custAddress.getText().toString());
                    ecafCustomer.setIn_email(emailId.getText().toString());
                    ecafCustomer.setIn_stid(stateId);
                    ecafCustomer.setIn_ctcd(cityId);
                    ecafCustomer.setIn_pin(pinCode.getText().toString());
                    ecafCustomer.setIn_road(street.getText().toString());
                    ecafCustomer.setIn_locid(locid);
                    ecafCustomer.setIn_bldgid(bldgid);
                    ecafCustomer.setIn_flatno(flatNo.getText().toString());
                    ecafCustomer.setIn_phone1(phoneNumber.getText().toString());
                    ecafCustomer.setIn_phone2(altPhoneNumber.getText().toString());
                    ecafCustomer.setIn_STBNo(stbNumber.getText().toString());
                    ecafCustomer.setIn_Smartcard(cardNumber.getText().toString());
                    ecafCustomer.setIn_connamt(instCharges.getText().toString());
                    ecafCustomer.setIn_substypeRC(custTypeRG);
                    ecafCustomer.setStbCharges(stbCharges.getText().toString());
                    Values.ECAF_CUSTOMER = ecafCustomer;
                    Values.MODULE = "KYCI";
                    Values.redirect = "";
                    Intent intent = new Intent(ECAFNewCustomerActivity.this, DocumentActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    /**
     * Background task to call for list of states
     */

    private class FetchState extends AsyncTask<String,Void,JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();


        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                if(stateAdapter!=null){
                    stateAdapter.clear();
                }
                url = new URL(Constants.GET_TYPE_DETAILS+"Type=ST"+"&Username="+localStore.getUserDetails().getUserName()+
                        "&Token="+localStore.getUserDetails().getToken()+"&lcoid="+Values.selectedLcoId+
                        "&zoneid="+Values.zoneId+"&areaid="+Values.areaId+"&buildingid="+Values.buildingId+"&stateid="+Values.STATE_ID+
                        "&ctid="+Values.CITY_ID);
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        for(int i = 0; i <dataArray.length(); i++){
                            final JSONObject obj = dataArray.getJSONObject(i);

                            try {
                                stateList.add(obj.getString("StateName"));
                                stateIds.add(obj.getString("StateId"));
                                stateAdapter.add(obj.getString("StateName"));
                                stateAdapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        stateAdapter.notifyDataSetChanged();
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ECAFNewCustomerActivity.this, "Cannot fetch state, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ECAFNewCustomerActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
            }
        }
    }

    /**
     * Background task to call for list of city based on state selected
     */
    private class FetchCity extends AsyncTask<String,Void,JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();


        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(cityAdapter!=null){
                            cityAdapter.clear();
                        }
                    }
                });
                url = new URL(Constants.GET_TYPE_DETAILS+"Type=CT"+"&Username="+localStore.getUserDetails().getUserName()+
                        "&Token="+localStore.getUserDetails().getToken()+"&lcoid="+Values.selectedLcoId+
                        "&zoneid="+Values.zoneId+"&areaid="+Values.areaId+"&buildingid="+Values.buildingId+"&stateid="+Values.STATE_ID+
                        "&ctid="+Values.CITY_ID);
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        for(int i = 0 ; i<dataArray.length(); i++){

                            final JSONObject obj = dataArray.getJSONObject(i);
                            try {
                                cityList.add(obj.getString("CityName"));
                                cityIds.add(obj.getString("CityId"));
                                cityAdapter.add(obj.getString("CityName"));
                                cityAdapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        cityAdapter.notifyDataSetChanged();
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ECAFNewCustomerActivity.this, "Cannot fetch state, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ECAFNewCustomerActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Fetch City ",ex.toString());
            }
        }
    }

    /**
     * Background task to call for list of area/location based on selected city
     */
    private class FetchLocation extends AsyncTask<String,Void,JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();


        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(locAdapter!=null){
                            locAdapter.clear();
                        }

                    }
                });
                url = new URL(Constants.GET_TYPE_DETAILS+"Type=LOC"+"&Username="+localStore.getUserDetails().getUserName()+
                        "&Token="+localStore.getUserDetails().getToken()+"&lcoid="+Values.selectedLcoId+
                        "&zoneid="+Values.zoneId+"&areaid="+Values.areaId+"&buildingid="+Values.buildingId+"&stateid="+Values.STATE_ID+
                        "&ctid="+Values.CITY_ID);
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        for(int i = 0; i<dataArray.length(); i++){

                            final JSONObject obj = dataArray.getJSONObject(i);
                            try {
                                locationList.add(obj.getString("AreaName"));
                                locationId.add(obj.getString("AreaId"));
                                locAdapter.add(obj.getString("AreaName"));
                                locAdapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        locAdapter.notifyDataSetChanged();
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ECAFNewCustomerActivity.this, "Cannot fetch state, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ECAFNewCustomerActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
            }
        }
    }

    /**
     * This background task is called at the end to fetch building based on state, city and location selected by the user
     */

    private class FetchBuilding extends AsyncTask<String,Void,JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();



        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(buildingAdapter!=null){
                            buildingAdapter.clear();
                        }

                    }
                });
                url = new URL(Constants.GET_TYPE_DETAILS+"Type=BLOC"+"&Username="+localStore.getUserDetails().getUserName()+
                        "&Token="+localStore.getUserDetails().getToken()+"&lcoid="+Values.selectedLcoId+
                        "&zoneid="+Values.zoneId+"&areaid="+Values.areaId+"&buildingid="+Values.buildingId+"&stateid="+Values.STATE_ID+
                        "&ctid="+Values.CITY_ID);
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        for(int i = 0; i < dataArray.length(); i++){
                            final JSONObject obj = dataArray.getJSONObject(i);

                            try {
                                BuildingList.add(obj.getString("BldgName"));
                                buildingId.add(obj.getString("BldgId"));
                                buildingAdapter.add(obj.getString("BldgName"));
                                buildingAdapter.notifyDataSetChanged();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        buildingAdapter.notifyDataSetChanged();
                        break;
                    case "1":
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ECAFNewCustomerActivity.this, "Cannot fetch state, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ECAFNewCustomerActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
            }
        }
    }

}
