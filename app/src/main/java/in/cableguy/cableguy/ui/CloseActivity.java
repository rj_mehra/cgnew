package in.cableguy.cableguy.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import in.cableguy.cableguy.R;

/**
 * @author Raj Mehra
 * The type Close activity.
 *
 */
public class CloseActivity extends AppCompatActivity {
    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_close);
        mInterstitialAd = new InterstitialAd(CloseActivity.this);
        mInterstitialAd.setAdUnitId("ca-app-pub-4954480297678755/8989153414");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            if(mInterstitialAd.isLoaded()){
                mInterstitialAd.show();
            }else{
                Log.d("TAG", "The interstitial wasn't loaded yet.");
            }

        }

        /**
         * For automatically closing the activity
         * @code
                new Thread(new Runnable() {
                @Override
                public void run() {
                try{
                Thread.sleep(2000);
                finish();
                }catch (Exception ex){
                Log.e("Exception ",ex.toString());
                }
                }
                }).start();
         *
         */

        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(2000);
                    finish();
                }catch (Exception ex){
                    Log.e("Exception ",ex.toString());
                }
            }
        }).start();

    }


    @Override
    protected void onPause() {
        super.onPause();

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            if(mInterstitialAd.isLoaded()){
                mInterstitialAd.show();
            }else{
                Log.d("TAG", "The interstitial wasn't loaded yet.");
            }

        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            if(mInterstitialAd.isLoaded()){
                mInterstitialAd.show();
            }else{
                Log.d("TAG", "The interstitial wasn't loaded yet.");
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            if(mInterstitialAd.isLoaded()){
                mInterstitialAd.show();
            }else{
                Log.d("TAG", "The interstitial wasn't loaded yet.");
            }

        }
    }
}
