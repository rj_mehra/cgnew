package in.cableguy.cableguy.ui;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import in.cableguy.cableguy.BuildConfig;
import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Constants;
import in.cableguy.cableguy.constants.URLs;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.Customer;
import in.cableguy.cableguy.helper.ECAFCustomer;
import in.cableguy.cableguy.helper.LocalStore;
import in.cableguy.cableguy.helper.TrackGPS;
import in.cableguy.cableguy.helper.Util;

/**
 * @author Raj Mehra
 * The type Document activity.
 *
 * This activity enables to collect document pictures, address,id and sign proof.
 */
public class DocumentActivity extends AppCompatActivity {
    /**
     * The Id proof.
     */
    ImageView idProof, /**
     * The Add proof.
     */
    addProof, /**
     * The Cust photo.
     */
    custPhoto, /**
     * The Sign.
     */
    sign;
    /**
     * The Id proof image name.
     */
    static String idProofImageName, /**
     * The Add proof image name.
     */
    addProofImageName, /**
     * The Cust photo image name.
     */
    custPhotoImageName, /**
     * The Sign image name.
     */
    signImageName;
    /**
     * The Sign file uri.
     */
    static Uri signFileUri;
    /**
     * The This activity.
     */
    Activity thisActivity;
    /**
     * The Transfer utility.
     */
    TransferUtility transferUtility;
    private String customerPhotoUrl,idProofUrl,addressProofUrl,signUrl;
    private static final int CAMERA_CAPTURE_ID_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_ADDR_IMAGE_REQUEST_CODE = 200;
    private static final int CAMERA_CAPTURE_PHOTO_IMAGE_REQUEST_CODE = 300;
    /**
     * The constant MEDIA_TYPE_ID_IMAGE.
     */
    public static final int MEDIA_TYPE_ID_IMAGE = 1;
    /**
     * The constant MEDIA_TYPE_ADDR_IMAGE.
     */
    public static final int MEDIA_TYPE_ADDR_IMAGE = 2;
    /**
     * The constant MEDIA_TYPE_PHOTO_IMAGE.
     */
    public static final int MEDIA_TYPE_PHOTO_IMAGE = 3;
    /**
     * The File uri.
     */
    public Uri fileUri;
    /**
     * The Folder.
     */
    static File folder;
    /**
     * The Id proof image file.
     */
    static File idProofImageFile, /**
     * The Add proof image file.
     */
    addProofImageFile, /**
     * The Cust photo image file.
     */
    custPhotoImageFile, /**
     * The Sign image file.
     */
    signImageFile;
    private final static int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    /**
     * The Next button.
     */
    Button nextButton;
    /**
     * The Customer.
     */
    static Customer customer;
    /**
     * The Local store.
     */
    LocalStore localStore;
    private TrackGPS gps;
    /**
     * The Longitude.
     */
    double longitude;
    /**
     * The Progress dialog.
     */
    ProgressDialog progressDialog;
    /**
     * The Latitude.
     */
    double latitude;
    /**
     * The Ecaf customer.
     */
    ECAFCustomer ecafCustomer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);
        getSupportActionBar().setTitle("Documents");
        nextButton = (Button)findViewById(R.id.nextButton);
        localStore = new LocalStore(this);
        ecafCustomer = new ECAFCustomer();
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("in ","kyc transaction");
                if(Values.addProofImageUri!=null && Values.custImageUri!=null &&Values.idProofImageUri!=null
                        &&Values.signImageUri!=null) {
                    Values.setCustomerPhotoUrl = URLs.IMAGE_URL + Values.custImageName;
                    Values.setIdProofUrl = URLs.IMAGE_URL + Values.idProofImageName;
                    Values.setSignUrl = URLs.IMAGE_URL + Values.signImageName;
                    Values.setAddressProofUrl = URLs.IMAGE_URL + Values.addProofImageName;
                    Log.e("USER NAME ", localStore.getUserDetails().getUserName());
                    beginUpload(Values.addProofImageFile);
                    beginUpload(Values.custImageFile);
                    Log.e("sign proof",Values.signImageFile);
                    beginUpload(Values.signImageFile);
                    beginUpload(Values.idProofImageFile);
                    gps = new TrackGPS(DocumentActivity.this);


                    if(gps.canGetLocation()){


                        longitude = gps.getLongitude();
                        latitude = gps.getLatitude();

                        Values.GeoLocation=longitude+","+latitude;

                        Values.ECAF_CUSTOMER.setIn_photoproof(Values.setCustomerPhotoUrl);
                        Values.ECAF_CUSTOMER.setIn_idproof(Values.setIdProofUrl);
                        Values.ECAF_CUSTOMER.setIn_sign(Values.setSignUrl);
                        Values.ECAF_CUSTOMER.setIn_addressproof(Values.setAddressProofUrl);
                        if(Values.MODULE.equalsIgnoreCase("KYCE")){
                            new UpdateEcafCustomer().execute();
                        }else if(Values.MODULE.equalsIgnoreCase("KYCI")){
                            Intent intent = new Intent(DocumentActivity.this,PlanActivity.class);
                            startActivity(intent);

                        }else{
                            Intent intent = new Intent(DocumentActivity.this,PlanActivity.class);
                            startActivity(intent);
                        }
                        Log.e("Location ",Values.GeoLocation);
                    }
                    else
                    {

                        gps.showSettingsAlert();
                    }
                    //KYC_Trans();
                }
                else {
                    Toast.makeText(thisActivity, "Please Capture KYC Docs", Toast.LENGTH_SHORT).show();
                }
            }
        });
        idProof = (ImageView) findViewById(R.id.idProof);
        addProof = (ImageView) findViewById(R.id.addProof);
        custPhoto = (ImageView) findViewById(R.id.custPhoto);
        sign = (ImageView) findViewById(R.id.sign);
        thisActivity = DocumentActivity.this;
        sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(thisActivity, SignatureSaveActivity.class);
                startActivity(i);
                finish();
            }
        });

        idProof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(thisActivity,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(thisActivity,
                                new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST_CAMERA);
                    } else {
                        captureImage(MEDIA_TYPE_ID_IMAGE);
                    }
                } else {
                    captureImage(MEDIA_TYPE_ID_IMAGE);
                }

            }
        });

        addProof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(thisActivity,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(thisActivity,
                                new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);
                    } else {
                        captureImage(MEDIA_TYPE_ADDR_IMAGE);
                    }
                } else {
                    captureImage(MEDIA_TYPE_ADDR_IMAGE);
                }
            }
        });
        custPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
                if (currentAPIVersion >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(thisActivity,
                            Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(thisActivity,
                                new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);
                    } else {
                        captureImage(MEDIA_TYPE_PHOTO_IMAGE);
                    }
                } else {
                    captureImage(MEDIA_TYPE_PHOTO_IMAGE);
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(Values.signImageUri!=null){
            Glide.with(thisActivity).load(signFileUri).apply(new RequestOptions().fitCenter().placeholder(R.drawable.noimage)).transition(new DrawableTransitionOptions().crossFade())
                    .into(sign);
        }
        if(Values.idProofImageUri!=null){
            Glide.with(thisActivity).load(Values.idProofImageUri).apply(new RequestOptions().fitCenter().placeholder(R.drawable.noimage)).transition(new DrawableTransitionOptions().crossFade())
                    .into(idProof);
        }
        if(Values.addProofImageUri!=null){
            Glide.with(thisActivity).load(Values.fileUri).apply(new RequestOptions().fitCenter().placeholder(R.drawable.noimage)).transition(new DrawableTransitionOptions().crossFade())
                    .into(addProof);
        }
        if(Values.custImageUri!=null){
            Glide.with(thisActivity).load(Values.fileUri).apply(new RequestOptions().fitCenter().placeholder(R.drawable.noimage)).transition(new DrawableTransitionOptions().crossFade())
                    .into(custPhoto);
        }
        transferUtility = Util.getTransferUtility(this);
        /*AmazonS3 amazonS3=new AmazonS3Client()
        transferUtility=new TransferUtility()*/
    }


    private void captureImage(int type) {

        Intent intent = null;

        intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        //null issue
        fileUri = getOutputMediaFileUri(type);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        Values.fileUri = fileUri;
        Log.e("fileURI", fileUri.toString());
        Log.e("fileURI", fileUri.getPath());
        if (type == MEDIA_TYPE_ID_IMAGE) {
            startActivityForResult(intent, CAMERA_CAPTURE_ID_IMAGE_REQUEST_CODE);
        } else if (type == MEDIA_TYPE_ADDR_IMAGE) {
            startActivityForResult(intent, CAMERA_CAPTURE_ADDR_IMAGE_REQUEST_CODE);
        } else if (type == MEDIA_TYPE_PHOTO_IMAGE) {
            startActivityForResult(intent, CAMERA_CAPTURE_PHOTO_IMAGE_REQUEST_CODE);
        }
    }

    /**
     * Gets output media file uri.
     *
     * @param type the type
     * @return the output media file uri
     */
    public Uri getOutputMediaFileUri(int type) {
        int currentAPIVersion = android.os.Build.VERSION.SDK_INT;
        Uri photoURI=null;
        File pictureFile=getOutputMediaFile(type);
        if (currentAPIVersion >= Build.VERSION_CODES.N) {
            photoURI= FileProvider.getUriForFile(thisActivity,
                    BuildConfig.APPLICATION_ID + ".provider",
                    pictureFile);
        } else {
            photoURI=Uri.fromFile(pictureFile);
        }
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),photoURI);
            FileOutputStream fos = null;
            fos = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return photoURI;

    }


    private static File getOutputMediaFile(int type) {

        // External sdcard location
        folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/CG" + "/UploadedImages/");
        try{
            File output = new File(folder,".nomedia");
        }catch (Exception ex){
            Log.e(".nomedia ",ex.toString());
        }
        if (!folder.exists()) {
            folder.mkdirs();
        }
        // Create the storage directory if it does not exist
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_ID_IMAGE) {
            idProofImageName =Values.ECAF_CUSTOMER.getIn_subsname()+"_idproof_"
                    + timeStamp + ".png";
            Values.idProofImageName=idProofImageName;
            Values.idProofImageFile=folder.getPath() + File.separator + idProofImageName;
            mediaFile = new File(Values.idProofImageFile);
            idProofImageFile = mediaFile;
            //replace 1234 with Values.user.getUserId();
        } else if (type == MEDIA_TYPE_ADDR_IMAGE) {
            addProofImageName = Values.ECAF_CUSTOMER.getIn_subsname()+"_addproof_"
                    + timeStamp + ".png";
            Values.addProofImageName=addProofImageName;
            Values.addProofImageFile=folder.getPath() + File.separator + addProofImageName;
            mediaFile = new File(Values.addProofImageFile);
            addProofImageFile = mediaFile;
        } else if (type == MEDIA_TYPE_PHOTO_IMAGE) {
            custPhotoImageName = Values.ECAF_CUSTOMER.getIn_subsname()+"_custphoto_"
                    + timeStamp + ".png";
            Values.custImageName=custPhotoImageName;
            Values.custImageFile=folder.getPath() + File.separator + custPhotoImageName;
            mediaFile = new File(Values.custImageFile);
            custPhotoImageFile = mediaFile;
        } else {
            return null;
        }

        return mediaFile;
    }



    //Changes for galaxy s4

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_CAPTURE_ID_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                if (fileUri != null) {
                    //addBillImageView.setImageBitmap(decodeSampledBitmapFromUri(fileUri.getPath(), 150, 150));
                    Glide.with(thisActivity).load(Values.fileUri).apply(new RequestOptions().fitCenter().placeholder(R.drawable.noimage)).transition(new DrawableTransitionOptions().crossFade())
                            .into(idProof);
                    Values.idProofImageUri=Values.fileUri;
                    toFile(decodeFile(idProofImageFile),idProofImageFile);
                    Toast.makeText(getApplicationContext(),
                            "Image successfully captured", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == CAMERA_CAPTURE_ADDR_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                if (fileUri != null) {
                    //addwarrImageView.setImageBitmap(decodeSampledBitmapFromUri(fileUri.getPath(), 150, 150));
                        /*Glide.with(thisActivity).load(Values.fileUri).fitCenter().placeholder(R.drawable.load).crossFade()
                                .error(R.drawable.error)
                                .into(warrantyImage);*/
                    Glide.with(thisActivity).load(Values.fileUri).apply(new RequestOptions().fitCenter().placeholder(R.drawable.noimage)).transition(new DrawableTransitionOptions().crossFade())
                            .into(addProof);
                    Values.addProofImageUri=Values.fileUri;
                    toFile(decodeFile(addProofImageFile),addProofImageFile);
                    Toast.makeText(getApplicationContext(),
                            "Image successfully captured", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == CAMERA_CAPTURE_PHOTO_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image
                // display it in image view
                if (fileUri != null) {
                    //deviceImageView.setImageBitmap(decodeSampledBitmapFromUri(fileUri.getPath(), 150, 150));
                    Glide.with(thisActivity).load(Values.fileUri).apply(new RequestOptions().fitCenter().placeholder(R.drawable.noimage)).transition(new DrawableTransitionOptions().crossFade())
                            .into(custPhoto);
                    Values.custImageUri=Values.fileUri;
                    toFile(decodeFile(custPhotoImageFile),custPhotoImageFile);
                    Toast.makeText(getApplicationContext(),
                            "Image successfully captured", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();

                }
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }

    }

    /**
     * To file file.
     *
     * @param bitmap the bitmap
     * @param file   the file
     * @return the file
     */
    public File toFile(Bitmap bitmap,File file){
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Values.setAddressProofUrl="";
        Values.setCustomerPhotoUrl="";
        Values.setIdProofUrl="";
        Values.setSignUrl="";
    }

    // Decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to --70
            final int REQUIRED_SIZE=200;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }


    /**
     * Decode sampled bitmap from uri bitmap.
     *
     * @param path      the path
     * @param reqWidth  the req width
     * @param reqHeight the req height
     * @return the bitmap
     */
    public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth,
                                             int reqHeight) {
        Bitmap bm = null;
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);

        return bm;
    }

    /**
     * Calculate in sample size int.
     *
     * @param options   the options
     * @param reqWidth  the req width
     * @param reqHeight the req height
     * @return the int
     */
    public int calculateInSampleSize(

            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 2;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height
                        / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }

        return inSampleSize;
    }


    /**
     * @param filePath
     */
    private void beginUpload(String filePath) {
        if (filePath == null) {
            Toast.makeText(this, "Could not find the filepath of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(filePath);
        Log.e("Name of Image", file.getName());
        TransferObserver observer = transferUtility.upload(Constants.BUCKET_NAME, file.getName(),
                file);
        /*
         * Note that usually we set the transfer listener after initializing the
         * transfer. However it isn't required in this sample app. The flow is
         * click upload button -> start an activity for image selection
         * startActivityForResult -> onActivityResult -> beginUpload -> onResume
         * -> set listeners to in progress transfers.
         */
        // observer.setTransferListener(new UploadListener());
    }


    private class UpdateEcafCustomer extends AsyncTask<String,Void,JSONObject> {
        /**
         * The Is.
         */
        InputStream is = null;
        /**
         * The Result.
         */
        String result = "";
        /**
         * The Json.
         */
        String json = "";

        /**
         * The Sb.
         */
        StringBuilder sb = new StringBuilder();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(DocumentActivity.this);
            progressDialog.setCancelable(true);
            progressDialog.setMessage("Loading ...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            URL url;
            try{
                String uri = Constants.ECAF_UPDATE_SUBSCRIBER+"username="+localStore.getUserDetails().getUserName()+"&in_subsid=" +
                        Values.ECAF_CUSTOMER.getIn_subsid()+"&in_operid="+Values.selectedLcoId+
                        "&in_phone1="+Values.ECAF_CUSTOMER.getIn_phone1()+"&in_phone2="+Values.ECAF_CUSTOMER.getIn_phone2()
                        +"&in_email="+Values.ECAF_CUSTOMER.getIn_email()+"&in_plan_id="+Values.PLAN_ID+"&in_addressproof="
                        +Values.ECAF_CUSTOMER.getIn_addressproof()+"&in_idproof="+Values.ECAF_CUSTOMER.getIn_idproof()+
                        "&in_photoproof="+Values.ECAF_CUSTOMER.getIn_photoproof()+"&in_sign="+Values.ECAF_CUSTOMER.getIn_sign()+
                        "&in_geolocation="+Values.GeoLocation+"&Token="+localStore.getUserDetails().getToken()+
                        "&in_subsname="+Values.ECAF_CUSTOMER.getIn_subsname()+"&in_subsaddress="+Values.ECAF_CUSTOMER.getIn_address1();
                url = new URL(uri.replaceAll(" ","%20"));

                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                conn.setRequestMethod("GET");
                conn.connect();
                Log.e("conn",":"+conn);
                int statuscode = conn.getResponseCode();
                Log.e("statuscode",""+statuscode);
                Log.e("response",conn.getResponseMessage());
                if(statuscode==200) {
                    is = new BufferedInputStream(conn.getInputStream());
                }else
                    is = new BufferedInputStream(conn.getErrorStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                is.close();

                json = sb.toString();
                //Log.d("JValue","value 1 "+json);

                json=json.replace("\\\"","\"");
                json = json.substring(1,json.length()-1);
                Log.d("JValue","value 2"+json);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jobj = new JSONObject();
            try {
                jobj = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jobj;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            String status;
            JSONArray dataArray;
            try {
                status = jsonObject.getString("STATUS");
                dataArray = jsonObject.getJSONArray("Data");
                switch (status) {
                    case "0":
                        Intent intent = new Intent(DocumentActivity.this, SuccessActivity.class);
                        Values.MODULE="KYCE";
                        startActivity(intent);
                        finish();
                        break;
                    case "1":
                        break;
                    default:
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(DocumentActivity.this, "Cannot connect to Server, Please try again!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }
            }catch (Exception ex){
                Log.e("Change Password ",ex.toString());
            }finally {
                progressDialog.dismiss();
            }
        }
    }


}
