package in.cableguy.cableguy.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import in.cableguy.cableguy.R;
import in.cableguy.cableguy.constants.Values;
import in.cableguy.cableguy.helper.LocalStore;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 *
 * @author Raj Mehra
 * The type Type selection activity.
 *
 * @deprecated No longer in use but kept for future reference or possible usage
 *
 * @since 12 November 2016
 *
 * The redirect to respected screen is handled in Login Activity itself
 * @see LoginActivity
 */
public class TypeSelectionActivity extends AppCompatActivity {
    /**
     * The Building.
     */
    Button building, /**
     * The Normal.
     */
    normal, /**
     * The Lco.
     */
    lco;
    /**
     * The Building ll.
     */
    LinearLayout buildingLL, /**
     * The Normal ll.
     */
    normalLL, /**
     * The Lco ll.
     */
    lcoLL;
    /**
     * The Local store.
     */
    LocalStore localStore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type_selection);
        overridePendingTransition(0, 0);

        buildingLL=(LinearLayout)findViewById(R.id.buildingLL);
        normalLL=(LinearLayout)findViewById(R.id.normalLL);
        lcoLL=(LinearLayout)findViewById(R.id.LCOLL);

        /*building = (Button)findViewById(R.id.buildingButton);
        normal = (Button)findViewById(R.id.normalButton);
        lco = (Button)findViewById(R.id.LCOButton);*/

        localStore=new LocalStore(TypeSelectionActivity.this);
        String menu=localStore.getUserDetails().getMenu();
        Intent intent;
        switch(menu){
            case "B":
                intent = new Intent(TypeSelectionActivity.this,NavDrawerActivity.class);
                startActivity(intent);
                Values.redirect="BuildingType";
                finish();
                break;
            case "N":
                intent  = new Intent(TypeSelectionActivity.this,NavDrawerActivity3.class);
                startActivity(intent);
                Values.redirect = "NormalType";
                Values.buildingId="";
                Values.zoneId="";
                Values.areaId="";
                Values.selectedCustomer=null;
                finish();
                break;
            case "L":
                intent = new Intent(TypeSelectionActivity.this,NavDrawerActivity2.class);
                startActivity(intent);
                Values.redirect = "LCOType";
                Values.buildingId="";
                Values.zoneId="";
                Values.areaId="";
                Values.selectedCustomer=null;
                finish();
                break;
            default:
                Toast.makeText(this, "There is some error, please contact support", Toast.LENGTH_SHORT).show();
                break;
        }


        getSupportActionBar().setTitle("Select Type");
        buildingLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TypeSelectionActivity.this,NavDrawerActivity.class);
                startActivity(intent);
            }
        });

        normalLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {/*
                Intent intent  = new Intent(TypeSelectionActivity.this,SearchActivity.class);*/
                Intent intent  = new Intent(TypeSelectionActivity.this,NavDrawerActivity3.class);
                startActivity(intent);
            }
        });

        lcoLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TypeSelectionActivity.this,NavDrawerActivity2.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
