package in.cableguy.cableguy.btprinter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Set;
import java.util.UUID;

import in.cableguy.cableguy.constants.Values;

/**
 * The type Bt printer.
 */
public class BTPrinter {
    private static final String UUID_STR = "00001101-0000-1000-8000-00805F9B34FB";
    /**
     * The constant bluetoothSocket.
     */
    protected static BluetoothSocket bluetoothSocket;
    /**
     * The constant device.
     */
    protected static BluetoothDevice device;
    /**
     * The constant isBTConnected.
     */
    public static boolean isBTConnected;
    /**
     * The constant mBluetoothAdapter.
     */
    protected static BluetoothAdapter mBluetoothAdapter;
    /**
     * The constant m_PrinterType.
     */
    public static char m_PrinterType;
    /**
     * The Is bt found.
     */
    protected boolean IsBTFound;
    private boolean IsEnquiryCompleted;
    private boolean available;
    /**
     * The Ct.
     */
    int ct;
    /**
     * The Image height.
     */
    int image_height;
    /**
     * The Image width.
     */
    int image_width;
    /**
     * The Input stream.
     */
    public InputStream inputStream = null;
    /**
     * The constant evoluteInputStream.
     */
    public static InputStream evoluteInputStream = null;
    /**
     * The constant evoluteOutputStream.
     */
    public static OutputStream evoluteOutputStream = null;
    /**
     * The K.
     */
    int k;
    /**
     * The M bt address.
     */
    protected String m_BTAddress;
    /**
     * The M err code.
     */
    public int m_ErrCode;
    /**
     * The M err string.
     */
    public String m_ErrString;
    private Context mcontext;
    /**
     * The No bytes.
     */
    int noBytes;
    /**
     * The Nof byts 1.
     */
    int nofByts1;
    /**
     * The Output stream.
     */
    public OutputStream outputStream = null;
    private int packetLength;
    /**
     * The Prn raster img.
     */
    int[] prnRasterImg;
    /**
     * The Response byte.
     */
    int responseByte;
    private int[] tempPacket;
    private boolean timeoutFlag;
    /**
     * The Tot data len.
     */
    int totDataLen;
    /**
     * The Context.
     */
    Context context;

    /**
     * The type Zero printer.
     */
    class ZeroPrinter {
        /**
         * The Actual packet.
         */
        int[] actualPacket;
        /**
         * The M err code.
         */
        public int m_ErrCode;
        /**
         * The M err string.
         */
        public String m_ErrString;
        /**
         * The No bytes.
         */
        int noBytes;
        /**
         * The No of bar code bytes.
         */
        int noOfBarCodeBytes;
        /**
         * The Packet buffer.
         */
        int[] packetBuffer;
        /**
         * The Print bar code data.
         */
        int[] printBarCodeData;

        /**
         * Instantiates a new Zero printer.
         */
        ZeroPrinter() {
            this.packetBuffer = new int[2000];
            this.printBarCodeData = new int[125];
            this.noBytes = 0;
            this.noOfBarCodeBytes = 0;
            this.actualPacket = new int[125];
            this.m_ErrString = null;
            this.m_ErrCode = 0;
        }

        /**
         * Zero printer.
         */
        public void ZeroPrinter() {
            this.noBytes = 0;
        }

        /**
         * Add printer data.
         *
         * @param data the data
         * @param Font the font
         * @param flag the flag
         */
        public void addPrinterData(String data, int Font, boolean flag) {
            int charNo;
            int i;
            System.out.println("In addPrinterData=============");
            int col = 0;
            if (Font == 241 || Font == 240) {
                charNo = 24;
            } else {
                charNo = 42;
            }
            char[] extraSpaceBuffer = new char[(charNo - data.length())];
            char[] tempBuffer = new char[charNo];
            if (data.length() < charNo) {
                col = 0;
                if (flag) {
                    col = (charNo - data.length()) / 2;
                    if (col < 0) {
                        col = 0;
                    }
                    for (i = 0; i <= col; i++) {
                        tempBuffer[i] = ' ';
                    }
                }
                arrayFill(extraSpaceBuffer, charNo - (data.length() + col));
            }
            data.getChars(0, data.length(), tempBuffer, col);
            System.arraycopy(extraSpaceBuffer, 0, tempBuffer, data.length() + col, charNo - (data.length() + col));
            int[] iArr = this.packetBuffer;
            int i2 = this.noBytes;
            this.noBytes = i2 + 1;
            iArr[i2] = Font;
            i = 1;
            int j = 0;
            while (j < charNo) {
                iArr = this.packetBuffer;
                i2 = this.noBytes;
                this.noBytes = i2 + 1;
                iArr[i2] = tempBuffer[j];
                j++;
                i++;
            }
        }

        /**
         * Array fill.
         *
         * @param buffer   the buffer
         * @param boundary the boundary
         */
        public void arrayFill(char[] buffer, int boundary) {
            for (int i = 0; i < boundary; i++) {
                buffer[i] = ' ';
            }
        }

        /**
         * Gets data.
         *
         * @param packet the packet
         * @return the data
         */
        public int getData(int[] packet) {
            System.arraycopy(this.packetBuffer, 0, packet, 0, this.noBytes);
            return this.noBytes;
        }

        /**
         * Gets bar code data.
         *
         * @param packet the packet
         * @return the bar code data
         */
        public int getBarCodeData(int[] packet) {
            System.arraycopy(this.printBarCodeData, 0, packet, 0, this.noOfBarCodeBytes);
            return this.noOfBarCodeBytes;
        }

        /**
         * Print bar code int.
         *
         * @param barCodeData     the bar code data
         * @param iBarCodeDataLen the bar code data len
         * @param BarCodeType     the bar code type
         * @return the int
         */
        public int iPrintBarCode(String barCodeData, int iBarCodeDataLen, int BarCodeType) {
            char[] charTempBuffer = new char[barCodeData.length()];
            int Lrc = 0;
            barCodeData.getChars(0, barCodeData.length(), charTempBuffer, 0);
            if (((BarCodeType == 184 || BarCodeType == 182) && iBarCodeDataLen > 27) || iBarCodeDataLen <= 1) {
                return 97;
            }
            if (((BarCodeType == 185 || BarCodeType == 183) && iBarCodeDataLen > 12) || iValidateBarCodeData(barCodeData, iBarCodeDataLen, BarCodeType) == 1) {
                return 97;
            }
            int noBytes = 0 + 1;
            this.printBarCodeData[0] = 126;
            int i = noBytes + 1;
            this.printBarCodeData[noBytes] = BarCodeType;
            int j = 0;
            while (j < barCodeData.length()) {
                noBytes = i + 1;
                this.printBarCodeData[i] = charTempBuffer[j];
                j++;
                i = noBytes;
            }
            noBytes = i + 1;
            this.printBarCodeData[i] = 4;
            for (int i2 = 1; i2 <= barCodeData.length() + 2; i2++) {
                Lrc ^= this.printBarCodeData[i2];
            }
            this.printBarCodeData[noBytes] = Lrc;
            this.noOfBarCodeBytes = noBytes + 1;
            return 0;
        }

        /**
         * Validate bar code data int.
         *
         * @param ucBarcodeData   the uc barcode data
         * @param iBarCodeDataLen the bar code data len
         * @param ucBarCodeType   the uc bar code type
         * @return the int
         */
        public int iValidateBarCodeData(String ucBarcodeData, int iBarCodeDataLen, int ucBarCodeType) {
            String SpecialChars = new String("-. $/+");
            char[] ThreeOfNineSpecialChars = new char[10];
            char[] charBarCode = new char[ucBarcodeData.length()];
            SpecialChars.getChars(0, SpecialChars.length(), ThreeOfNineSpecialChars, 0);
            ucBarcodeData.getChars(0, ucBarcodeData.length(), charBarCode, 0);
            int i;
            if (ucBarCodeType == 182 || ucBarCodeType == 183) {
                i = 0;
                while (i < iBarCodeDataLen) {
                    int j = 0;
                    while (j < ThreeOfNineSpecialChars.length && charBarCode[i] != ThreeOfNineSpecialChars[j]) {
                        j++;
                    }
                    if (j >= ThreeOfNineSpecialChars.length && (charBarCode[i] < 'A' || charBarCode[i] > 'Z')) {
                        if (charBarCode[i] < '0' || charBarCode[i] > '9') {
                            return 1;
                        }
                    }
                    i++;
                }
                return 0;
            } else if (ucBarCodeType != 184 && ucBarCodeType != 185) {
                return 1;
            } else {
                i = 0;
                while (i < iBarCodeDataLen) {
                    if (charBarCode[i] < '0' || charBarCode[i] > '9') {
                        return 1;
                    }
                    i++;
                }
                return 0;
            }
        }

        /**
         * Analyze status.
         *
         * @param status the status
         */
        public void analyzeStatus(int status) {
            switch (status) {
                case 65:
                    System.out.println("Error !!!! PAPER OUT");
                    this.m_ErrCode = -11;
                    this.m_ErrString = "Error !!!! PAPER OUT";
                case 66:
                    System.out.println("Error !!!! PLATEN OPEN");
                    this.m_ErrCode = -2;
                    this.m_ErrString = "Error !!!! PLATEN OPEN";
                case 68:
                    System.out.println("Error !!!! COMMUNICATION FAILED");
                    this.m_ErrCode = -6;
                    this.m_ErrString = "Error !!!! COMMUNICATION FAILED";
                case 72:
                    System.out.println("Error !!!! TEMP HIGH");
                    this.m_ErrCode = -3;
                    this.m_ErrString = "Error !!!! TEMP HIGH";
                case 80:
                    System.out.println("Error !!!! TEMP TOO LOW");
                    this.m_ErrCode = -4;
                    this.m_ErrString = "Error !!!! TEMP TOO LOW";
                case 96:
                    System.out.println("Error !!!! IMPROPER VOLTAGE");
                    this.m_ErrCode = -5;
                    this.m_ErrString = "Error !!!! IMPROPER VOLTAGE";
                case 128:
                    System.out.println("SUCCESS!!!!!!!!!!!!!!");
                    this.m_ErrCode = 0;
                    this.m_ErrString = "SUCCESS!!!!!!!!!!!!!!";
                default:
                    System.out.println("Error !!!! NACK RECEIVED");
                    this.m_ErrCode = -7;
                    this.m_ErrString = "Error !!!! NACK RECEIVED";
            }
        }
    }

    static {
        bluetoothSocket = null;
        isBTConnected = false;
        m_PrinterType = 'A';
    }

    /**
     * Instantiates a new Bt printer.
     *
     * @param context the context
     */
    public BTPrinter(Context context) {
        this.inputStream = null;
        this.outputStream = null;
        this.m_ErrString = "";
        this.m_ErrCode = 0;
        this.m_BTAddress = "";
        this.packetLength = 0;
        this.timeoutFlag = false;
        this.responseByte = 0;
        this.available = false;
        this.noBytes = 0;
        this.ct = 0;
        this.nofByts1 = 0;
        this.k = 0;
        this.image_width = 0;
        this.image_height = 0;
        this.IsEnquiryCompleted = false;
        this.IsBTFound = false;
        this.mcontext = context;
        this.m_ErrString = "";
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
/*            Toast.makeText(this.mcontext, "Bluetooth is not available", 1).show();*/
            Toast.makeText(context, "Bluetooth not available", Toast.LENGTH_SHORT).show();
        } else if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
    }

    /**
     * Open bt connection boolean.
     *
     * @return the boolean
     */
    public boolean OpenBTConnection() {
        String deviceadd = "";
        try {
            if (mBluetoothAdapter == null) {
                this.m_ErrString = "Bluetooth is not available";
                return false;
            } else if (mBluetoothAdapter.isEnabled()) {
                if (!this.IsBTFound) {
                    this.IsEnquiryCompleted = false;
                    this.IsBTFound = false;
                    if (mBluetoothAdapter.isDiscovering()) {
                        mBluetoothAdapter.cancelDiscovery();
                    }
                    mBluetoothAdapter.startDiscovery();
                    Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
                    if (pairedDevices.size() > 0) {
                        for (BluetoothDevice mdevice : pairedDevices) {
                            if (!mdevice.getName().startsWith("ACC") && !mdevice.getName().startsWith("BTACC")) {
                                if (!mdevice.getName().startsWith("ESYS") && !mdevice.getName().startsWith("ESB")) {
                                    if (!mdevice.getName().startsWith("MP") && !mdevice.getName().startsWith("VB") && !mdevice.getName().startsWith("SCRI") && !mdevice.getName().startsWith("ST")) {
                                        if (mdevice.getName().startsWith("BT")||mdevice.getName().startsWith("Blu")) {
                                            m_PrinterType = 'B';
                                            device = mdevice;
                                            SetBTAddress(device.getAddress());
                                            this.IsEnquiryCompleted = true;
                                            mBluetoothAdapter.cancelDiscovery();
                                            break;
                                        }
                                    }
                                    m_PrinterType = 'M';
                                    device = mdevice;
                                    SetBTAddress(device.getAddress());
                                    this.IsEnquiryCompleted = true;
                                    mBluetoothAdapter.cancelDiscovery();
                                    break;
                                }
                                m_PrinterType = 'E';
                                device = mdevice;
                                SetBTAddress(device.getAddress());
                                this.IsEnquiryCompleted = true;
                                Values.PRINTER_TYPE = "E";
                                /*try{
                                    GlobalApp.setup = new Setup();
                                }catch (Exception ex){
                                    Log.e("Error",ex.toString());
                                }*/
                                mBluetoothAdapter.cancelDiscovery();
                                break;
                            }
                            m_PrinterType = 'A';
                            device = mdevice;
                            SetBTAddress(device.getAddress());
                            this.IsEnquiryCompleted = true;
                            mBluetoothAdapter.cancelDiscovery();
                            break;
                        }
                    }
                }
                if (this.IsBTFound) {
                    device = mBluetoothAdapter.getRemoteDevice(GetBTAddress());
                    try {
                        bluetoothSocket = device.createRfcommSocketToServiceRecord(UUID.fromString(UUID_STR));
                        Log.v("bluetoothSocket", ">>> Connecting ");
                        bluetoothSocket.connect();
                        isBTConnected = true;
                        inputStream=bluetoothSocket.getInputStream();
                        outputStream=bluetoothSocket.getOutputStream();
                        Log.e("Btrpinter"," InputStream = "+inputStream);
                        Log.e("BtPrinter","OutputStream = "+outputStream);
                        Log.v("bluetoothSocket", ">>> CONNECTED SUCCESSFULLY");
                        evoluteInputStream=bluetoothSocket.getInputStream();
                        evoluteOutputStream=bluetoothSocket.getOutputStream();
                        /*Printer_GEN ptrGen = new Printer_GEN(GlobalApp.setup, outputStream, inputStream);
                        int iret = ptrGen.iTestPrint();
                        Log.d("ESYS SDK..", "iret : "+iret);*/
                        return true;
                    } catch (Exception e) {
                        StringWriter stackTrace = new StringWriter();
                        e.printStackTrace(new PrintWriter(stackTrace));
                        System.err.println(stackTrace);
                        this.m_ErrString = "Error while connecting Printer \n" + e.getMessage();
                        this.m_ErrCode = -2;
                        isBTConnected = false;
                        return false;
                    }
                }
                this.m_ErrString = new StringBuilder(String.valueOf(0)).append(" Printer not found").toString();
                this.m_ErrCode = -1;
                isBTConnected = false;
                return false;
            } else {
                this.m_ErrString = "Bluetooth is off. \nPlz switch on bluetooth. ";
                return false;
            }
        } catch (Exception e2) {
            this.m_ErrString = "Error Connecting Printer \n" + e2.getMessage();
            this.m_ErrCode = -1;
            isBTConnected = false;
            return false;
        }
    }

    /**
     * Set bt address.
     *
     * @param m_Address the m address
     */
    public void SetBTAddress(String m_Address) {
        this.IsBTFound = true;
        this.m_BTAddress = m_Address;
    }

    /**
     * Get bt address string.
     *
     * @return the string
     */
    public String GetBTAddress() {
        return this.m_BTAddress;
    }

    /**
     * Close bt connection.
     */
    public void CloseBTConnection() {
        if (isBTConnected) {
            try {
                isBTConnected = false;
                if (this.outputStream != null) {
                    this.outputStream.close();
                }
                if (this.inputStream != null) {
                    this.inputStream.close();
                }
                bluetoothSocket.close();
                mBluetoothAdapter.disable();
            } catch (IOException e) {
                this.m_ErrString = "Error Connecting Printer \n" + e.getMessage();
                this.m_ErrCode = -1;
                isBTConnected = false;
            }
        }
    }

    /**
     * Start print boolean.
     *
     * @return the boolean
     */
    public boolean StartPrint() {
        if (isBTConnected) {
            this.m_ErrCode = 0;
            return true;
        }
        this.m_ErrString = " Printer not found";
        this.m_ErrCode = -1;
        return false;
    }

    /**
     * Print line boolean.
     *
     * @param str        the str
     * @param m_FontType the m font type
     * @return the boolean
     */
    public boolean PrintLine(String str, char m_FontType) {
        if (!isBTConnected) {
            this.m_ErrString = "Printer not connected";
            this.m_ErrCode = -1;
            return false;
        } else if (m_PrinterType == 'M') {
            return PrintTextM(str, m_FontType);
        } else {
            if (m_PrinterType == 'B') {
                return PrintTextB(str, m_FontType);
            }
            return PrintText(str, m_FontType);
        }
    }

    private boolean PrintText(String data1, char m_FontType) {
        try {
            String data;
            ZeroPrinter printer = new ZeroPrinter();
            int[] dataPacket = new int[2000];
            int m_font = 242;
            if (data1.length() > 41) {
                data = data1.substring(0, 40);
            } else {
                data = data1;
            }
            switch (m_FontType) {
                case 'B':
                    m_font = 240;
                    break;
                case 'L':
                    m_font = 241;
                    break;
                case 'N':
                    m_font = 243;
                    break;
                case 'S':
                    m_font = 242;
                    break;
                case 'T':
                    m_font = 242;
                    break;
            }
            if (m_PrinterType == 'E') {
                m_font = 146;
            }
            this.outputStream = bluetoothSocket.getOutputStream();
            this.inputStream = bluetoothSocket.getInputStream();
            printer.addPrinterData(data, m_font, false);
            int status = sendUserData(dataPacket, printer.getData(dataPacket));
            this.outputStream.flush();
            return true;
        } catch (IOException e) {
            this.m_ErrString = e.getMessage();
            this.m_ErrCode = -1;
            isBTConnected = false;
            return false;
        }
    }

    private boolean PrintTextM(String data, char m_FontType) {
        try {
            this.outputStream = bluetoothSocket.getOutputStream();
            this.inputStream = bluetoothSocket.getInputStream();
            byte size = (byte) 0;
            switch (m_FontType) {
                case 'L':
                    size = (byte) 17;
                    break;
                case 'M':
                    size = (byte) 8;
                    break;
                case 'S':
                    size = (byte) 0;
                    break;
                case 'T':
                    size = (byte) 2;
                    break;
            }
            this.outputStream.write(new byte[]{(byte) 27, (byte) 33, size});
            this.outputStream.write((" " + data).getBytes());
            this.outputStream.write(13);
            this.outputStream.flush();
            this.outputStream.write(new byte[]{(byte) 27, (byte) 118});
            this.outputStream.flush();
            int read = this.inputStream.read();
            return true;
        } catch (IOException e) {
            this.m_ErrString = e.getMessage();
            this.m_ErrCode = -1;
            isBTConnected = false;
            return false;
        }
    }

    private int sendUserData(int[] Packet, int length) {
        this.packetLength = 0;
        this.tempPacket = new int[length];
        System.arraycopy(Packet, 0, this.tempPacket, 0, length);
        this.packetLength += length;
        return FilterUserData();
    }

    private int FilterUserData() {
        int[] printerBuffer = new int[2000];
        int j = 0;
        int tempBufCount = 0;
        int Lrc = 0;
        int c = 0;
        int NoOfBytesToPrint = this.packetLength;
        while (NoOfBytesToPrint > 0) {
            int bufSize = 0;
            if (m_PrinterType == 'A') {
                printerBuffer[0] = 126;
                printerBuffer[1] = 178;
            } else if (m_PrinterType == 'E') {
                printerBuffer[0] = 138;
                printerBuffer[1] = 195;
            }
            while (bufSize < 125 && bufSize < NoOfBytesToPrint) {
                if (this.tempPacket[j] == 240 || this.tempPacket[j] == 241) {
                    bufSize += 25;
                    if (bufSize >= 125) {
                        bufSize -= 25;
                        break;
                    }
                    j += 25;
                } else if (this.tempPacket[j] != 242 && this.tempPacket[j] != 243 && this.tempPacket[j] != 146) {
                    return 0;
                } else {
                    bufSize += 43;
                    if (bufSize >= 125) {
                        bufSize -= 43;
                        break;
                    }
                    j += 43;
                }
            }
            System.arraycopy(this.tempPacket, tempBufCount, printerBuffer, 2, bufSize);
            printerBuffer[bufSize + 2] = 4;
            if (m_PrinterType == 'A') {
                for (int k = 1; k < bufSize + 3; k++) {
                    Lrc ^= printerBuffer[k];
                }
                printerBuffer[bufSize + 3] = Lrc;
                c = sendData(printerBuffer, bufSize + 4);
            } else if (m_PrinterType == 'E') {
                c = sendData(printerBuffer, bufSize + 3);
            }
            if (c != 128) {
                return c;
            }
            NoOfBytesToPrint -= bufSize;
            tempBufCount += bufSize;
            Lrc = 0;
        }
        return 128;
    }

    private int sendData(int[] packet, int length) {
        int counter = 1;
        while (counter <= 3) {
            for (int i = 0; i < length; i++) {
                try {
                    this.outputStream.write(packet[i]);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                this.outputStream.flush();
            } catch (IOException e) {
            }
            int c = 0;
            try {
                c = this.inputStream.read();
            } catch (IOException e2) {
            }
            setResponse(c);
            int response = getResponse();
            if (response != 128 && !this.timeoutFlag) {
                this.responseByte = 0;
                return response;
            } else if (response == 128 || !this.timeoutFlag) {
                this.responseByte = 0;
                return response;
            } else {
                this.timeoutFlag = false;
                System.out.println("Retrying");
                counter++;
            }
        }
        if (counter > 3) {
            this.responseByte = 0;
            return 68;
        }
        this.responseByte = 0;
        return 0;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void setResponse(int r2) {
        /*
        r1 = this;
        monitor-enter(r1);
    L_0x0001:
        r0 = r1.available;	 Catch:{ all -> 0x0015 }
        if (r0 != 0) goto L_0x000f;
    L_0x0005:
        r1.responseByte = r2;	 Catch:{ all -> 0x0015 }
        r0 = 1;
        r1.available = r0;	 Catch:{ all -> 0x0015 }
        r1.notifyAll();	 Catch:{ all -> 0x0015 }
        monitor-exit(r1);
        return;
    L_0x000f:
        r1.wait();	 Catch:{ InterruptedException -> 0x0013 }
        goto L_0x0001;
    L_0x0013:
        r0 = move-exception;
        goto L_0x0001;
    L_0x0015:
        r0 = move-exception;
        monitor-exit(r1);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.upass.MyPrinter.BTPrinter.setResponse(int):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized int getResponse() {
        /*
        r4 = this;
        monitor-enter(r4);
        r0 = 100;
    L_0x0003:
        r1 = r4.available;	 Catch:{ all -> 0x001e }
        if (r1 == 0) goto L_0x0011;
    L_0x0007:
        r1 = 0;
        r4.available = r1;	 Catch:{ all -> 0x001e }
        r4.notifyAll();	 Catch:{ all -> 0x001e }
        r1 = r4.responseByte;	 Catch:{ all -> 0x001e }
        monitor-exit(r4);
        return r1;
    L_0x0011:
        r2 = 3;
        r4.wait(r2);	 Catch:{ InterruptedException -> 0x0021 }
    L_0x0016:
        r0 = r0 + -1;
        if (r0 != 0) goto L_0x0003;
    L_0x001a:
        r1 = 1;
        r4.timeoutFlag = r1;	 Catch:{ all -> 0x001e }
        goto L_0x0007;
    L_0x001e:
        r1 = move-exception;
        monitor-exit(r4);
        throw r1;
    L_0x0021:
        r1 = move-exception;
        goto L_0x0016;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.upass.MyPrinter.BTPrinter.getResponse():int");
    }

    /**
     * Print line m boolean.
     *
     * @param strprint the strprint
     * @return the boolean
     */
    public boolean PrintLineM(String strprint) {
        try {
            this.outputStream = bluetoothSocket.getOutputStream();
            this.outputStream.write(new byte[]{(byte) 27, (byte) 85, (byte) 1});
            this.outputStream.write(hexStringToByteArray(strprint));
            this.outputStream.write(13);
            byte[] Command2 = new byte[3];
            Command2[0] = (byte) 27;
            Command2[1] = (byte) 85;
            this.outputStream.write(Command2);
            return true;
        } catch (IOException e) {
            this.m_ErrString = e.getMessage();
            this.m_ErrCode = -1;
            isBTConnected = false;
            return false;
        }
    }

    /**
     * Hex string to byte array byte [ ].
     *
     * @param s the s
     * @return the byte [ ]
     */
    public byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[(len / 2)];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = Integer.decode("0x" + s.charAt(i) + s.charAt(i + 1)).byteValue();
        }
        return data;
    }

    private boolean PrintTextB(String data, char m_FontType) {
        try {
            this.outputStream = bluetoothSocket.getOutputStream();
            byte size = (byte) 6;
            switch (m_FontType) {
                case 'A':
                    size = (byte) 3;
                    break;
                case 'B':
                    size = (byte) 4;
                    break;
                case 'C':
                    size = (byte) 22;
                    break;
                case 'S':
                    size = (byte) 6;
                    break;
                case 'T':
                    size = (byte) 6;
                    break;
            }
            this.outputStream.write(size);
            byte[] send;
            if (data.length() > 32) {
                send = (" " + data.substring(0, 31)).getBytes();
                this.outputStream.write(send, 0, send.length);
                this.outputStream.write(13);
                byte[] send1 = (" " + data.substring(31)).getBytes();
                this.outputStream.write(send1, 0, send1.length);
                this.outputStream.write(13);
                this.outputStream.flush();
            } else {
                send = (" " + data).getBytes();
                this.outputStream.write(send, 0, send.length);
                this.outputStream.write(13);
                this.outputStream.flush();
            }
            return true;
        } catch (IOException e) {
            this.m_ErrString = e.getMessage();
            this.m_ErrCode = -1;
            isBTConnected = false;
            return false;
        }
    }

/*    public boolean PrintLineImage(int line, char c, String text) {
        int height = line * 50;
        int alignment = 49;
        if (c == 'C') {
            alignment = 51;
        } else if (c == 'L') {
            alignment = 49;
        } else if (c == 'R') {
            alignment = 50;
        }
        Bitmap ecgBmp = new Cocos2dxBitmap(this.mcontext).createTextBitmap(text, "monospace", 0, (int) 28.0f, alignment, 400, height);
        GetPrintableImage pt = new GetPrintableImage();
        this.prnRasterImg = pt.GetPrintableArray(this.mcontext, ecgBmp.getWidth(), ecgBmp);
        this.image_height = pt.getPrintHeight();
        this.image_width = pt.getPrintWidth();
        try {
            if (bluetoothSocket == null) {
                return false;
            }
            this.outputStream = bluetoothSocket.getOutputStream();
            byte[] CommandImagePrint = new byte[254];
            CommandImagePrint[0] = (byte) 27;
            CommandImagePrint[1] = (byte) 35;
            CommandImagePrint[2] = (byte) this.image_width;
            CommandImagePrint[3] = (byte) (this.image_height / 256);
            CommandImagePrint[4] = (byte) (this.image_height % 256);
            this.outputStream.write(CommandImagePrint, 0, 5);
            CommandImagePrint = new byte[this.prnRasterImg.length];
            for (int i = 0; i < CommandImagePrint.length; i++) {
                CommandImagePrint[i] = (byte) (this.prnRasterImg[i] & 255);
            }
            this.outputStream.write(CommandImagePrint);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean PrintLineImage(int line, char c, String text, float fontSize) {
        int height = line * 50;
        int alignment = 49;
        if (c == 'C') {
            alignment = 51;
        } else if (c == 'L') {
            alignment = 49;
        } else if (c == 'R') {
            alignment = 50;
        }
        Bitmap ecgBmp = new Cocos2dxBitmap(this.mcontext).createTextBitmap(text, "monospace", 0, (int) fontSize, alignment, 400, height);
        GetPrintableImage pt = new GetPrintableImage();
        this.prnRasterImg = pt.GetPrintableArray(this.mcontext, ecgBmp.getWidth(), ecgBmp);
        this.image_height = pt.getPrintHeight();
        this.image_width = pt.getPrintWidth();
        try {
            if (bluetoothSocket == null) {
                return false;
            }
            this.outputStream = bluetoothSocket.getOutputStream();
            byte[] CommandImagePrint = new byte[254];
            CommandImagePrint[0] = (byte) 27;
            CommandImagePrint[1] = (byte) 35;
            CommandImagePrint[2] = (byte) this.image_width;
            CommandImagePrint[3] = (byte) (this.image_height / 256);
            CommandImagePrint[4] = (byte) (this.image_height % 256);
            this.outputStream.write(CommandImagePrint, 0, 5);
            CommandImagePrint = new byte[this.prnRasterImg.length];
            for (int i = 0; i < CommandImagePrint.length; i++) {
                CommandImagePrint[i] = (byte) (this.prnRasterImg[i] & 255);
            }
            this.outputStream.write(CommandImagePrint);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean PrintLogoMCC(Bitmap ecgBmp) {
        GetPrintableImage pt = new GetPrintableImage();
        this.prnRasterImg = pt.GetPrintableArray(this.mcontext, ecgBmp.getWidth(), ecgBmp);
        this.image_height = pt.getPrintHeight();
        this.image_width = pt.getPrintWidth();
        try {
            if (bluetoothSocket == null) {
                return false;
            }
            this.outputStream = bluetoothSocket.getOutputStream();
            byte[] CommandImagePrint = new byte[254];
            CommandImagePrint[0] = (byte) 27;
            CommandImagePrint[1] = (byte) 35;
            CommandImagePrint[2] = (byte) this.image_width;
            CommandImagePrint[3] = (byte) (this.image_height / 256);
            CommandImagePrint[4] = (byte) (this.image_height % 256);
            this.outputStream.write(CommandImagePrint, 0, 5);
            CommandImagePrint = new byte[this.prnRasterImg.length];
            for (int i = 0; i < CommandImagePrint.length; i++) {
                CommandImagePrint[i] = (byte) (this.prnRasterImg[i] & 255);
            }
            this.outputStream.write(CommandImagePrint);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }*/
}
