package in.cableguy.cableguy.btprinter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Typeface;
import android.util.Log;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedList;

/**
 * The type Cocos 2 dx bitmap.
 */
public class Cocos2dxBitmap {
    /**
     * The constant ALIGNCENTER.
     */
    public static final int ALIGNCENTER = 51;
    /**
     * The constant ALIGNLEFT.
     */
    public static final int ALIGNLEFT = 49;
    /**
     * The constant ALIGNRIGHT.
     */
    public static final int ALIGNRIGHT = 50;
    private Context context;

    private class TextProperty {
        /**
         * The Height per line.
         */
        int heightPerLine;
        /**
         * The Lines.
         */
        String[] lines;
        /**
         * The Max width.
         */
        int maxWidth;
        /**
         * The Total height.
         */
        int totalHeight;

        /**
         * Instantiates a new Text property.
         *
         * @param w     the w
         * @param h     the h
         * @param lines the lines
         */
        TextProperty(int w, int h, String[] lines) {
            this.maxWidth = w;
            this.heightPerLine = h;
            this.totalHeight = lines.length * h;
            this.lines = lines;
        }
    }

    private native void nativeInitBitmapDC(int i, int i2, byte[] bArr);

    /**
     * Instantiates a new Cocos 2 dx bitmap.
     *
     * @param context the context
     */
    public Cocos2dxBitmap(Context context) {
        this.context = context;
    }

    /**
     * Create text bitmap bitmap.
     *
     * @param content   the content
     * @param fontName  the font name
     * @param typeface  the typeface
     * @param fontSize  the font size
     * @param alignment the alignment
     * @param width     the width
     * @param height    the height
     * @return the bitmap
     */
    public Bitmap createTextBitmap(String content, String fontName, int typeface, int fontSize, int alignment, int width, int height) {
        content = refactorString(content);
        Paint paint = newPaint(fontName, fontSize, alignment, typeface);
        paint.setColor(-16777216);
        paint.setStrokeWidth(1.5f);
        TextProperty textProperty = computeTextProperty(content, paint, width, height);
        Bitmap bitmap = Bitmap.createBitmap(textProperty.maxWidth, textProperty.totalHeight, Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(-1);
        int y = -paint.getFontMetricsInt().ascent;
        for (String line : textProperty.lines) {
            canvas.drawText(line, (float) computeX(paint, line, textProperty.maxWidth, alignment), (float) y, paint);
            y += textProperty.heightPerLine;
        }
        return bitmap;
    }

    private int computeX(Paint paint, String content, int w, int alignment) {
        switch (alignment) {
            case ALIGNRIGHT /*50*/:
                return w;
            case ALIGNCENTER /*51*/:
                return w / 2;
            default:
                return 0;
        }
    }

    private TextProperty computeTextProperty(String content, Paint paint, int maxWidth, int maxHeight) {
        FontMetricsInt fm = paint.getFontMetricsInt();
        int h = (int) Math.ceil((double) (fm.descent - fm.ascent));
        int maxContentWidth = 0;
        String[] lines = splitString(content, maxHeight, maxWidth, paint);
        if (maxWidth != 0) {
            maxContentWidth = maxWidth;
        } else {
            for (String line : lines) {
                int temp = (int) Math.ceil((double) paint.measureText(line, 0, line.length()));
                if (temp > maxContentWidth) {
                    maxContentWidth = temp;
                }
            }
        }
        return new TextProperty(maxContentWidth, h, lines);
    }

    private String[] splitString(String content, int maxHeight, int maxWidth, Paint paint) {
        String[] lines = content.split("\\n");
        String[] ret = null;
        FontMetricsInt fm = paint.getFontMetricsInt();
        int maxLines = maxHeight / ((int) Math.ceil((double) (fm.descent - fm.ascent)));
        LinkedList<String> strList;
        if (maxWidth != 0) {
            strList = new LinkedList();
            for (String line : lines) {
                if (((int) Math.ceil((double) paint.measureText(line))) > maxWidth) {
                    strList.addAll(divideStringWithMaxWidth(paint, line, maxWidth));
                } else {
                    strList.add(line);
                }
                if (maxLines > 0 && strList.size() >= maxLines) {
                    break;
                }
            }
            if (maxLines > 0 && strList.size() > maxLines) {
                while (strList.size() > maxLines) {
                    strList.removeLast();
                }
            }
            ret = new String[strList.size()];
            strList.toArray(ret);
            return ret;
        } else if (maxHeight == 0 || lines.length <= maxLines) {
            return lines;
        } else {
            strList = new LinkedList();
            for (int i = 0; i < maxLines; i++) {
                strList.add(lines[i]);
            }
            ret = new String[strList.size()];
            strList.toArray(ret);
            return ret;
        }
    }

    private LinkedList<String> divideStringWithMaxWidth(Paint paint, String content, int width) {
        int charLength = content.length();
        int start = 0;
        LinkedList<String> strList = new LinkedList();
        int i = 1;
        while (i <= charLength) {
            int tempWidth = (int) Math.ceil((double) paint.measureText(content, start, i));
            if (tempWidth >= width) {
                int lastIndexOfSpace = content.substring(0, i).lastIndexOf(" ");
                if (lastIndexOfSpace != -1) {
                    strList.add(content.substring(start, lastIndexOfSpace));
                    i = lastIndexOfSpace;
                } else if (tempWidth > width) {
                    strList.add(content.substring(start, i - 1));
                    i--;
                } else {
                    strList.add(content.substring(start, i));
                }
                start = i;
            }
            i++;
        }
        if (start < charLength) {
            strList.add(content.substring(start));
        }
        return strList;
    }

    private Paint newPaint(String fontName, int fontSize, int alignment, int typeface) {
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setTextSize((float) fontSize);
        paint.setAntiAlias(true);
        if (fontName.endsWith(".ttf")) {
            try {
                paint.setTypeface(Typeface.createFromAsset(this.context.getAssets(), fontName));
            } catch (Exception e) {
                Log.e("Cocos2dxBitmap", "error to create ttf type face: " + fontName);
                paint.setTypeface(Typeface.create(fontName, typeface));
            }
        } else {
            paint.setTypeface(Typeface.create(fontName, typeface));
        }
        switch (alignment) {
            case ALIGNLEFT /*49*/:
                paint.setTextAlign(Align.LEFT);
                break;
            case ALIGNRIGHT /*50*/:
                paint.setTextAlign(Align.RIGHT);
                break;
            case ALIGNCENTER /*51*/:
                paint.setTextAlign(Align.CENTER);
                break;
            default:
                paint.setTextAlign(Align.LEFT);
                break;
        }
        return paint;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String refactorString(java.lang.String r6) {
        /*
        r5 = this;
        r3 = "";
        r3 = r6.compareTo(r3);
        if (r3 != 0) goto L_0x000b;
    L_0x0008:
        r3 = " ";
    L_0x000a:
        return r3;
    L_0x000b:
        r2 = new java.lang.StringBuilder;
        r2.<init>(r6);
        r1 = 0;
        r3 = "\n";
        r0 = r2.indexOf(r3);
    L_0x0017:
        r3 = -1;
        if (r0 != r3) goto L_0x001f;
    L_0x001a:
        r3 = r2.toString();
        goto L_0x000a;
    L_0x001f:
        if (r0 == 0) goto L_0x002b;
    L_0x0021:
        r3 = r0 + -1;
        r3 = r2.charAt(r3);
        r4 = 10;
        if (r3 != r4) goto L_0x0045;
    L_0x002b:
        r3 = " ";
        r2.insert(r1, r3);
        r1 = r0 + 2;
    L_0x0032:
        r3 = r2.length();
        if (r1 > r3) goto L_0x001a;
    L_0x0038:
        r3 = r2.length();
        if (r0 == r3) goto L_0x001a;
    L_0x003e:
        r3 = "\n";
        r0 = r2.indexOf(r3, r1);
        goto L_0x0017;
    L_0x0045:
        r1 = r0 + 1;
        goto L_0x0032;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.upass.MyPrinter.Cocos2dxBitmap.refactorString(java.lang.String):java.lang.String");
    }

    private void initNativeObject(Bitmap bitmap) {
        byte[] pixels = getPixels(bitmap);
        if (pixels != null) {
            nativeInitBitmapDC(bitmap.getWidth(), bitmap.getHeight(), pixels);
        }
    }

    /**
     * Get pixels byte [ ].
     *
     * @param bitmap the bitmap
     * @return the byte [ ]
     */
    public byte[] getPixels(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        byte[] pixels = new byte[((bitmap.getWidth() * bitmap.getHeight()) * 4)];
        ByteBuffer buf = ByteBuffer.wrap(pixels);
        buf.order(ByteOrder.nativeOrder());
        bitmap.copyPixelsToBuffer(buf);
        return pixels;
    }
}
